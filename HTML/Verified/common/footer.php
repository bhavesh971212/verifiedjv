<!-- REGISTER MODAL HERE -->
<div id="mySidenav" class="sidenav">
	<a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
	<a href="javascript:void(0);">About</a>
	<a href="javascript:void(0);">Services</a>
	<a href="javascript:void(0);">Clients</a>
	<a href="javascript:void(0);">Contact Us</a>
</div>
<div id="header-myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">  
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title">Create an new account</h3>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
								<div class="text-center">
									<div id="login-r-button">
										<ul class="list-inline">
											<li><img src="images/6.png" alt=""></li>
											<li><img src="images/6.png" alt=""></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
								<div class="text-center">
                                    <div id="login-r-button">
                                        <ul  class="list-inline">
                                          <li>
                                            <div>
                                                <input id="radio-1" class="radio-custom" name="radio-group" type="radio" checked>
                                                <label for="radio-1" class="radio-custom-label">Land Seeker</label>
                                            </div>
                                          </li>
                                          <li>
                                            <div>
                                                <input id="radio-2" class="radio-custom" name="radio-group" type="radio">
                                                <label for="radio-2" class="radio-custom-label">Land Owner</label>
                                            </div>
                                          </li>
                                        </ul>
                                    </div>
                              	</div>
                              	<form action="">
                              		<div class="login-model-form">
										<div class="form-group">
											<input type="text" class="form-control" id="usr" placeholder="Full Name">
										</div>
										<div class="form-group">
											<input type="tel" class="form-control" id="tele-phone" placeholder="Mobile Number" required="">
										</div>
										<div class="form-group">
											<input type="text" class="form-control" id="email" placeholder=" Email id" required="">
										</div>
										<div class="form-group">
											<input type="password" class="form-control" id="pwd" placeholder="Password*" required="">
										</div>
										<ul class="list-inline vjv-user">
											<li>
												<button type="submit" id="set-alert2" class="text-center">Submit</button>
											</li>
											<li>
												<p class="vjv-account-p1">Are you a VJV user?</p>
												<p class="vjv-account-p2">If you have a account.<a data-dismiss="modal" data-toggle="modal" href="#login-myModal"> Login</a></p>
											</li>
										</ul>
                              		</div>
                              	</form>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="row">
							<div class="col-sm-12 col-md-12 col-sm-12">
								<div class="new-account-rightpart">
									<ul>
										<li>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium.</li>
										<li>Voluptatum deleniti atque corrupti quos dolores et quas molestias.</li>
										<li>Soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime.</li>
										<li>Omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis.</li>
										<li>Password should have at least 8 characters including 1 special character.</li>
										<li>By signing up you agree to our terms and privacy policy.</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 col-md-12 col-sm-12">
								<div class="new-account-image">
									<img class="img-responsive" src="images/login.png" alt="">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- LOGIN MODAL -->
<div id="login-myModal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title">Login</h3>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
								<form action="">
									<div class="login-model-form">
										<div class="form-group">
											<input type="text" class="form-control" id="email" placeholder="Email id/Mobile Number" required="">
										</div>
										<div class="form-group">
											<input type="password" class="form-control" id="pwd" placeholder="Password*" required="">
											<p class="login-model-p">Forgot you <a href="#">Password</a>?</p>
										</div>
										<div class="login-checkbox">
											<input id="checkbox-1" class="checkbox-custom" name="checkbox-1" type="checkbox" checked>
											<label for="checkbox-1" class="checkbox-custom-label">Stay Logged in</label>
										</div>
										<ul class="list-inline vjv-user">
											<li>
												<button type="submit" id="set-alert2" class="text-center">Login</button></li>
											<li>
												<p class="vjv-account-p1">New to VJV?</p>
												<p class="vjv-account-p2">Get started now.<a href="javascript:void(0);" data-dismiss="modal" data-toggle="modal" data-target="#header-myModal">Register</a></p>
											</li>
										</ul>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="row">
								<div class="col-sm-12 col-md-12 col-sm-12">
									<div class="new-account-image text-center">
										<img class="img-responsive" src="images/login.png" alt="">
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- SOCIAL ICONS CODE -->
<div id="social_icons">
	<ul>
		<li><a href="javascript:void(0);" target="_blank"><img src="images/facebook.png" width="35" height="35" alt="venture_facebook"></a></li>
		<li><a href="javascript:void(0);" target="_blank"><img src="images/google.png" width="35" height="35" alt="venture_google"></a></li>
		<li><a href="javascript:void(0);" target="_blank"><img src="images/twitter.png" width="35" height="35" alt="venture_twitter"></a></li>
		<li><a href="javascript:void(0);" target="_blank"><img src="images/linkedin.png" width="35" height="35" alt="venture_linkedin"></a></li>
		<li><a href="javascript:void(0);" target="_blank"><img src="images/youtube.png" width="35" height="35" alt="venture_youtube"></a></li>
	</ul>
</div>
<!-- COMMON PROPERTY ALERT CODE -->
<div class="property_alert animate_div_click">
	<img src="images/property.png" height="120" data-attr="property_alert">
	<div id="feedback-inner">
		<div class="text-center">
			<h3>Set a Property Alert</h3>
			<p>Get matching properties delivered in your inbox as soon as they're uploaded</p>
			<form action="">
				<ul  class="list-inline">
					<li>
						<div>
							<input id="land_seeker" class="sa-custom" name="send_feedback_as" type="radio" checked>
							<label for="land_seeker" class="sa-custom-label">Land Seeker</label>
						</div>
					</li>
					<li>
						<div>
							<input id="land_owner" class="sa-custom" name="send_feedback_as" type="radio">
							<label for="land_owner" class="sa-custom-label">Land Owner</label>
						</div>
					</li>
				</ul>
				<div class="form-group">
					<input type="text" class="form-control" id="usr" placeholder="Your Name*">
				</div>
				<div class="form-group">
					<input type="tel" class="form-control" id="tele-phone" placeholder="Your Mobile Number*">
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="email" placeholder="Your Email id*">
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="location" placeholder="Preffered Location">
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="pwd" placeholder="Land Size" required="">
				</div>
			</form>
		</div>
	</div>
</div>
<!-- COMMON FEEDBACK ALERT CODE -->
<div class="feedback_alert animate_div_click">
	<img src="images/feedback.png" height="120" data-attr="feedback_alert">
	<div id="feedback-inner">
		<div class="text-center">
			<h3>WE ARE ALL EARS!!!</h3>
			<p>Tell us what you love, tell us what you hate, &amp; tell us what you want us to improve</p>
			<form action="">
				<div class="form-group">
					<input type="text" class="form-control" id="usr" placeholder=" Your Name*" required="">
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="email" placeholder=" Your Email id*" required="">
				</div>
				<div class="form-group">
					<textarea  rows="5" id="comment" placeholder="Your feedback*"></textarea>
				</div>
				<button type="submit" class="btn  text-center" id="set-alert1">Submit</button>
			</form>
		</div>
	</div>
</div>

<!-- SCRIPT FILES HERE -->
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="plugins/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="js/owl.carousel.min.js"></script>
<script type="text/javascript" src="js/bootstrap-select.js"></script>
<script type="text/javascript" src="js/jquery.colorbox.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript">
	function openNav() {
		document.getElementById("mySidenav").style.width = "250px";
	}
	function closeNav() {
		document.getElementById("mySidenav").style.width = "0";
	}
</script>
</body>
</html>