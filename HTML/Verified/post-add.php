<?php include('common/header.php') ?>
	<section id="inner_header">
		<header>
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<ul class="header_left">
							<li><a href="index.php"><img src="images/white-logo.png" alt="Verified Joint Ventures" width="100"></a></li>
							<li><a href="javascript:void(0);" id="call_icon">+91 9878834567</a></li>
							<li><a href="javascript:void(0);" id="mail_icon">info@verified.com</a></li>
						</ul>
					</div>
					<div class="col-md-6">
						<ul class="header_right nav_bar_link">
							<li>
								<div class="dropdown">
									<button class="btn dropdown-toggle" type="button" id="post_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Post&nbsp;&nbsp;<span class="caret"></span></button>
									<ul class="dropdown-menu" aria-labelledby="post_menu">
										<li><a href="post-add.php">Post Adds</a></li>
										<li><a href="post-requirement.php">Post Requirements</a></li>
									</ul>
								</div>
							</li>
							<li><a href="javascript:void(0);" data-toggle="modal" data-target="#login-myModal">Login</a></li>
							<li><a href="javascript:void(0);" onclick="openNav()"><i class="fa fa-bars" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</header>
	</section>
	<div class="post-ad-inner">
		<form action="">
			<div class="post-ad-details">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<ul class="list-inline post-border">
								<li><h3 class="post-color">Post a ads</h3></li>
								<li class="post-req">
									<div class="dropdown">
										<button class="btn btn-default dropdown-toggle" type="button" id="demo" data-toggle="dropdown">Post Your Requirement&nbsp;<span class="caret"></span></button>
										<ul class="dropdown-menu" role="menu" aria-labelledby="demo">
											<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Post a ads</a></li>
											<li role="presentation" class="divider"></li>
											<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Post a requirements</a></li>
										</ul>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="property-content">
					<p>Property Details</p>
				</div>
				<div class="container">
					<div class="post-form">
						<div class="row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>Title<span class="requirement-span">*</span></label>
									<select id="basic" class="selectpicker">
										<option>Residential</option>
										<option>Mustard</option>
										<option>Ketchup</option>
										<option>Relish</option>
									</select>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>Zone<span class="requirement-span">*</span></label>
									<select id="basic" class="selectpicker">
										<option>Residential</option>
										<option>Mustard</option>
										<option>Ketchup</option>
										<option>Relish</option>
									</select>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>Property<span class="requirement-span">*</span></label>
									<select id="basic" class="selectpicker">
										<option>Toenship</option>
										<option>Mustard</option>
										<option>Ketchup</option>
										<option>Relish</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group form-marg">
									<label>Property area<span class="requirement-span">*</span></label>
									<div class="combine-input-select">
										<div class="row">
											<div class="col-xs-6 col-sm-6 col-md-6">
												<input type="text" name="" value="" placeholder="5 to 6 acre">
											</div>
											<div class="col-xs-6 col-sm-6 col-md-6">
												<select id="basic" class="selectpicker">
													<option>Acre</option>
													<option>Mustard</option>
													<option>Ketchup</option>
													<option>Relish</option>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>Expected market value<span class="requirement-span">*</span></label>
									<select id="basic" class="selectpicker">
										<option>20 - 25</option>
										<option>Mustard</option>
										<option>Ketchup</option>
										<option>Relish</option>
									</select>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>Token advance<span class="requirement-span">*</span></label>
									<select id="basic" class="selectpicker">
										<option>2 crores</option>
										<option>Mustard</option>
										<option>Ketchup</option>
										<option>Relish</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>Ratio (Owner : Builder)<span class="requirement-span">*</span></label>
									<select id="basic" class="selectpicker">
										<option>40:60</option>
										<option>Mustard</option>
										<option>Ketchup</option>
										<option>Relish</option>
									</select>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>Road width<span class="requirement-span">*</span></label>
									<select id="basic" class="selectpicker">
										<option>60 feet</option>
										<option>Mustard</option>
										<option>Ketchup</option>
										<option>Relish</option>
									</select>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>Frontage<span class="requirement-span">*</span></label>
									<select id="basic" class="selectpicker">
										<option>100 feet</option>
										<option>Mustard</option>
										<option>Ketchup</option>
										<option>Relish</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
								<div class="form-group message-something">
									<label for="comment">Property description</label>
									<textarea class="form-control" rows="3" id="comment1" placeholder="4 acer joint venture land available in pune.This joint venture property in pune is suited at kanhe phata, near sai baba sevadham"></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="property-content">
					<p>Property Locality</p>
				</div>
				<div class="property-location">
					<div class="container">
						<div class="post-form">
							<div class="row">
								<div class="col-xs-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label>City</label>
										<select id="basic" class="selectpicker">
											<option>Pune</option>
											<option>Bangalore</option>
											<option>Hyderabad</option>
											<option>Delhi</option>
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label>City Location</label>
										<select id="basic" class="selectpicker">
											<option>Kanhe phata</option>
											<option>City 1</option>
											<option>City 2</option>
											<option>City 3</option>
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6">
									<div class="form-group">
										<label>Address</label>
										<input type="text" class="form-control" id="address" placeholder=" 60 &amp; 61 1st Floor,Dr Rajkumar road,Rajaji Nagar,Karnataka 560021">
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12">
									<div style="width: 100%" class="post-iframe">
										<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3887.727065594782!2d77.55686221422218!3d12.989300818005535!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae3d9a0fffff8b%3A0xdac2494122c74956!2sIndglobal!5e0!3m2!1sen!2sin!4v1488962983616" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="property-content">
					<p>Property Photos</p>
				</div>
				<div class="property-photo">
					<div class="container">
						<div class="post-form">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12">
									<div class="photoupload">
										<p>Photos format must be in .jpg, .gif &amp; .png. size < 5MB</p>
										<ul class="list-inline">
											<li>
												<label for="fileupload" class="fileupload"></label>
												<input type="file" name="fileupload" value="fileupload" id="fileupload">
												<i class="fa fa-plus-circle file-11 fa-3x" aria-hidden="true"></i>
											</li>
											<li>
												<label for="fileupload-1" class="fileupload"></label>
												<input type="file" name="fileupload-1" value="fileupload" id="fileupload">
												<i class="fa fa-plus-circle file-11 fa-3x" aria-hidden="true"></i>
											</li>
											<li>
												<label for="fileupload-2" class="fileupload"></label>
												<input type="file" name="fileupload-2" value="fileupload" id="fileupload">
												<i class="fa fa-plus-circle file-11 fa-3x" aria-hidden="true"></i>
											</li>
											<li>
												<label for="fileupload-3" class="fileupload"></label>
												<input type="file" name="fileupload-3" value="fileupload" id="fileupload">
												<i class="fa fa-plus-circle file-11 fa-3x" aria-hidden="true"></i>
											</li>
											<li>
												<label for="fileupload-4" class="fileupload"></label>
												<input type="file" name="fileupload-4" value="fileupload" id="fileupload">
												<i class="fa fa-plus-circle file-11 fa-3x" aria-hidden="true"></i>
											</li>
											<li>
												<label for="fileupload-5" class="fileupload"></label>
												<input type="file" name="fileupload-5" value="fileupload" id="fileupload">
												<i class="fa fa-plus-circle file-11 fa-3x" aria-hidden="true"></i>
											</li>
										</ul>
										<div class="photoupload-absolute">	
											<p class="text-center main-photo">This will be our main photo</p>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12">
									<div class="photoupload">
										<ul class="list-inline">
											<li>
												<label for="fileupload-6" class="fileupload"></label>
												<input type="file" name="fileupload-6" value="fileupload" id="fileupload">
												<i class="fa fa-plus-circle file-11 fa-3x" aria-hidden="true"></i>
											</li>
											<li>
												<label for="fileupload-7" class="fileupload"></label>
												<input type="file" name="fileupload-7" value="fileupload" id="fileupload">
												<i class="fa fa-plus-circle file-11 fa-3x" aria-hidden="true"></i>
											</li>
											<li>
												<label for="fileupload-8" class="fileupload"></label>
												<input type="file" name="fileupload-8" value="fileupload" id="fileupload">
												<i class="fa fa-plus-circle file-11 fa-3x" aria-hidden="true"></i>
											</li>
											<li>
												<label for="fileupload-9" class="fileupload"></label>
												<input type="file" name="fileupload-9" value="fileupload" id="fileupload">
												<i class="fa fa-plus-circle file-11 fa-3x" aria-hidden="true"></i>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="property-content">
					<p>Contact Details</p>
				</div>
				<div class="contact-details">
					<div class="container">
						<div class="post-form">
							<div class="row">
								<div class="col-xs-12 col-sm-4 col-md-4">
									<div class="form-group">
										<label for="usr">Name</label>
										<input type="text" class="form-control" id="usr" placeholder="Brigade Developer" required="">
									</div>
								</div>
								<div class="col-xs-12 col-sm-4 col-md-4">
									<div class="form-group">
										<label for="pwd">Mobile Number</label>
										<input type="tel" name="usrtel" class="form-control" placeholder="+91 1234567890" required="">
										<div class="checkbox">
											<label><input type="checkbox">Display my number</label>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-4 col-md-4">
									<div class="form-group">
										<label for="email">Email Id</label>
										<input type="email" class="form-control" id="email" placeholder="info@brigadedeveloper.com" required="">
										<div class="checkbox">
											<label><input type="checkbox">Display my number</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="contact-details">
					<div class="container">
						<div class="post-form ">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12">
									<p class="tems-rules">By clicking 'Submit' you agree to our <a href="#">Terms of use</a> &amp;<a href="#"> Posting Rules</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="contact-details contat-pad">
					<div class="container">
						<div class="post-form ">
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-6">
									<ul class="list-inline">
										<li><button type="submit" class="btn  contact-submit">Submit</button></li>
										<li><button type="cancel" class="btn  contact-cancel">Cancel</button></li>
									</ul>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6">
									<button type="submit" class="btn contact-submit preview">Preview</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<footer id="inner_footer">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<ul class="text-left">
						<li><a href="javascript:void(0);">About Us</a></li>
						<li><a href="javascript:void(0);">Recent</a></li>
						<li><a href="javascript:void(0);">Updates</a></li>
						<li><a href="javascript:void(0);">Career</a></li>
						<li><a href="javascript:void(0);">Contact</a></li>
						<li><a href="javascript:void(0);">Sitemap</a></li>
					</ul>
				</div>
				<div class="col-md-6">
					<ul class="text-right">
						<li><a href="javascript:void(0);">Terms and Condition</a></li>
						<li><a href="javascript:void(0);">Privacy and Legal</a></li>
						<li>&copy; <span id="current_year"></span><a href="javascript:void(0);"> &nbsp;Verified Join ventures Pvt.Ltd.</a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
<?php include('common/footer.php') ?>