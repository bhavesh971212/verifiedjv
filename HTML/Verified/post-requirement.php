<?php include('common/header.php') ?>
	<section id="inner_header">
		<header>
			<div class="container">
				<div class="row">
					<div class="col-md-6">
						<ul class="header_left">
							<li><a href="index.php"><img src="images/white-logo.png" alt="Verified Joint Ventures" width="100"></a></li>
							<li><a href="javascript:void(0);" id="call_icon">+91 9878834567</a></li>
							<li><a href="javascript:void(0);" id="mail_icon">info@verified.com</a></li>
						</ul>
					</div>
					<div class="col-md-6">
						<ul class="header_right nav_bar_link">
							<li>
								<div class="dropdown">
									<button class="btn dropdown-toggle" type="button" id="post_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Post&nbsp;&nbsp;<span class="caret"></span></button>
									<ul class="dropdown-menu" aria-labelledby="post_menu">
										<li><a href="post-add.php">Post Adds</a></li>
										<li><a href="post-requirement.php">Post Requirements</a></li>
									</ul>
								</div>
							</li>
							<li><a href="javascript:void(0);" data-toggle="modal" data-target="#login-myModal">Login</a></li>
							<li><a href="javascript:void(0);" onclick="openNav()"><i class="fa fa-bars" aria-hidden="true"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</header>
	</section>
	<div class="post-ad-inner">
		<form action="">
			<div class="post-ad-details">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<ul class="list-inline post-border">
								<li><h3 class="post-color">Post Your Requirement</h3></li>
								<li class="post-req">
									<div class="dropdown">
										<button class="btn btn-default dropdown-toggle" type="button" id="demo" data-toggle="dropdown">Post Ads&nbsp;<span class="caret"></span></button>
										<ul class="dropdown-menu" role="menu" aria-labelledby="demo">
											<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Post Ads</a></li>
											<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Post Requirements</a></li>
										</ul>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="property-content">
					<p>Property Details</p>
				</div>
				<div class="container">
					<div class="post-form">
						<div class="row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>I am<span class="requirement-span"><sup>*</sup></span></label>
									<select id="basic" class="selectpicker">
										<option>Individual</option>
										<option>Group</option>
										<option>Single</option>
										<option>Group</option>
									</select>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>I want to buy a<span class="requirement-span"><sup>*</sup></span></label>
									<select id="basic" class="selectpicker">
										<option>Commercial Plot</option>
										<option>Living Plot</option>
										<option>Business Plot</option>
									</select>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>Re-sale Plot Only<span class="requirement-span"><sup>*</sup></span></label>
									<select id="basic" class="selectpicker">
										<option>Commercial Plot</option>
										<option>Living Plot</option>
										<option>Business Plot</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>Property Area<span class="requirement-span"><sup>*</sup></span></label>
									<div class="combine-input-select">
										<div class="row">
											<div class="col-xs-6 col-sm-6 col-md-6">
												<input type="text" name="" value="" placeholder="5 to 6 acre">
											</div>
											<div class="col-xs-6 col-sm-6 col-md-6">
												<select id="basic" class="selectpicker">
													<option>Acre</option>
													<option>Acre</option>
													<option>Acre</option>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>Approx Budget<span class="requirement-span"><sup>*</sup></span></label>
									<select id="basic" class="selectpicker">
										<option>20 - 25</option>
										<option>20 - 25</option>
										<option>20 - 25</option>
									</select>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>Preferred Facing Direction<span class="requirement-span"><sup>*</sup></span></label>
									<select id="basic" class="selectpicker">
										<option>Any direction</option>
										<option>North</option>
										<option>South</option>
										<option>East</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
								<div class="form-group message-something">
									<label for="comment">Few Words About My Requirements</label>
									<textarea class="form-control" rows="3" id="comment1" placeholder="Few words...."></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="property-content">
					<p>Property Locality</p>
				</div>
				<div class="property-location location-pad-doun">
					<div class="container">
						<div class="post-form">
							<div class="row">
								<div class="col-xs-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label>City</label>
										<select id="basic" class="selectpicker">
											<option>Pune</option>
											<option>Bangalore</option>
											<option>Hyderabad</option>
											<option>Delhi</option>
										</select>
									</div>
								</div>
								<div class="col-sm-offset-1 col-md-offset-1  col-xs-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label>City Location (upto 5)</label>
										<select id="basic" class="selectpicker select_location">
											<option>Tathawade</option>
											<option>Kharadi</option>
											<option>Wagholi</option>
											<option>Handewadi</option>
											<option>Tathawade</option>
										</select>
									</div>
									<div class="append_area">
										<ul class="list-inline"></ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="property-content">
					<p>Contact Details</p>
				</div>
				<div class="contact-details">
					<div class="container">
						<div class="post-form">
							<div class="row">
								<div class="col-xs-12 col-sm-4 col-md-4">
									<div class="form-group">
										<label for="usr">Name</label>
										<input type="text" class="form-control" id="usr" placeholder="Brigade Developer" required="">
									</div>
								</div>
								<div class="col-xs-12 col-sm-4 col-md-4">
									<div class="form-group">
										<label for="pwd">Mobile Number</label>
										<input type="tel" name="usrtel" class="form-control" placeholder="+91 1234567890" required="">
										<div class="display_check_none">
											<input type="checkbox" class="custom_checkbox" id="disp_numb">
											<label for="disp_numb" class="custom_checkbox_label">Display My Number</label>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-4 col-md-4">
									<div class="form-group">
										<label for="email">Email Id</label>
										<input type="email" class="form-control" id="email" placeholder="info@brigadedeveloper.com" required="">
										<div class="display_check_none">
											<input type="checkbox" class="custom_checkbox" id="disp_mail">
											<label for="disp_mail" class="custom_checkbox_label">Display My Email</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="contact-details">
					<div class="container">
						<div class="post-form ">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12">
									<p class="tems-rules">By clicking 'Submit' you agree to our <a href="javascript:void(0);">Terms of use</a> &amp;<a href="javascript:void(0);"> Posting Rules</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="contact-details contat-pad">
					<div class="container">
						<div class="post-form ">
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-6">
									<ul class="list-inline">
										<li><button type="submit" class="btn  contact-submit">Submit</button></li>
										<li><button type="cancel" class="btn  contact-cancel">Cancel</button></li>
									</ul>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6">
									<button type="submit" class="btn contact-submit preview">Preview</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
	<footer id="inner_footer">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<ul class="text-left">
						<li><a href="javascript:void(0);">About Us</a></li>
						<li><a href="javascript:void(0);">Recent</a></li>
						<li><a href="javascript:void(0);">Updates</a></li>
						<li><a href="javascript:void(0);">Career</a></li>
						<li><a href="javascript:void(0);">Contact</a></li>
						<li><a href="javascript:void(0);">Sitemap</a></li>
					</ul>
				</div>
				<div class="col-md-6">
					<ul class="text-right">
						<li><a href="javascript:void(0);">Terms and Condition</a></li>
						<li><a href="javascript:void(0);">Privacy and Legal</a></li>
						<li>&copy; <span id="current_year"></span><a href="javascript:void(0);"> &nbsp;Verified Join ventures Pvt.Ltd.</a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
<?php include('common/footer.php') ?>