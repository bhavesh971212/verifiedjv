<?php include('common/header.php') ?>
<section id="inner_header">
	<header>
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<ul class="header_left">
						<li><a href="index.php"><img src="images/white-logo.png" alt="Verified Joint Ventures" width="100"></a></li>
						<li><a href="javascript:void(0);" id="call_icon">+91 9878834567</a></li>
						<li><a href="javascript:void(0);" id="mail_icon">info@verified.com</a></li>
					</ul>
				</div>
				<div class="col-md-6">
					<ul class="header_right nav_bar_link">
						<li>
							<div class="dropdown">
								<button class="btn dropdown-toggle" type="button" id="post_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Post&nbsp;&nbsp;<span class="caret"></span></button>
								<ul class="dropdown-menu" aria-labelledby="post_menu">
									<li><a href="post-add.php">Post Adds</a></li>
									<li><a href="post-requirement.php">Post Requirements</a></li>
								</ul>
							</div>
						</li>
						<li><a href="javascript:void(0);" data-toggle="modal" data-target="#login-myModal">Login</a></li>
						<li><a href="javascript:void(0);" onclick="openNav()"><i class="fa fa-bars" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</header>
</section>
	<section id="result_page_section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="filer_section white_bg_color">
						<div class="row">
							<div class="col-md-7 col-xs-12 less_pad_custom">
								<div class="row">
									<div class="col-md-6 less_pad_custom">
										<div class="border_div">
											<select name="" id="">
												<option value="">Land</option>
												<option value="">Plot</option>
											</select>
											<input type="text" placeholder="Neighourhood, City....">
											<button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
										</div>
									</div>
									<div class="col-md-6 less_pad_custom">
										<div class="row">
											<div class="col-md-6 less_pad_custom">
												<select id="basic" class="selectpicker">
													<option>Popular Localities</option>
													<option>Bengaluru</option>
													<option>Hassan</option>
													<option>Mysore</option>
												</select>
											</div>
											<div class="col-md-6 less_pad_custom">
												<select id="basic" class="selectpicker">
													<option>Popular Type</option>
													<option>High Level</option>
													<option>Low Level</option>
													<option>Mid Level</option>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-5 col-xs-12 less_pad_custom">
								<div class="row">
									<div class="col-md-7 less_pad_custom">
										<div class="row">
											<div class="col-md-6 less_pad_custom">
												<select id="basic" class="selectpicker">
													<option>Price Range</option>
													<option>24Lakhs</option>
													<option>25Lakhs</option>
													<option>28Crores</option>
												</select>
											</div>
											<div class="col-md-6 less_pad_custom">
												<select id="basic" class="selectpicker">
													<option>Area Unit</option>
													<option>24Lakhs</option>
													<option>25Lakhs</option>
													<option>28Crores</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-5 less_pad_custom">
										<div class="row">
											<!-- <div class="col-md-4 less_pad_custom"><a href="javascript:void(0);" class="same_btn_list" id="more_filter">More Filters</a></div> -->
											<div class="col-md-6 less_pad_custom"><a href="javascript:void(0);" class="same_btn_list" id="apply">Apply Now</a></div>
											<div class="col-md-6 less_pad_custom"><a href="javascript:void(0);" class="same_btn_list" id="reset">Reset</a></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="main_result">
				<div class="row">
					<div class="col-md-6">
						<div class="result_left_section">
							<div class="white_bg_color" id="map_look_good">
								<div id="map_result"></div>
							</div>
							<div class="white_bg_color" id="contact_form">
								<div class="contact_form_start">
									<div class="text-right">
										<h6 id="close_form"><i class="fa fa-times-circle-o" aria-hidden="true"></i></h6>
									</div>
									<div class="text-center"><h5>CONTACT US</h5></div>
									<form action="">
										<div class="form-group">
											<input type="text" class="form-control" placeholder="Your Name*">
										</div>
										<div class="form-group">
											<input type="text" class="form-control" placeholder="Your Phone Number*">
										</div>
										<div class="form-group">
											<input type="email" class="form-control" placeholder="Your Email*">
										</div>
										<div class="form-group">
											<textarea name="" id="" rows="4" placeholder="Your Message" class="form-control"></textarea>
										</div>
										<div class="form-group">
											<button type="submit" class="submit_full_btn">SUBMIT</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6 less_padding">
						<div class="result_right_section">
							<div class="white_bg_color">
								<div class="title_box_sort">
									<div class="row">
										<div class="col-md-8">
											<h4>Showing 28 results for <span>Residential Projects</span></h4>
										</div>
										<div class="col-md-4 text-right sort_by_select">
											<label for="">Sort By</label>
											<select name="" id="sort_by_select">
												<option value="">Relevent</option>
												<option value="">Abcdef</option>
												<option value="">Bcedddd</option>
											</select>
										</div>
									</div>
								</div>
								<div class="result_listing">
									<ul>
										<li>
											<div class="text-right post_date">
												<h5>Posted on 14<sup>th</sup> Feb 17</h5>
											</div>
											<div class="row">
												<div class="col-md-8">
													<div class="row">
														<div class="col-md-5">
															<div class="left_result">
																<figure>
																	<a href=""><img src="images/result/1.jpg"  alt=""></a>
																</figure>
															</div>
														</div>
														<div class="col-md-7">
															<div class="center_result">
																<a href="">Brigade Developer</a>
																<div>
																	<p class="location">Location</p>
																	<h4>Doddballapur</h4>
																</div>
																<div>
																	<p class="space">Space</p>
																	<h4>174240 Sq.ft (4 Acres)</h4>
																</div>
																<div>
																	<p class="price_tag">Price Range</p>
																	<h4><i class="fa fa-inr" aria-hidden="true"></i> 995/Sq.ft - 4500/Sq.ft</h4>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-4">
													<div class="right_result">
														<ul class="list-inline">
															<li><button class="contact_logo_btn">CONTACT</button></li>
															<li> <button class="favour_btn"><i class="fa fa-heart" aria-hidden="true"></i></button></li>
														</ul>
													</div>
												</div>
											</div>
										</li>
										<li>
											<div class="text-right post_date">
												<h5>Posted on 14<sup>th</sup> Feb 17</h5>
											</div>
											<div class="row">
												<div class="col-md-8">
													<div class="row">
														<div class="col-md-5">
															<div class="left_result">
																<figure>
																	<a href=""><img src="images/result/1.jpg"  alt=""></a>
																</figure>
															</div>
														</div>
														<div class="col-md-7">
															<div class="center_result">
																<a href="">Brigade Developer</a>
																<div>
																	<p class="location">Location</p>
																	<h4>Doddballapur</h4>
																</div>
																<div>
																	<p class="space">Space</p>
																	<h4>174240 Sq.ft (4 Acres)</h4>
																</div>
																<div>
																	<p class="price_tag">Price Range</p>
																	<h4><i class="fa fa-inr" aria-hidden="true"></i> 995/Sq.ft - 4500/Sq.ft</h4>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-4">
													<div class="right_result">
														<ul class="list-inline">
															<li><button class="contact_logo_btn">CONTACT</button></li>
															<li> <button class="favour_btn"><i class="fa fa-heart" aria-hidden="true"></i></button></li>
														</ul>
													</div>
												</div>
											</div>
										</li>
										<li>
											<div class="text-right post_date">
												<h5>Posted on 14<sup>th</sup> Feb 17</h5>
											</div>
											<div class="row">
												<div class="col-md-8">
													<div class="row">
														<div class="col-md-5">
															<div class="left_result">
																<figure>
																	<a href=""><img src="images/result/1.jpg"  alt=""></a>
																</figure>
															</div>
														</div>
														<div class="col-md-7">
															<div class="center_result">
																<a href="">Brigade Developer</a>
																<div>
																	<p class="location">Location</p>
																	<h4>Doddballapur</h4>
																</div>
																<div>
																	<p class="space">Space</p>
																	<h4>174240 Sq.ft (4 Acres)</h4>
																</div>
																<div>
																	<p class="price_tag">Price Range</p>
																	<h4><i class="fa fa-inr" aria-hidden="true"></i> 995/Sq.ft - 4500/Sq.ft</h4>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-4">
													<div class="right_result">
														<ul class="list-inline">
															<li><button class="contact_logo_btn">CONTACT</button></li>
															<li> <button class="favour_btn"><i class="fa fa-heart" aria-hidden="true"></i></button></li>
														</ul>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<footer id="inner_footer">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<ul class="text-left">
						<li><a href="javascript:void(0);">About Us</a></li>
						<li><a href="javascript:void(0);">Recent</a></li>
						<li><a href="javascript:void(0);">Updates</a></li>
						<li><a href="javascript:void(0);">Career</a></li>
						<li><a href="javascript:void(0);">Contact</a></li>
						<li><a href="javascript:void(0);">Sitemap</a></li>
					</ul>
				</div>
				<div class="col-md-6">
					<ul class="text-right">
						<li><a href="javascript:void(0);">Terms and Condition</a></li>
						<li><a href="javascript:void(0);">Privacy and Legal</a></li>
						<li>&copy; <span id="current_year"></span><a href="javascript:void(0);"> &nbsp;Verified Join ventures Pvt.Ltd.</a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
<?php include('common/footer.php') ?>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCdNQvIbP21kxPzykkrLSvU-Hl3X7_F6zc&callback=initMap"></script>
	<script>
		function initMap() {
			var myLatLng = {lat: 12.954517, lng: 77.715671};
			var map = new google.maps.Map(document.getElementById('map_result'), {
				zoom: 13,
				center: myLatLng,
				scrollwheel: false
			});
				var marker = new google.maps.Marker({
				position: myLatLng,
				map: map,
				title: 'Pavani Royal'
			});
		}
	</script>