<?php include('common/header.php') ?>
<section id="inner_header">
	<header>
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<ul class="header_left">
						<li><a href="index.php"><img src="images/white-logo.png" alt="Verified Joint Ventures" width="100"></a></li>
						<li><a href="javascript:void(0);" id="call_icon">+91 9878834567</a></li>
						<li><a href="javascript:void(0);" id="mail_icon">info@verified.com</a></li>
					</ul>
				</div>
				<div class="col-md-6">
					<ul class="header_right nav_bar_link">
						<li>
							<div class="dropdown">
								<button class="btn dropdown-toggle" type="button" id="post_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Post&nbsp;&nbsp;<span class="caret"></span></button>
								<ul class="dropdown-menu" aria-labelledby="post_menu">
									<li><a href="post-add.php">Post Adds</a></li>
									<li><a href="post-requirement.php">Post Requirements</a></li>
								</ul>
							</div>
						</li>
						<li><a href="javascript:void(0);" data-toggle="modal" data-target="#login-myModal">Login</a></li>
						<li><a href="javascript:void(0);" onclick="openNav()"><i class="fa fa-bars" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</header>
</section>
	<section id="property_detail_section">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="property_main_left">
						<div class="cmmn_white_bg">
							<div class="plot_img_section">
								<figure>
									<img src="images/sites/site_image.jpg" class="img-responsive" alt="">
									<figcaption>
										<div class="row">
											<div class="col-md-8">
												<ul class="list-inline rating_list">
													<li><h3>Brigade Plots in Electronic City</h3></li>
													<li><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i><i class="fa fa-star" aria-hidden="true"></i></li>
												</ul>
												<ul class="list-inline">
													<li><h3>₹ 25.6 Lac</h3></li>
													<li><h4>₹ 1,708 per square  feet</h4></li>
												</ul>
											</div>
											<div class="col-md-4">
												<ul class="site_gallery">
													<li><a class="group2" href="images/sites/site_small.png"><img src="images/sites/site_small.png" alt=""></a></li>
													<li><a class="group2" href="images/sites/site_small1.png"><img src="images/sites/site_small1.png" alt=""></a></li>
													<li><a class="group2" href="images/sites/site_small.png"><img src="images/sites/site_small.png" alt=""></a></li>
													<li><a class="group2" href="images/sites/site_small1.png"><img src="images/sites/site_small1.png" alt=""></a></li>
													<li><a class="group2" href="images/sites/site_small.png"><img src="images/sites/site_small.png" alt=""></a></li>
													<li><a class="group2" href="images/sites/site_small1.png"><img src="images/sites/site_small1.png" alt=""></a></li>
													<li class="special_load">+ <span id="total_length"></span> More</li>
												</ul>
											</div>
										</div>
									</figcaption>
									<div class="favor_heart">
										<i class="fa fa-heart" aria-hidden="true"></i>
									</div>
								</figure>
							</div>
							<article>
								<div class="row">
									<div class="col-md-7"><h3>Project OverView</h3></div>
									<div class="col-md-5 text-right">
										<ul class="list-inline user_short_cuts">
											<li class="print"><span></span></li>
											<li class="pdf"><span></span></li>
											<li class="phone"><span></span></li>
										</ul>
									</div>
								</div>
							</article>
							<div class="plot_detail_content">
								<div class="row">
									<div class="col-md-4">
										<table>
											<tr>
												<td>Property Type</td>
												<td><button class="spl">Residential Plot</button></td>
											</tr>
											<tr>
												<td>Plot Dimension</td>
												<td><button class="spl">30 x 40 feet</button></td>
											</tr>
										</table>
									</div>
									<div class="col-md-4">
										<table>
											<tr>
												<td>Direction Facing</td>
												<td><button class="spl">East, West, North</button></td>
											</tr>
											<tr>
												<td>Boundary Wall</td>
												<td><button class="spl">20 Feet Long</button></td>
											</tr>
										</table>
									</div>
									<div class="col-md-4">
										<table>
											<tr>
												<td>Front Road Width </td>
												<td><button class="spl">40 feet</button></td>
											</tr>
											<tr>
												<td>Plots side</td>
												<td><button class="spl">Corner</button></td>
											</tr>
										</table>
									</div>
								</div>
							</div>
						</div>
						<div class="cmmn_white_bg">
							<article>
								<h3>Project Description</h3>
							</article>
							<div class="plot_detail_content">
								<p class="more">Brigate Ploits, A seamless amalgamation of luxury, comfort and style blend to provide a truly sophisticated lifestyle. These Residential Apartments in Bangalore are beautifully planned keeping in mind the architecture which can soothe your senses whenever you step into your house after a tiring day from work. Century Ethos by Century Real Estate in Bellary Road strives for customer satisfaction and believes in building world-class projects without compromising on quality standards, innovation and timely delivery. Brigate Ploits, A seamless amalgamation of luxury, comfort and style blend to provide a truly sophisticated lifestyle. These Residential Apartments in Bangalore are beautifully planned keeping in mind the architecture which can soothe your senses whenever you step into your house after a tiring day from work. Century Ethos by Century Real Estate in Bellary Road strives for customer satisfaction and believes in building world-class projects without compromising on quality standards, innovation and timely delivery. </p>
							</div>
						</div>
						<div class="cmmn_white_bg">
							<article>
								<h3>Documents</h3>
							</article>
							<div class="plot_detail_content">
								<div class="row">
									<div class="col-md-2">
										<div class="document-section">
											<figure>
												<a href=""><img src="images/pdf_color_icon.png" alt=""></a>
											</figure>
											<figcaption>Sale Deed</figcaption>
										</div>
									</div>
									<div class="col-md-2">
										<div class="document-section">
											<figure>
												<a href=""><img src="images/pdf_color_icon.png" alt=""></a>
											</figure>
											<figcaption>Mother Deed</figcaption>
										</div>
									</div>
									<div class="col-md-2">
										<div class="document-section">
											<figure>
												<a href=""><img src="images/pdf_color_icon.png" alt=""></a>
											</figure>
											<figcaption>Conversion Certificate</figcaption>
										</div>
									</div>
									<div class="col-md-2">
										<div class="document-section">
											<figure>
												<a href=""><img src="images/pdf_color_icon.png" alt=""></a>
											</figure>
											<figcaption>Khata Certificate</figcaption>
										</div>
									</div>
									<div class="col-md-2">
										<div class="document-section">
											<figure>
												<a href=""><img src="images/pdf_color_icon.png" alt=""></a>
											</figure>
											<figcaption>Encumbrance Certificate</figcaption>
										</div>
									</div>
									<div class="col-md-2">
										<div class="document-section">
											<figure>
												<a href=""><img src="images/pdf_color_icon.png" alt=""></a>
											</figure>
											<figcaption>Power of Attorney</figcaption>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="cmmn_white_bg before_login_documents">
							<article>
								<h3>Documents</h3>
							</article>
							<div class="plot_detail_content">
								<div class="row">
									<div class="col-md-12">
										<ul class="list-inline before_login_list">
											<li>
												<figure><img src="images/document.png" width="120" alt=""></figure>
											</li>
											<li>
												<h5><a href="javascript:void(0);" class="login_button" data-target="#login-myModal" data-toggle="modal">Login</a> To view Documents</h5>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="cmmn_white_bg">
							<article>
								<h3>Map &amp; Nearby</h3>
							</article>
							<div class="plot_detail_content">
								<div class="row">
									<div class="col-md-4 col-xs-12">
										<ul>
											<li>
												<div>
													<input type="checkbox" class="custom_checkbox" id="one">
													<label for="one" class="custom_checkbox_label">Airport</label>
												</div>
											</li>
											<li>
												<div>
													<input type="checkbox" class="custom_checkbox" id="two">
													<label for="two" class="custom_checkbox_label">Sports/Health Clubs</label>
												</div>
											</li>
											<li>
												<div>
													<input type="checkbox" class="custom_checkbox" id="three">
													<label for="three" class="custom_checkbox_label">Bus Stand</label>
												</div>
											</li>
											<li>
												<div>
													<input type="checkbox" class="custom_checkbox" id="four">
													<label for="four" class="custom_checkbox_label">Hotels</label>
												</div>
											</li>
											<li>
												<div>
													<input type="checkbox" class="custom_checkbox" id="five">
													<label for="five" class="custom_checkbox_label">Metro Station</label>
												</div>
											</li>
											<li>
												<div>
													<input type="checkbox" class="custom_checkbox" id="six">
													<label for="six" class="custom_checkbox_label">Schools</label>
												</div>
											</li>
											<li>
												<div>
													<input type="checkbox" class="custom_checkbox" id="seven">
													<label for="seven" class="custom_checkbox_label">Hospital</label>
												</div>
											</li>
											<li>
												<div>
													<input type="checkbox" class="custom_checkbox" id="eight">
													<label for="eight" class="custom_checkbox_label">Super Market</label>
												</div>
											</li>
											<li>
												<div>
													<input type="checkbox" class="custom_checkbox" id="nine">
													<label for="nine" class="custom_checkbox_label">Railway Station</label>
												</div>
											</li>
											<li>
												<div>
													<input type="checkbox" class="custom_checkbox" id="ten">
													<label for="ten" class="custom_checkbox_label">Park/Recreation</label>
												</div>
											</li>
										</ul>
									</div>
									<div class="col-md-8 col-xs-12">
										<div id="map"></div>
									</div>
								</div>
							</div>
						</div>
						<div class="cmmn_white_bg before_login_documents map_before">
							<article>
								<h3>Map &amp; Nearby</h3>
							</article>
							<div class="plot_detail_content">
								<div class="row">
									<div class="col-md-12">
										<ul class="list-inline before_login_list">
											<li>
												<figure><img src="images/map.png" width="150" alt=""></figure>
											</li>
											<li>
												<h5><a href="javascript:void(0);" class="login_button" data-target="#login-myModal" data-toggle="modal">Login</a> To view the land maps</h5>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-4 less_pad">
					<div class="property_detail_right">
						<div class="cmmn_white_bg">
							<div class="title_box"><h4>Similar Plots/Lands</h4></div>
							<ul class="similar_search">
								<li>
									<div class="row">
										<div class="col-md-5 col-xs-12">
											<figure><img src="images/similar_plot1.jpg" class="img-responsive" alt=""></figure>
										</div>
										<div class="col-md-7 col-xs-12">
											<a href="javascript:void(0);">Mahaveer Palatium</a>
											<div>
												<p class="location">Location</p>
												<h5>Doddballapur</h5>
											</div>
											<div>
												<p class="space">SPACE</p>
												<h5>174240 Sq.ft (4 Acres)</h5>
											</div>
											<div>
												<p class="price">PRICE RANGE</p>
												<h5>995/Sq.ft -  4500/Sq.ft</h5>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="row">
										<div class="col-md-5 col-xs-12">
											<figure><img src="images/similar_plot1.jpg" class="img-responsive" alt=""></figure>
										</div>
										<div class="col-md-7 col-xs-12">
											<a href="javascript:void(0);">Mahaveer Palatium</a>
											<div>
												<p class="location">Location</p>
												<h5>Doddballapur</h5>
											</div>
											<div>
												<p class="space">SPACE</p>
												<h5>174240 Sq.ft (4 Acres)</h5>
											</div>
											<div>
												<p class="price">PRICE RANGE</p>
												<h5>995/Sq.ft -  4500/Sq.ft</h5>
											</div>
										</div>
									</div>
								</li>
								<li>
									<div class="row">
										<div class="col-md-5 col-xs-12">
											<figure><img src="images/similar_plot1.jpg" class="img-responsive" alt=""></figure>
										</div>
										<div class="col-md-7 col-xs-12">
											<a href="javascript:void(0);">Mahaveer Palatium</a>
											<div>
												<p class="location">Location</p>
												<h5>Doddballapur</h5>
											</div>
											<div>
												<p class="space">SPACE</p>
												<h5>174240 Sq.ft (4 Acres)</h5>
											</div>
											<div>
												<p class="price">PRICE RANGE</p>
												<h5>995/Sq.ft -  4500/Sq.ft</h5>
											</div>
										</div>
									</div>
								</li>
							</ul>
						</div>
						<div class="cmmn_white_bg">
							<div class="title_box"><h4>Recent Plots/Lands</h4></div>
							<ul class="simple_list">
								<li>
									<div class="row">
										<div class="col-md-8">
											<h4>Mahaveer Palatium</h4>
											<p class="city_name">Doddballapur</p>
										</div>
										<div class="col-md-4 text-right">
											<a href="javascript:void(0)" class="read_more_btn">Read More</a>
										</div>
									</div>
								</li>
								<li>
									<div class="row">
										<div class="col-md-8">
											<h4>Mahaveer Palatium</h4>
											<p class="city_name">Doddballapur</p>
										</div>
										<div class="col-md-4 text-right">
											<a href="javascript:void(0)" class="read_more_btn">Read More</a>
										</div>
									</div>
								</li>
								<li>
									<div class="row">
										<div class="col-md-8">
											<h4>Mahaveer Palatium</h4>
											<p class="city_name">Doddballapur</p>
										</div>
										<div class="col-md-4 text-right">
											<a href="javascript:void(0)" class="read_more_btn">Read More</a>
										</div>
									</div>
								</li>
								<li>
									<div class="row">
										<div class="col-md-8">
											<h4>Mahaveer Palatium</h4>
											<p class="city_name">Doddballapur</p>
										</div>
										<div class="col-md-4 text-right">
											<a href="javascript:void(0)" class="read_more_btn">Read More</a>
										</div>
									</div>
								</li>
								<li>
									<div class="row">
										<div class="col-md-8">
											<h4>Mahaveer Palatium</h4>
											<p class="city_name">Doddballapur</p>
										</div>
										<div class="col-md-4 text-right">
											<a href="javascript:void(0)" class="read_more_btn">Read More</a>
										</div>
									</div>
								</li>
								<li>
									<div class="row">
										<div class="col-md-8">
											<h4>Mahaveer Palatium</h4>
											<p class="city_name">Doddballapur</p>
										</div>
										<div class="col-md-4 text-right">
											<a href="javascript:void(0)" class="read_more_btn">Read More</a>
										</div>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<footer id="inner_footer">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<ul class="text-left">
						<li><a href="javascript:void(0);">About Us</a></li>
						<li><a href="javascript:void(0);">Recent</a></li>
						<li><a href="javascript:void(0);">Updates</a></li>
						<li><a href="javascript:void(0);">Career</a></li>
						<li><a href="javascript:void(0);">Contact</a></li>
						<li><a href="javascript:void(0);">Sitemap</a></li>
					</ul>
				</div>
				<div class="col-md-6">
					<ul class="text-right">
						<li><a href="javascript:void(0);">Terms and Condition</a></li>
						<li><a href="javascript:void(0);">Privacy and Legal</a></li>
						<li>&copy; <span id="current_year"></span><a href="javascript:void(0);"> &nbsp;Verified Join ventures Pvt.Ltd.</a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>
<?php include('common/footer.php') ?>
	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCdNQvIbP21kxPzykkrLSvU-Hl3X7_F6zc&callback=initMap"></script>
	<script>
		function initMap() {
			var myLatLng = {lat: 12.954517, lng: 77.715671};
			var map = new google.maps.Map(document.getElementById('map'), {
				zoom: 13,
				center: myLatLng,
				scrollwheel: false
			});
				var marker = new google.maps.Marker({
				position: myLatLng,
				map: map,
				title: 'Pavani Royal'
			});
		}
	</script>