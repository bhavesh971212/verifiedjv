<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contact;
class ContactController extends Controller
{
    public function store(Request $request)
    {
    	$contact=new Contact($request->all());
    	$contact->save();
    }
    public function querylist()
    {
    	$contacts=Contact::all();
        return view('admin.listcontact')->with('contacts',$contacts);
    }
}