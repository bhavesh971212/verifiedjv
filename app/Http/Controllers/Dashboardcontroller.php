<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Property;
use Auth;
use App\User;
use Zipper;
use App\media;
use File;
class Dashboardcontroller extends Controller
{
    public function index()
    {
      $data['total_users']=User::get()->count();
      $data['buyers']=User::where('role',2)->count();
      $data['sellers']=User::where('role',2)->count();
      $data['property']=Property::get()->count();
      return view('admin.dashboard')->with('data',$data);
    }
    public function listproperty()
    {
        $property= Property::all();
        return view('admin.listproperty')->with('properties',$property);
    }
    Public function activate($slug)
    {
         $properties_id= Property::where('slug',$slug)->first();
         $id=$properties_id->id;
         $property= Property::find($id);
         $property->approve=1;
         $property->save();
         return redirect('property-list');
    }
    Public function deactivate($slug)
    {
         $properties_id= Property::where('slug',$slug)->first();
         $id=$properties_id->id;
         $property= Property::find($id);
         $property->approve=0;
         $property->save();
         return redirect('property-list');
    }
    Public function deal($slug)
    {
         $properties_id= Property::where('slug',$slug)->first();
         $id=$properties_id->id;
         $property= Property::find($id);
         $property->deal_status=1;
         $property->deal_date=date('Y-m-d H:i:s');
         $property->save();
         return redirect('property-list');
    }
    Public function undeal($slug)
    {
         $properties_id= Property::where('slug',$slug)->first();
         $id=$properties_id->id;
         $property= Property::find($id);
         $property->deal_status=0;
         $property->save();
         return redirect('property-list');
    }
    public function show($id)
    {
        $properties_id= Property::where('id',$id)->first();
        $id=$properties_id->id;
        $properties= Property::find($id); 
        return view('admin.property-show')->with('properties',$properties);
    }
    public function edit($id)
    {
        $properties_id= Property::where('id',$id)->first();
        $id=$properties_id->id;
        $properties= Property::find($id); 
        return view('admin.property-edit')->with('properties',$properties);
    }
    public function zipFileDownload(Request $request)
    {
       
      $zip_file_name='lsp-'.date('ymdhis').'.zip';
      $zipper = new \Chumper\Zipper\Zipper;
      foreach ($request->file as $file) 
      {
         $medias=Media::where('property_id',$file)->get();
         $property=Property::find($file);
         foreach ($medias as  $media) 
         {
            $zipper->make( public_path() . '/'.$zip_file_name )->folder($property->slug);
            if($media->type=='images')
             {
                 $files = glob( public_path() . "/$media->name" );
                 $zipper->make( public_path() . '/'.$zip_file_name)->folder($property->slug)->add( $files );
             }
             else
             {
                 $files = glob( public_path() . "/$media->name" );   
                 $zipper->make( public_path() . '/'.$zip_file_name)->folder($property->slug)->add( $files );
             }
         }
      }
      $headers = [ 'Content-Type' => 'application/octet-stream' ];
      return response()->download( public_path() .'\\'.$zip_file_name,$zip_file_name,$headers); 
    }
}