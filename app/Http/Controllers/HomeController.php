<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Partners;
use App\Media;

class HomeController extends Controller
{
    public function index(){
    	$partner = Partners::where('admin_status', 1)->orderBy('id')->get();
    	return view('user/home',compact('partner',$partner));
    }

    public function search(){
    	//print_r("okkk");exit();
    	//$partner = Partners::where('admin_status', 1)->orderBy('id')->get();
    	//return view('user/searching_page',compact('partner',$partner));
    	return view('user/search_results');

    }
}

