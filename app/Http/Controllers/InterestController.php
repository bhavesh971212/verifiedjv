<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\InterestedType;


class InterestController extends Controller
{
    public function interestlist()
    {
        //print_r("interesyt");exit();
    	// $plot = Plot::orderby('id','DESC')->get();
    	// return view('admin.listplot')->with('plot',$plot);

        $interst = InterestedType::orderby('id','DESC')->get();
        //print_r($interst);exit();
        return view('admin.interestlist')->with('interest',$interst);

    }

    public function create()
    {
    	return view('admin.add-interest');
    }

    public function store(Request $request)
    {
    	 $rules=array('interested_type'=>'required');
    	 $this->validate($request,$rules);
    	 $interest = new InterestedType;
    	 $interest->interested_type = $request->interested_type;
    	 $interest->save();
    	 $request->session()->flash('message','Succesfully Inserted Record');
    	 return redirect('interestlist');
    }

    public function edit($id)
    {
        //print_r($id);exit();
    	$interest = InterestedType::where('id',$id)->first();
    	return view('admin.edit-interest')->with('interest',$interest);
    }

    public function update(Request $request,$id)
    {
    	$rules = array('interested_type'=>'required');
    	$this->validate($request,$rules);
    	$interest =InterestedType::where('id',$id)->first();
    	$interest->interested_type = $request->interested_type;
    	$interest->save();
    	//$interest->session()->flash('message','Succesfully Updated Record');
    	return redirect('interestlist');
    }

    public function delete($id)
    {
    	$interest = InterestedType::where('id',$id)->first();
    	$interest->delete();
    	return redirect('interestlist');
    }
}
