<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Locality;
use DB;
use App\City;


class LocalityController extends Controller
{
    public function locality_list(){
    	$locality = Locality::orderBy('id','desc')->get();
    	return view('admin.list-locality',compact('locality',$locality));
    }

    public function create(){
    	$city = City::orderBy('city_name')->get();
    	return view('admin.add-locality')->with('city',$city);
    }

    public function store(Request $request){
    	$rules = array('city'=>'required' , 'locality'=>'required');
    	$this->validate($request,$rules);
    	$locality = new Locality;
    	$locality->city_id = $request->city;
    	$locality->locality_name = $request->locality;
    	$locality->save();
    	return redirect('locality/list');
    }

    public function edit($slug){
    	$city = City::orderBy('city_name')->get();
    	$locality = Locality::where('slug',$slug)->first();
    	return view('admin.edit-locality', compact('city','locality') );
    }

    public function update(Request $request,$slug){
    	$rules = array('city'=>'required' , 'locality'=>'required');
    	$this->validate($request,$rules);
    	$locality = Locality::where('slug',$slug)->first();
    	$locality->city_id = $request->city;
    	$locality->locality_name = $request->locality;
    	$locality->save();
    	return redirect('locality/list');
    }

    public function delete($slug){
    	$locality = Locality::where('slug',$slug)->first();
    	$locality->delete();
    	return redirect('locality/list');
    }

}
