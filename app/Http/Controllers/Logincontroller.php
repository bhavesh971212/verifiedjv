<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use Auth;
use Hash;

class Logincontroller extends Controller
{
  // User Side Functions
  public function userlogin(Request $request){
      if(!empty($request->lemail) && !empty($request->lpassword)){
          $user = User::where('email',$request->lemail)->first();
          if($user){
              if(Hash::check($request->lpassword , $user['password'])){
                if(Auth::attempt(['email' => $request->lemail , 'password'=>$request->lpassword , 'admin_status'=>1 , 'email_verification'=>1 ])){
                  return Auth::user(); die();
                } else{
                  echo 2; die();
                }
              } else {
                 echo 3; die();
              } 
          } else {
              echo 3; die();
          }
      } else {
          echo 4; die();
      }
  }

  public function userLogout(){
    Auth::logout();
    return redirect('/');
  } 

  // End User Side Functions

// -----------------------------------------------------------------------------------------------------------------------------------------------------------------------------

  // Admin Side Functions
    public function login()
    {
       return view('login');
    }

    public function postlogin(Request $request)
    {
      $rules=array('email'=>'required','password'=>'required');	
      $this->validate($request,$rules);

      $email=$request->email;
      $password=$request->password;
      $users=User::where('email',$email)->first();  
      if(!empty($users))
      { 
        if (Hash::check($password, $users->password)) 
        {
             if(Auth::attempt(['email'=>$request->email,'password'=>$request->password,'admin_status'=>1 , 'role'=>1]))
             {
                  return redirect('admin-dashboard');
             }
             else
                return redirect('/admin')->with('message','Incorrect Email id or Password');
        }
        else 
           return redirect('/admin')->with('message','Incorrect Email id or Password');
      }
      else
        return redirect('/admin')->with('message','Incorrect Email id or Password');
      
    }

    public function logout()
    {
    	Auth::logout();
    	return redirect('/');
    }

    // End Admin Side Functions
}
