<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Notification;
use Auth;
class NotificationController extends Controller
{
	public function index()
	{
		  $user_id=Auth::User()->id;
	    $notifications=Notification::where('user_id',$user_id)->orderBy('created_at','desc')->get();
	    $unseen_notifications_count=Notification::where('user_id',$user_id)->where('seen',0)->count();
	    dd($notifications);
    }
    public function seen_notifications()
    {
    	$user_id=Auth::User()->id;
    	$notification_seen_update=Notification::where('user_id', $user_id)
          ->where('seen',0)
          ->update(['seen' => 1]);
    }
}
