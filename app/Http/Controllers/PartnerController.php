<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Partners;
use App\Media;
use Image;

class PartnerController extends Controller
{
    public function index()
    {
    	 $partners=Partners::all();
    	 return view('admin.listpartner')->with('partners',$partners);
    }
    public function create()
    {
    	 return view('admin.add-partner');
    }
    public function store(Request $request)
    {
    	   $rules=array('title'=>'required','image'=>'required');
    	   $this->validate($request,$rules);
           $partner=new Partners;
	       $partner->title=$request->title;
           $partner->admin_status=1;
	       $partner->save();
	        if($request->hasFile('image'))
            {		  
                /*$destination = 'upload';  
		 	    $file = $request->file('image');
		    	$file->move($destination, $file->getClientOriginalName());
		        $filelocation = $destination. "/" .$file->getClientOriginalName();*/

                 $destination = 'upload';
                 $thumb = $request->file('image');
                 $filelocation = $destination. "/" .time().'.'.$thumb->getClientOriginalExtension(); 
                 //$filelocation = $destination. "/" .$thumb->getClientOriginalName(); 
                 $thumb_img = \Image::make($thumb->getRealPath())->resize(200, 200);
                 $thumb_img->save($filelocation,80);

		        $media=new Media;
		        $media->type='partner';
		        $media->property_id=$partner->id;
                $media->name=$filelocation;   
                $media->save();
		    }
		   return redirect('partnerlist');	
    }
    public function update(Request $request,$slug)
    {
    	   $rules=array('title'=>'required');
    	   $this->validate($request,$rules);
           $partner=Partners::where('slug',$slug)->first();
	       $partner->title=$request->title;
	       $partner->save();
	        if($request->hasFile('image'))
            {		  
                /*$destination = 'upload';  
                $file = $request->file('image');
                $file->move($destination, $file->getClientOriginalName());
                $filelocation = $destination. "/" .$file->getClientOriginalName();*/

                 $destination = 'upload';
                 $thumb = $request->file('image');
                 $filelocation = $destination. "/" .time().'.'.$thumb->getClientOriginalExtension();  
                 $thumb_img = \Image::make($thumb->getRealPath())->resize(200, 200);
                 $thumb_img->save($filelocation,80);

                $previousImage=Media::where('property_id',$partner->id)->where('type','partner')->select('name')->first();
                @unlink($previousImage->name);
		        $media=Media::where('property_id',$partner->id)->where('type','partner')->first();
                $media->name=$filelocation;   
                $media->save();
		    }
		   return redirect('partnerlist');	
    }
    public function edit($slug)
    {
       $partners=Partners::where('slug',$slug)->first();
       return view('admin.edit-partner')->with('partners',$partners);

    }
     public function activate($id)
    {
        $user=Partners::find($id);
        $user->admin_status=1;
        $user->save();
        return redirect('partnerlist');
    }
    public function deactivate($id)
    {
        $user=Partners::find($id);
        $user->admin_status=2;
        $user->save();
        return redirect('partnerlist');
    }
    public function partnerlist()
    {
        $partners=Partners::where('admin_status',1)->get();
        
    }
}