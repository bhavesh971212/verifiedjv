<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Plot;

class PlotController extends Controller
{
    public function plotlist(){
    	$plot = Plot::orderby('id','DESC')->get();
    	return view('admin.listplot')->with('plot',$plot);
    }

    public function create(){
    	return view('admin.add-plot');
    }

    public function store(Request $request)
    {
    	 $rules=array('title'=>'required');
    	 $this->validate($request,$rules);
    	 $plot = new Plot;
    	 $plot->title = $request->title;
    	 $plot->save();
    	 $request->session()->flash('message','Succesfully Inserted Record');
    	 return redirect('plotlist');
    }

    public function edit($slug){
    	$plot = Plot::where('slug',$slug)->first();
    	return view('admin.edit-plot')->with('plot',$plot);
    }

    public function update(Request $request,$slug){
    	$rules = array('title'=>'required');
    	$this->validate($request,$rules);
    	$plot =Plot::where('slug',$slug)->first();
    	$plot->title = $request->title;
    	$plot->save();
    	$request->session()->flash('message','Succesfully Updated Record');
    	return redirect('plotlist');
    }

    public function delete($slug){
    	$plot = Plot::where('slug',$slug)->first();
    	$plot->delete();
    	return redirect('plotlist');
    }
}
