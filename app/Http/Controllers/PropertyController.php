<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Property;
use App\Media;
use App\Requirement;
use App\Notification;
use App\PropertyType;
use App\City;
use App\State;
use App\Locality;
use App\RoadWidth;
use App\Zone;
use App\ProjectCategory;
use App\Price;

use Validator;
use Auth;
use DB;

class PropertyController extends Controller
{
    // User Side Function
    public function post_adds(){
      $property_type = PropertyType::all();
      $state = State::orderBy('state_name')->get();
      $city = City::orderBy('city_name')->get();
      $locality = Locality::all();
      $roadwidth = RoadWidth::all();
      $zone = Zone::all();
      $project_cat = ProjectCategory::all();
      $price = Price::all();
      return view('user/post_add',compact('property_type','state','city','locality','roadwidth','zone','project_cat','price'));
    }

    public function store_post_adds(Request $request){
       // dd($request->all());
         $validator = Validator::make($request->all(), [ 'status'=>'required|numeric','title'=>'required|numeric','zone'=>'required|numeric','project_cat'=>'required|numeric','property_area'=>'required|numeric','property_type'=>'required|numeric','token_amout' =>'required|numeric','token_type'=>'required|numeric','ratio_owner'=>'required|numeric','ratio_builder'=>'required|numeric','road_width'=>'required|numeric','road_width_type'=>'required|numeric','frontage'=>'required|numeric','frontage_type'=>'required|numeric','locality'=>'required','locality_lat'=>'required','locality_lng'=>'required'
        ]);
        if(!$validator->fails()){
            $property = new Property;
            $property->user_id=2;
            $property->land_title=$request->title;
            $property->land_zone=$request->zone;
            $property->project_cat_id_fk = $request->project_cat;
            $property->land_size=$request->property_area;
            $property->property_type_id_fk=$request->property_type;
            $property->token_amount=$request->token_amout;
            $property->token_price_type=$request->token_type;
            $property->ratio_owner=$request->ratio_owner;
            $property->ratio_builder=$request->ratio_builder;
            $property->road_width=$request->road_width;
            $property->road_width_type=$request->road_width_type;
            $property->frontage=$request->frontage;
            $property->frontage_type=$request->frontage_type;
            $property->locality=$request->locality;
            $property->locality_lat=$request->locality_lat;
            $property->locality_lng=$request->locality_lng;
            $property->status=$request->status;
            $property->admin_status=2;

            $property->market_value=$request->budget ? $request->budget : '0';
            $property->market_value_price_type=$request->budget_type ? $request->budget_type :'0';
            $property->land_description=$request->property_description;
            $property->other_criteria=$request->other_criteria;
            $property->far=$request->far_text;
            $property->owner_notes=$request->owner_notes;
            $property->state=$request->state ? $request->state : '0' ;
            $property->city=$request->city ? $request->city : '0' ;
            $property->address=$request->address;
            if(!empty($request->address_lat)){
                $property->address_lat=$request->address_lat;
                $property->address_lng=$request->address_lng;
            } else if(!empty($request->address_lat2) && !empty($request->address_lng2) ){
                $property->address_lat=$request->address_lat2;
                $property->address_lat=$request->address_lng2;
            }
            $property->save();
            echo 1; die();
        } else {
            return response()->json($validator->messages(), 200);
        }
    }

    // Admin Side Function
    public function create()
    {
       return view('seller.add-property');
    }
    public function myproperty()
    {
    	$user_id=Auth::User()->id;
    	$my_properties=Property::where('user_id',$user_id)->get();
    	return view('seller.my-property')->with('my_properties',$my_properties);
    }
    public function store(Request $request)
    {
    	 $rules=array('title'=>'required','details'=>'required','size'=>'required|numeric','land_title'=>'required','land_zone'=>'required|numeric','access_road_width'=>'numeric','land_frontage'=>'numeric','market_value'=>'numeric','advance'=>'numeric','sizeunit'=>'required','sizeunit'=>'required','sizeunit'=>'required','road_width_unit'=>'required','land_frontage_unit'=>'required','market_value_unit'=>'required','advance_value_unit'=>'required','market_value_unit'=>'required');
    	   if(empty($request->latitude) && empty($request->langitude))
         {
           $rules['location']='required';
           $rules['country']='required';
           $rules['postal_code']='required';
           $rules['state']='required';
           $rules['city']='required';
           $rules['route']='required';
           $rules['street_address']='required';
         }
         else
         {
            $rules['latitude']='required';
            $rules['langitude']='required';
         }
         if(!empty($request->size))
         {
             $rules['sizeunit']='required';
         }
         if(!empty($request->access_road_width))
         {
             $rules['road_width_unit']='required';
         }
         if(!empty($request->land_frontage))
         {
             $rules['land_frontage_unit']='required';
         }
         if(!empty($request->land_zone))
         {
             $rules['land_zone_unit']='required';
         }
         if(!empty($request->market_value))
         {
             $rules['market_value_unit']='required';
         }
         if(!empty($request->advance))
         {
             $rules['advance_value_unit']='required';
         }

         $this->validate($request,$rules);
	       $property=new Property;
         $property->sizeunit=$request->sizeunit;
         $property->road_width_unit=$request->road_width_unit;
         $property->land_frontage_unit=$request->land_frontage_unit;
         $property->land_zone_unit=$request->land_zone_unit;
         $property->market_value_unit=$request->market_value_unit;
         $property->advance_value_unit=$request->advance_value_unit;
	       $property->title=$request->title;
	       $property->details=$request->details;
	       $property->category=$request->project_category;
	       $property->other_category=$request->other_category;
	       $property->street_address=$request->street_address;
         $property->route=$request->route;
         $property->city=$request->city;
         $property->state=$request->state;
         $property->zipcode=$request->postal_code;
         $property->country=$request->country;
         $property->lat=$request->latitude;
         $property->lang=$request->langitude;
         $property->fsi=$request->fsi;
	       $property->land_title=$request->land_title;
	       $property->land_zone=$request->land_zone;
	       $property->land_frontage=$request->land_frontage;
	       $property->access_road_width=$request->access_road_width;
	       $property->ratio=$request->ratio;
	       $property->market_value=$request->market_value;
	       $property->advance=$request->advance;
	       $property->owner_notes=$request->owner_notes;
	       $property->size=$request->size;
	       $property->user_id=Auth::User()->id;
	       $property->save();
	       if($request->hasFile('image'))
           {
            $destination = 'upload';
		 	      $files = $request->file('image');
            foreach($files as $file)
            {
		    	       $file->move($destination, $file->getClientOriginalName());
		             $filelocation = $destination. "/" .$file->getClientOriginalName();
		             $media=new Media;
		             $media->type='images';
		             $media->property_id=$property->id;
                 $media->name=$filelocation;
                 $media->save();
             }
		   }
		     if($request->hasFile('documents'))
           {
                $destination = 'upload/document';
		 	          $files = $request->file('documents');
		 	          foreach($files as $file)
		 	          {
			             	$file->move($destination, $file->getClientOriginalName());
			              $filelocation = $destination. "/" .$file->getClientOriginalName();
			              $media=new Media;
			              $media->type='documents';
			              $media->property_id=$property->id;
	                  $media->name=$filelocation;
	                  $media->save();
                }
		       }
            $explodeloc=explode(",",$request->location);
            $locate=$explodeloc[0];
            $requirements=Requirement::where('location', 'like', '%'.$locate.'%')->get();
            foreach ($requirements as $requirement)
            {
               $notifications=new Notification;
               $notifications->property_id=$property->id;
               $notifications->user_id=$requirement->user_id;
               $notifications->requirement_id=$requirement->id;
               $notifications->message='New Flat available';
               $notifications->seen=0;
               $notifications->save();
            }
           return redirect('my-property');
    }

    public function edit($id)
    {
        //$properties_id= Property::where('id',$id)->first();
        //$id=$properties_id->id;
        $property_type = PropertyType::all();
        $state = State::orderBy('state_name')->get();
        $city = City::orderBy('city_name')->get();
        $locality = Locality::all();
        $roadwidth = RoadWidth::all();
        $zone = Zone::all();
        $project_cat = ProjectCategory::all();
        $price = Price::all();
        $properties= Property::find($id);
        return view('admin.property-edit',compact('properties','property_type','project_cat','zone','roadwidth','price'));
    }

    public function update(Request $request,$id){

      $rules = array('title'=>'required|numeric','zone'=>'required|numeric','project_cat'=>'required|numeric','property_area'=>'required|numeric','property_type'=>'required|numeric','token_amout' =>'required|numeric','token_type'=>'required|numeric','ratio_owner'=>'required|numeric','ratio_builder'=>'required|numeric','road_width'=>'required|numeric','road_width_type'=>'required|numeric','frontage'=>'required|numeric','frontage_type'=>'required|numeric','locality'=>'required','locality_lat'=>'required','locality_lng'=>'required');
      $this->validate($request,$rules);
      dd($request->all());
    }
    /*public function edit($slug)
    {
    	$properties_id= Property::where('slug',$slug)->first();
        $id=$properties_id->id;
    	$property=Property::find($id);
    	return view('seller.edit-property')->with('property',$property);
    }
    public function post_edit(Request $request,$id)
    {
       $rules=array('title'=>'required','details'=>'required','size'=>'required|numeric','land_title'=>'required','land_zone'=>'required|numeric','access_road_width'=>'numeric','land_frontage'=>'numeric','market_value'=>'numeric','advance'=>'numeric');
         if(empty($request->latitude) && empty($request->langitude))
         {
           $rules['location']='required';
           $rules['country']='required';
           $rules['postal_code']='required';
           $rules['state']='required';
           $rules['city']='required';
           $rules['route']='required';
           $rules['street_address']='required';
         }
         else
         {
            $rules['latitude']='required';
            $rules['langitude']='required';
         }
         if(!empty($request->size))
         {
             $rules['sizeunit']='required';
         }
         if(!empty($request->access_road_width))
         {
             $rules['road_width_unit']='required';
         }
         if(!empty($request->land_frontage))
         {
             $rules['land_frontage_unit']='required';
         }
         if(!empty($request->land_zone))
         {
             $rules['land_zone_unit']='required';
         }
         if(!empty($request->market_value))
         {
             $rules['market_value_unit']='required';
         }
         if(!empty($request->advance))
         {
             $rules['advance_value_unit']='required';
         }
         $this->validate($request,$rules);
  	     $property=Property::find($id);
         $property->sizeunit=$request->sizeunit;
         $property->road_width_unit=$request->road_width_unit;
         $property->land_frontage_unit=$request->land_frontage_unit;
         $property->land_zone_unit=$request->land_zone_unit;
         $property->market_value_unit=$request->market_value_unit;
         $property->advance_value_unit=$request->advance_value_unit;
         $property->title=$request->title;
         $property->details=$request->details;
         $property->category=$request->project_category;
         $property->other_category=$request->other_category;
         $property->fsi=$request->fsi;
         $property->street_address=$request->street_address;
         $property->route=$request->route;
         $property->city=$request->city;
         $property->state=$request->state;
         $property->zipcode=$request->postal_code;
         $property->country=$request->country;
         $property->land_title=$request->land_title;
         $property->land_zone=$request->land_zone;
         $property->land_frontage=$request->land_frontage;
         $property->access_road_width=$request->access_road_width;
         $property->ratio=$request->ratio;
         $property->market_value=$request->market_value;
         $property->advance=$request->advance;
         $property->owner_notes=$request->owner_notes;
         $property->size=$request->size;
         $property->user_id=Auth::User()->id;
         $property->save();
         if($request->hasFile('image'))
           {
                $destination = 'upload';
		 	          $file = $request->file('image');
		    	      $file->move($destination, $file->getClientOriginalName());
		            $filelocation = $destination. "/" .$file->getClientOriginalName();
    		        $media=new Media;
    		        $media->type='images';
    		        $media->property_id=$property->id;
                $media->name=$filelocation;
                $media->save();
		       }
		     if($request->hasFile('documents'))
           {
                $destination = 'upload/document';
		 	          $files = $request->file('documents');
      		 	    foreach($files as $file)
      		 	    {
      			    	  $file->move($destination, $file->getClientOriginalName());
      			        $filelocation = $destination. "/" .$file->getClientOriginalName();
      			        $media=new Media;
      			        $media->type='documents';
      			        $media->property_id=$property->id;
      	            $media->name=$filelocation;
      	            $media->save();
                }
		       }
       return redirect('my-property');
    }*/
    public function delete($slug)
    {
       $properties_id= Property::where('slug',$slug)->first();
       $id=$properties_id->id;
       $property=Property::find($id);
       $property->delete();
	     Media::where('property_id',$id)->delete();
	     return redirect('my-property');
    }
    public function recent_deals()
    {
       $properties_deals= Property::where('deal_status',1)->orderBy('deal_date','desc')->get();
    }
    public function property_details($slug,$loc)
    {
       /* Property Details*/
        $property_details_id= Property::where('slug',$slug)->first();
        $id=$property_details_id->id;
        $property_details=Property::find($id);

        /*Recent plots*/
        $recent_plot= Property::where('approve',1)->where('deal_status',0)->orderBy('created_at','desc')->get();

        /* similar plos*/
        $similar_plot=DB::table('property')->where('approve',1)->where('deal_status',0);
        $explodeloc=explode(",",$loc);
        $locate=$explodeloc[0];
          $similar_plot=$similar_plot->where(DB::raw('concat(street_address," ",route," ",city," ",state," ",country)') , 'LIKE' , "%$locate%");
        $similar_plots=$similar_plot->whereNotIn('slug',[$slug])->get();
        dd($similar_plots);
     }
}
