<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PropertyType;


class PropertytypeController extends Controller
{
    public function typelist()
    {
    	$property_type = PropertyType::orderby('id','DESC')->get();
        //print_r($property_type);exit();
    	return view('admin.listproperty_type')->with('property_type',$property_type);
    }

    public function create(){
    	return view('admin.add-propertytype');
    }

    public function store(Request $request)
    {

    	 $rules=array('property_type'=>'required');
    	 $this->validate($request,$rules);
    	 $property = new PropertyType;
    	 $property->property_type = $request->property_type;
         //print_r($property);exit();
    	 $property->save();
    	 $request->session()->flash('message','Succesfully Inserted Record');
    	 return redirect('property_typelist');
    }

    public function edit($id)
    {
        //print_r("okk");exit();
    	$property_type = PropertyType::where('id',$id)->first();
        //print_r($property_type);exit();
    	return view('admin.edit-propertytype')->with('property_type',$property_type);
    }

    public function update(Request $request,$id){
        //print_r("okk");exit();
    	$rules = array('property_type'=>'required');
    	$this->validate($request,$rules);
    	$property =PropertyType::where('id',$id)->first();
    	$property->property_type = $request->property_type;
    	$property->save();
    	$request->session()->flash('message','Succesfully Updated Record');
    	return redirect('property_typelist');
    }

    public function delete($id){
        //print_r("okkk");exit();
    	$property_type = PropertyType::where('id',$id)->first();
    	$property_type->delete();
    	return redirect('property_typelist');
    }
}
