<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
use App\User;
use Hash;
use Session;
use Helper;

class RegisterController extends Controller
{
	public function register()
    {
            return view('register');
    }
    
    public function postRegister(Request $request){
    	if(!empty($_POST['name']) &&  !empty($_POST['email']) && !empty($_POST['password']) && !empty($_POST['usertype']) && !empty($_POST['phone']) ) {
    		
            if(User::where('email', '=',$request->email)->count() > 0){
                echo 1; die();
            } else if(User::where('phone','=',$request->phone)->count() > 0) {
                echo 2; die();
            } else{
                $data = $request->all();
                $data['otp'] = mt_rand(100000 , 999999);
          
                Session::put('user_otp', $data); 
                Session::save();
                $message = "Your Otp is ".$data['otp']; 
                
                Helper::sendSms($request->phone,$message);
               
                $userData = array('email' => $request->email , 'phone' => $request->phone  );

                echo json_encode($userData); die();
            }    		
    	} else {
    		echo 4; die();
    	}
    }

    public function postOtpCheck(Request $request){
        if (Session::has('user_otp')) {
            $users = Session::get('user_otp');
            if($request->otpemail == $users['email'] && $request->otpphone == $users['phone']){
                if($request->otpcode == $users['otp']){
                   // dd($users);

                    $confirmation_code = mt_rand(1000000 , 9999999);
                    /*$mail_data = array(
                    'email' => $data['email'],
                    'confirmation_code' => $confirmation_code
                    );*/
                    /*Mail::send('email.verify', $mail_data, function ($message) use ($mail_data) {
                        $message->subject('Email Verification')
                                ->from('admin@verifiedjv.com')
                                ->to($mail_data['email']);
                    });
                    */

                    $user = new User;
                    $user->role = $users['usertype'];
                    $user->name = $users['name'];
                    $user->email= $users['email'];
                    $user->phone= $users['phone'];
                    $user->password = Hash::make($users['password']); 
                    $user->email_hashcode = $confirmation_code;
                    $user->email_verification = 1;
                    $user->save();
                    Session::forget('user_otp');

                     echo 1; die();
                }else{
                    echo 2; die();
                } 
            } else{
                echo 3; die();
            }
        } else {
            echo 3; die();
        } 
    }

    public function verifyEmail($confirmation_code){
        if(!$confirmation_code)
        {
            throw new InvalidConfirmationCodeException;
        }
        $user = User::where('email_hashcode',$confirmation_code)->first();
        if(!$user){
            throw new InvalidConfirmationCodeException;
        }
        $user->email_hashcode = 0;
        $user->email_verification = 1;
        $user->save();
        return redirect('/');
    }
}
