<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Hash;
class Registercontroller extends Controller
{
    public function register()
    {
            return view('register');
    }
    public function postregister(Request $request)
    {
    	$rules=array('name' =>'required','email'=>'required|','password'=>'required|min:6','password_confirmation'=>'required|min:6|same:password','phone'=>'numeric|unique:users'); 
    	$this->validate($request,$rules);
        $user=new User;
        $user->role=$request->role;  
        $user->name=$request->name;  
        $user->email=$request->email;
        $user->phone=$request->phone;
        $user->password=Hash::make($request->password);
        $user->save();
        return redirect('/')->with('message','Registration Successfull'); 
    }
}