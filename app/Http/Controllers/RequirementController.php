<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Requirement;
use Auth;

use App\InterestedType;
use App\SeekerType;
use App\City;
use App\State;
use App\Locality;
use App\Direction;
use App\ResaleType;
use App\PropertyType;
use App\UserLocality;
use App\Price;
use Validator;
use Input;
use DB;

class RequirementController extends Controller
{
  //  User Side Function
  public function post_requirement(){
      $seeker_type = SeekerType::all();
      $interested_type = InterestedType::all();
      $resale_type = ResaleType::all();
      $property_type = PropertyType::all();
      $direction = Direction::all();
      $state = State::orderBy('state_name')->get();
      $city = City::orderBy('city_name')->get();
      $locality = Locality::all();
      $price = Price::all();

      return view('user/post_requirement',compact('seeker_type','interested_type','resale_type','property_type','direction','state','city','locality','price') );
  }

  public function store_post_requirement(Request $request){
     $validator = Validator::make($request->all(), [ 'status'=>'required|numeric',
        'seeker_type'=>'required|numeric','interested_type'=>'required|numeric','resale_type'=>'required|numeric','min_plot_size'=>'required|numeric','max_plot_size'=>'required|numeric','property_type' =>'required|numeric','address'=>'required','lat'=>'required','lng'=>'required'
     ]);
      
      if(!$validator->fails()){
         
              $requirement=new Requirement;
              $requirement->user_id=2;
              $requirement->seeker_type_id_fk=$request->seeker_type;
              $requirement->interested_type_id_fk=$request->interested_type;
              $requirement->resale_type_id_fk=$request->resale_type;
              $requirement->min_plot_size=$request->min_plot_size;
              $requirement->max_plot_size=$request->max_plot_size;
              $requirement->property_type_id_fk=$request->property_type;
              $requirement->min_budget=$request->min_budget ? $request->min_budget : '0' ;
              $requirement->max_budget=$request->max_budget ? $request->max_budget : '0' ;              
              $requirement->direction_id_fk=$request->direction ? $request->direction : '0' ;
              $requirement->requirement_description = $request->requirement_description;
              $requirement->state=$request->state ? $request->state : '0' ;
              $requirement->city=$request->city ? $request->city : '0' ;
              $requirement->status=$request->status;
              $requirement->admin_status=2;
              $requirement->save();

              $insertedId = $requirement->id;

              $userlocality = [];
              for($i=0;$i<count($request->address);$i++){
              $userlocality[] = ['requirement_id_fk' => $insertedId,
                                'locality_name'=>$request->address[$i] ,
                                 'locality_lat'=>$request->lat[$i] ,
                                'locality_lng'=>$request->lng[$i]
                      ];
              }
              /*foreach($request->local as $l){
                  $userlocality[] = [
                      'requirement_id_fk' => $insertedId,
                      'locality_id_fk' => $l
                  ];
              }*/
              DB::table('user_locality')->insert($userlocality);

        /*if($request->status == 1)
         {

         } else if($request->status == 2){

         }*/
      } else {
           return response()->json($validator->messages(), 200);
      }
  }

  public function getCityByState(Request $request){
      $state_id = $request->state;
      $city = City::where('state_id',$state_id)->get();
      $str = '<option value="">Select State</option>';
      foreach ($city as $key => $value) {
         $str .= '<option value="'.$value->city_id.'">'.$value->city_name.'</option>';
      }
      echo $str; die();
  }

  public function getLocalityByCity(Request $request){
      $city_id = $request->city;
      $locality = Locality::where('city_id',$city_id)->get();
      $str = '<option value="">Select City Localtiy</option>';
      foreach ($locality as $key => $value) {
         $str .= '<option value="'.$value->id.'">'.$value->locality_name.'</option>';
      }
      echo $str; die();
  }
  //  End User Side Function

  //  Admin Side Function
	public function index()
	{
		   $requirements=Requirement::all();
       return view('admin.listrequirement',compact('requirements')); 
  }
  public function requirementlist()
  {
       $requirements=Requirement::where('admin_status',1)->get();
  }
  public function activate($id)
  {
      $user=Requirement::find($id);
      $user->admin_status=1;
      $user->save();
      return redirect('requirementslist');
  }
  public function deactivate($id)
  {
      $user=Requirement::find($id);
      $user->admin_status=2;
      $user->save();
      return redirect('requirementslist');
  }
  public function create()
  {
    	 return view('buyer.add-requirement');
  }
  public function store(Request $request)
  {
      $rules=array('title'=>'required','minsize'=>'required|numeric','maxsize'=>'required|numeric','minprice'=>'required|numeric','maxprice'=>'required|numeric',
        'location'=>'required','sizeunit'=>'required','priceunit'=>'required');
      $this->validate($request,$rules);
      $explodeloc=explode(",",$request->location);
      $requirement=new Requirement;
      $requirement->title=$request->title;
      $requirement->min_plot_size=$request->minsize;
      $requirement->max_plot_size=$request->maxsize;
      $requirement->min_budget=$request->minprice;
      $requirement->max_budget=$request->maxprice;
      $requirement->full_location=$request->location;
      $requirement->location=$explodeloc[0];
      $requirement->admin_status=2;
      $requirement->user_id=Auth::id();  
      $requirement->priceunit=$request->priceunit;
      $requirement->sizeunit=$request->sizeunit;
      $requirement->save();
  }
  public function edit($slug)
  {   
      $requirement= Requirement::where('slug',$slug)->first();
      $id=$requirement->id;
      $requirements=Requirement::find($id);
      return view('buyer.edit-requirement')->with('requirents',$requirements); 
  }
  public function update(Request $request,$id)
  {
     $rules=array('title'=>'required','minsize'=>'required|numeric','maxsize'=>'required|numeric','minprice'=>'required|numeric','maxprice'=>'required|numeric','location'=>'required','sizeunit'=>'required','priceunit'=>'required');
      $this->validate($request,$rules);
      $requirement=Requirement::find($id);
      $explodeloc=explode(",",$request->location);
      $requirement->title=$request->title;
      $requirement->min_plot_size=$request->minsize;
      $requirement->max_plot_size=$request->maxsize;
      $requirement->min_budget=$request->minprice;
      $requirement->max_budget=$request->maxprice;
      $requirement->full_location=$request->location;
      $requirement->location=$explodeloc[0];
      $requirement->priceunit=$request->priceunit;
      $requirement->sizeunit=$request->sizeunit;
      $requirement->save();
  }

  //  End Admin Side Function
}
