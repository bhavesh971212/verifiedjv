<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Property;
use App\Media;
use DB;
class SearchController extends Controller
{
    public function search()
    {
        return view('buyer.search');
    }
    public function postsearch(Request $request)
    {   
        $property=DB::table('property')->where('approve',1)->where('deal_status',0);
        if(isset($request->location))
        {   
            $explodeloc=explode(",",$request->location);
            $loc=$explodeloc[0];
         	$property=$property->where(DB::raw('concat(street_address," ",route," ",city," ",state," ",country)') , 'LIKE' , "%$loc%"); 
        }
        $properties=$property->get();
        return view('buyer.result-search')->with(['properties'=>$properties,'loc'=>$request->location]);
    }
    public function filter(Request $request)
    {
        $property=DB::table('property')->where('approve',1)->where('deal_status',0);
        if(isset($request->location))
        {   
            $explodeloc=explode(",",$request->location);
            $loc=$explodeloc[0];
            $property=$property->where(DB::raw('concat(street_address," ",route," ",city," ",state," ",country)') , 'LIKE' , "%$loc%"); 
        }
        if(isset($request->min_price) && isset($request->max_price))
        {   
            $property=$property->whereBetween('market_value', array($request->min_price,$request->max_price));
        }
        $properties=$property->get();
       dd($properties);
    }    
}	