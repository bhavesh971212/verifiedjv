<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    public function index()
    {
    	$users_lists=User::all();
        return view('admin.listuser')->with('userlists',$users_lists);
    }
    public function activate($id)
    {
    	$user=User::find($id);
        $user->admin_status=1;
        $user->save();
        return redirect('user-list');
    }
    public function deactivate($id)
    {
    	$user=User::find($id); 
    	$user->admin_status=0;
        $user->save();
        return redirect('user-list');
    }
}
