<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Zone;

class ZoneController extends Controller
{
    public function zonelist()
    {
    	$zone = Zone::orderby('id','DESC')->get();
    	return view('admin.listzone')->with('zone',$zone);
    }

    public function create()
    {
    	return view('admin.add-zone');
    }

    public function store(Request $request)
    {
    	 $rules=array('zone_type'=>'required');
    	 $this->validate($request,$rules);
    	 $zone = new Zone;
    	 $zone->zone_type = $request->zone_type;
    	 $zone->save();
    	 $request->session()->flash('message','Succesfully Inserted Record');
    	 return redirect('zonelist');
    }

    public function edit($id)
    {
    	$zone = Zone::where('id',$id)->first();
    	return view('admin.edit-zone')->with('zone',$zone);
    }

    public function update(Request $request,$id)
    {
    	$rules = array('zone_type'=>'required');
    	$this->validate($request,$rules);
    	$zone =Zone::where('id',$id)->first();
    	$zone->zone_type = $request->zone_type;
    	$zone->save();
    	$request->session()->flash('message','Succesfully Updated Record');
    	return redirect('zonelist');
    }

    public function delete($id)
    {
    	$zone = Zone::where('id',$id)->first();
    	$zone->delete();
    	return redirect('zonelist');
    }
}
