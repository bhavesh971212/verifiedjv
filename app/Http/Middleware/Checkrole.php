<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
class Checkrole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        
        if (Auth::check()) {
            $user = Auth::user();
            if ($user->role != 2) 
            {
                return redirect('/')->with('message','you dont have Privilege to do This');
            }
        }
        return $next($request);
    }
}
