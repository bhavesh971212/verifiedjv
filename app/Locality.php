<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Locality extends Model
{
	use Sluggable;
    protected $table = "locality";

    public function sluggable(){
    	return [
    		'slug' => [
    			'source' => 'locality_name'
    		]
    	];
    }

    public function city(){
    	return $this->hasOne('App\City', 'city_id', 'city_id');
    }

}
