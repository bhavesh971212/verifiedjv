<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
class Partners extends Model
{
   use Sluggable;	
   protected $table='partners';
   public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }   

    public function partnermedia()
    {
        return $this->hasone('App\Media','property_id')->where('type','=', 'partner');
    }
}
