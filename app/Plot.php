<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

class Plot extends Model
{
	use Sluggable;
    protected $table = "plots"; 
    public function sluggable(){
    	return [
    		'slug' => [
    			'source' => 'title'
    		]
    	];
    }
}
