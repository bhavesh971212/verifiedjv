<?php
namespace App;
use Illuminate\Database\Eloquent\Model;
//use Cviebrock\EloquentSluggable\Sluggable;
class Property extends Model
{
   //use Sluggable;
   protected $table='property';

   public function project_category(){
        return $this->hasOne('App\ProjectCategory','id','project_cat_id_fk'   );
   }

   public function property_type(){
        return $this->hasOne('App\PropertyType','id','property_type_id_fk'   );
   }

   public function property_title(){
        return $this->hasOne('App\Zone','id','land_title');
   }

   public function property_zone(){
        return $this->hasOne('App\Zone','id','land_zone');
   }

   public function property_roadwidth(){
        return $this->hasOne('App\RoadWidth','id','road_width_type');
   }

   public function property_frontage(){
        return $this->hasOne('App\RoadWidth','id','frontage_type');
   }

   public function property_price(){
        return $this->hasOne('App\Price','id','market_value_price_type');
   }

   public function property_token_price(){
        return $this->hasOne('App\Price','id','token_price_type');
   }

   public function imagemedia()
    {
        return $this->hasMany('App\Media','property_id')->where('type','=', 'images');
    }
    public function documentmedia()
    {
        return $this->hasMany('App\Media','property_id')->where('type','=', 'documents');
    }
    /*public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }*/
    public function user()
    {
      return $this->belongsTo('App\User', 'user_id');
    }
}
