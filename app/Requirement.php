<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\UserLocality;

class Requirement extends Model
{   	
    protected $table='user_requirements';

    public function user()
    {
      return $this->belongsTo('App\User', 'user_id');
    }

    public function property_type(){
    	return $this->belongsTo('App\PropertyType', 'property_type_id_fk');
    }

    public function user_locality(){
    	return $this->hasMany('App\UserLocality', 'requirement_id_fk');
    }
}