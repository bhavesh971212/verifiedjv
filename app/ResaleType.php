<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResaleType extends Model
{
    protected $table = "resale_type";
}
