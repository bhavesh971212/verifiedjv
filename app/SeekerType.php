<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeekerType extends Model
{
    protected $table='seeker_type';
}
