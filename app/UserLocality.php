<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserLocality extends Model
{
   	protected $table = "user_locality";
   	public $timestamps = false;

   	public function locality(){
   		return $this->belongsTo('App\Locality', 'locality_id_fk');
   	}

}
