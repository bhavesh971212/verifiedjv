<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePropertyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('property', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('title');
            $table->string('slug',255);
            $table->text('details');
            $table->string('category');
            $table->string('fsi');
            $table->string('land_title');
            $table->string('land_zone');
            $table->string('land_frontage');
            $table->string('access_road_width');
            $table->string('other_category');
            $table->string('ratio');
            $table->bigInteger('market_value');
            $table->bigInteger('advance');
            $table->text('owner_notes');
            $table->text('admin_notes')->nullable();
            $table->string('size');
            $table->integer('approve')->default(0);
            $table->string('street_address');
            $table->string('route');
            $table->string('city');
            $table->string('state');
            $table->integer('zipcode');
            $table->string('country');
            $table->integer('deal_status')->default(0);
            $table->dateTime('deal_date')->nullable();
            $table->string('lat')->nullable();
            $table->string('lang')->nullable();
            $table->string('road_width_unit')->nullable();
            $table->string('land_frontage_unit')->nullable();
            $table->string('land_zone_unit')->nullable();
            $table->string('market_value_unit')->nullable();
            $table->string('advance_value_unit')->nullable();
            $table->string('sizeunit')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
