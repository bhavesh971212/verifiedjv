<?php

use Illuminate\Database\Seeder;
use App\User;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         DB::table('users')->insert([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'phone' => 1234567899,
            'role' => 1,
            'admin_status' => 1,
            'password' =>Hash::make('123456'),
            'email_hashcode' => 0
        ]);
    }
}
