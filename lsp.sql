-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 28, 2017 at 12:14 PM
-- Server version: 10.1.8-MariaDB
-- PHP Version: 5.6.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `lsp`
--

-- --------------------------------------------------------

--
-- Table structure for table `contact`
--

CREATE TABLE `contact` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile` bigint(20) NOT NULL,
  `email_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contact`
--

INSERT INTO `contact` (`id`, `name`, `mobile`, `email_id`, `message`, `created_at`, `updated_at`) VALUES
(1, 'sumit', 8050147365, 'sk20900@gmail.com', 'hiiiiiiiiiiiii', '2017-02-25 06:48:15', '2017-02-25 06:48:15');

-- --------------------------------------------------------

--
-- Table structure for table `media`
--

CREATE TABLE `media` (
  `id` int(10) UNSIGNED NOT NULL,
  `property_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `media`
--

INSERT INTO `media` (`id`, `property_id`, `name`, `type`, `created_at`, `updated_at`) VALUES
(49, 11, 'upload/Chrysanthemum.jpg', 'images', '2017-02-28 06:29:18', '2017-02-28 06:29:18'),
(50, 11, 'upload/document/Koala.jpg', 'documents', '2017-02-28 06:29:18', '2017-02-28 06:29:18'),
(51, 12, 'upload/Chrysanthemum.jpg', 'images', '2017-02-28 06:31:14', '2017-02-28 06:31:14'),
(52, 12, 'upload/document/Koala.jpg', 'documents', '2017-02-28 06:31:14', '2017-02-28 06:31:14'),
(53, 13, 'upload/Chrysanthemum.jpg', 'images', '2017-02-28 06:31:38', '2017-02-28 06:31:38'),
(54, 13, 'upload/document/Koala.jpg', 'documents', '2017-02-28 06:31:38', '2017-02-28 06:31:38'),
(55, 14, 'upload/Chrysanthemum.jpg', 'images', '2017-02-28 06:33:57', '2017-02-28 06:33:57'),
(56, 14, 'upload/document/Koala.jpg', 'documents', '2017-02-28 06:33:57', '2017-02-28 06:33:57'),
(57, 15, 'upload/Chrysanthemum.jpg', 'images', '2017-02-28 06:35:50', '2017-02-28 06:35:50'),
(58, 15, 'upload/document/Koala.jpg', 'documents', '2017-02-28 06:35:50', '2017-02-28 06:35:50'),
(59, 16, 'upload/Chrysanthemum.jpg', 'images', '2017-02-28 06:37:45', '2017-02-28 06:37:45'),
(60, 16, 'upload/document/Koala.jpg', 'documents', '2017-02-28 06:37:45', '2017-02-28 06:37:45'),
(61, 17, 'upload/Chrysanthemum.jpg', 'images', '2017-02-28 06:37:57', '2017-02-28 06:37:57'),
(62, 17, 'upload/document/Koala.jpg', 'documents', '2017-02-28 06:37:57', '2017-02-28 06:37:57'),
(63, 18, 'upload/Chrysanthemum.jpg', 'images', '2017-02-28 06:38:48', '2017-02-28 06:38:48'),
(64, 18, 'upload/document/Koala.jpg', 'documents', '2017-02-28 06:38:48', '2017-02-28 06:38:48'),
(65, 19, 'upload/Chrysanthemum.jpg', 'images', '2017-02-28 06:43:42', '2017-02-28 06:43:42'),
(66, 19, 'upload/document/Koala.jpg', 'documents', '2017-02-28 06:43:42', '2017-02-28 06:43:42'),
(67, 2, 'upload/Koala.jpg', 'partner', '2017-02-28 10:07:00', '2017-02-28 10:07:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2017_02_02_052404_aad_column_users_table', 2),
(4, '2017_02_07_044143_create-property-table', 3),
(5, '2017_02_07_072616_create-media-table', 4),
(6, '2017_02_08_102208_create-requirement-table', 5),
(7, '2017_02_08_120357_create-notifications-table', 6),
(8, '2017_02_14_084218_create_partners_table', 7),
(9, '2017_02_25_114331_contact', 8);

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE `notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `property_id` int(11) NOT NULL,
  `requirement_id` int(11) NOT NULL,
  `message` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seen` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notifications`
--

INSERT INTO `notifications` (`id`, `user_id`, `property_id`, `requirement_id`, `message`, `seen`, `created_at`, `updated_at`) VALUES
(2, 1, 18, 10, 'New Flat available', 0, '2017-02-28 06:38:49', '2017-02-28 06:38:49'),
(3, 1, 18, 11, 'New Flat available', 0, '2017-02-28 06:38:49', '2017-02-28 06:38:49'),
(4, 1, 19, 10, 'New Flat available', 0, '2017-02-28 06:43:42', '2017-02-28 06:43:42'),
(5, 1, 19, 11, 'New Flat available', 0, '2017-02-28 06:43:43', '2017-02-28 06:43:43');

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_status` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `title`, `slug`, `admin_status`, `created_at`, `updated_at`) VALUES
(2, 'new partner', 'new-partner', 1, '2017-02-28 10:06:59', '2017-02-28 10:06:59');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `property`
--

CREATE TABLE `property` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fsi` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `land_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `land_zone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `land_frontage` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_road_width` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `other_category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ratio` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `market_value` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `advance` bigint(20) NOT NULL,
  `owner_notes` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_notes` text COLLATE utf8mb4_unicode_ci,
  `size` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `approve` int(11) NOT NULL DEFAULT '0',
  `deal_status` int(255) NOT NULL DEFAULT '0',
  `deal_date` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `street_address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `route` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `zipcode` int(11) NOT NULL,
  `lat` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lang` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `road_width_unit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `land_frontage_unit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `land_zone_unit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `market_value_unit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `advance_value_unit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sizeunit` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `property`
--

INSERT INTO `property` (`id`, `user_id`, `title`, `slug`, `details`, `category`, `fsi`, `land_title`, `land_zone`, `land_frontage`, `access_road_width`, `other_category`, `ratio`, `market_value`, `advance`, `owner_notes`, `admin_notes`, `size`, `approve`, `deal_status`, `deal_date`, `created_at`, `updated_at`, `street_address`, `route`, `city`, `state`, `zipcode`, `lat`, `lang`, `road_width_unit`, `land_frontage_unit`, `land_zone_unit`, `market_value_unit`, `advance_value_unit`, `sizeunit`, `country`) VALUES
(11, 1, 'new title', 'new-title', 'no property detail', 'Apartments', '111', 'Agriculture', '111', '111', '111', '1111', '111', '111', 111, '111', NULL, '11', 0, 0, NULL, '2017-02-28 06:29:18', '2017-02-28 06:29:18', 'JP Nagar', '7th phase', 'Bengaluru', 'KA', 560078, '11', '1111', 'Feet', 'Feet', 'Agriculture', 'Crores', 'Crores', 'Acre', 'India'),
(12, 1, 'new title', 'new-title-1', 'no property detail', 'Apartments', '111', 'Agriculture', '111', '111', '111', '1111', '111', '111', 111, '111', NULL, '11', 0, 0, NULL, '2017-02-28 06:31:14', '2017-02-28 06:31:14', 'JP Nagar', '7th phase', 'Bengaluru', 'KA', 560078, '11', '1111', 'Feet', 'Feet', 'Agriculture', 'Crores', 'Crores', 'Acre', 'India'),
(13, 1, 'new title', 'new-title-2', 'no property detail', 'Apartments', '111', 'Agriculture', '111', '111', '111', '1111', '111', '111', 111, '111', NULL, '11', 0, 0, NULL, '2017-02-28 06:31:37', '2017-02-28 06:31:37', 'JP Nagar', '7th phase', 'Bengaluru', 'KA', 560078, '11', '1111', 'Feet', 'Feet', 'Agriculture', 'Crores', 'Crores', 'Acre', 'India'),
(14, 1, 'new title', 'new-title-3', 'no property detail', 'Apartments', '111', 'Agriculture', '111', '111', '111', '1111', '111', '111', 111, '111', NULL, '11', 0, 0, NULL, '2017-02-28 06:33:57', '2017-02-28 06:33:57', 'JP Nagar', '7th phase', 'Bengaluru', 'KA', 560078, '11', '1111', 'Feet', 'Feet', 'Agriculture', 'Crores', 'Crores', 'Acre', 'India'),
(15, 1, 'new title', 'new-title-4', 'no property detail', 'Apartments', '111', 'Agriculture', '111', '111', '111', '1111', '111', '111', 111, '111', NULL, '11', 0, 0, NULL, '2017-02-28 06:35:49', '2017-02-28 06:35:49', 'JP Nagar', '7th phase', 'Bengaluru', 'KA', 560078, '11', '1111', 'Feet', 'Feet', 'Agriculture', 'Crores', 'Crores', 'Acre', 'India'),
(16, 1, 'new title', 'new-title-5', 'no property detail', 'Apartments', '111', 'Agriculture', '111', '111', '111', '1111', '111', '111', 111, '111', NULL, '11', 0, 0, NULL, '2017-02-28 06:37:45', '2017-02-28 06:37:45', 'JP Nagar', '7th phase', 'Bengaluru', 'KA', 560078, '11', '1111', 'Feet', 'Feet', 'Agriculture', 'Crores', 'Crores', 'Acre', 'India'),
(17, 1, 'new title', 'new-title-6', 'no property detail', 'Apartments', '111', 'Agriculture', '111', '111', '111', '1111', '111', '111', 111, '111', NULL, '11', 0, 0, NULL, '2017-02-28 06:37:56', '2017-02-28 06:37:56', 'JP Nagar', '7th phase', 'Bengaluru', 'KA', 560078, '11', '1111', 'Feet', 'Feet', 'Agriculture', 'Crores', 'Crores', 'Acre', 'India'),
(18, 1, 'new title', 'new-title-7', 'no property detail', 'Apartments', '111', 'Agriculture', '111', '111', '111', '1111', '111', '111', 111, '111', NULL, '11', 0, 0, NULL, '2017-02-28 06:38:48', '2017-02-28 06:38:48', 'JP Nagar', '7th phase', 'Bengaluru', 'KA', 560078, '11', '1111', 'Feet', 'Feet', 'Agriculture', 'Crores', 'Crores', 'Acre', 'India'),
(19, 1, 'new title', 'new-title-8', 'no property detail', 'Apartments', '111', 'Agriculture', '111', '111', '111', '1111', '111', '111', 111, '111', NULL, '11', 0, 0, NULL, '2017-02-28 06:43:42', '2017-02-28 06:43:42', 'JP Nagar', 'ggg', 'Bengaluru', 'KA', 560078, '11', '1111', 'Feet', 'Feet', 'Agriculture', 'Crores', 'Crores', 'Acre', 'India');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` int(11) NOT NULL,
  `phone` int(11) NOT NULL,
  `admin_status` int(11) NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `role`, `phone`, `admin_status`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@gmail.com', '$2y$10$.We9hVIzBsanho/tC.vzFunl7VQvi.c/7Q4nKmmnt/OnpazgRDEp.', 1, 0, 1, '8CRsk2OkXSvVcJTOZNieiOZj7WEariNnQj8FUTo20VBxCOTVUHSN6hnmRYla', NULL, NULL),
(2, 'sumit', 'sumit@gmail.com', '$2y$10$.We9hVIzBsanho/tC.vzFunl7VQvi.c/7Q4nKmmnt/OnpazgRDEp.', 2, 0, 1, 'YCp2o2tzQZ4iguIMrUOM8OvR5DlHXmzSqpjpnwm20ZV4AfPKv6TmFWvGJnNz', '2017-02-06 06:59:02', '2017-02-14 04:13:18'),
(3, 'buyer', 'buyer@gmail.com', '$2y$10$HgG8VaYDOhzjCljyb0wg4.Eblk3UYp5ybFqDBjDZh2oFna9TkfVHS', 3, 1234567890, 1, 'nXW8MLAFc8N3IUiEW3nH2wKNE7mBKPy04bXShJdZze7lLmWpB6WbFAX7uq1y', '2017-02-08 04:44:25', '2017-02-16 06:59:16');

-- --------------------------------------------------------

--
-- Table structure for table `user_requirements`
--

CREATE TABLE `user_requirements` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `min_budget` int(11) NOT NULL,
  `max_budget` int(11) NOT NULL,
  `min_plot_size` int(11) NOT NULL,
  `max_plot_size` int(11) NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `full_location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `admin_status` int(11) NOT NULL DEFAULT '0',
  `priceunit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sizeunit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `user_requirements`
--

INSERT INTO `user_requirements` (`id`, `user_id`, `title`, `slug`, `min_budget`, `max_budget`, `min_plot_size`, `max_plot_size`, `location`, `full_location`, `admin_status`, `priceunit`, `sizeunit`, `created_at`, `updated_at`) VALUES
(10, 1, 'new requirementt', 'new-requirement', 1, 10, 10, 100, 'JP Nagar', 'JP Nagar', 2, 'Lakh', 'Acre', '2017-02-28 05:00:47', '2017-02-28 06:07:39'),
(11, 1, 'new requirement', 'new-requirement-1', 1, 10, 10, 100, 'JP Nagar', 'JP Nagar', 2, 'Lakh', 'Acre', '2017-02-28 06:05:20', '2017-02-28 06:05:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `media`
--
ALTER TABLE `media`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notifications`
--
ALTER TABLE `notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `property`
--
ALTER TABLE `property`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_requirements`
--
ALTER TABLE `user_requirements`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `media`
--
ALTER TABLE `media`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `notifications`
--
ALTER TABLE `notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `property`
--
ALTER TABLE `property`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `user_requirements`
--
ALTER TABLE `user_requirements`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
