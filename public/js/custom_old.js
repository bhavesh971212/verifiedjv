$(function(){
	// CURRENT YEAR
	var currentYear = (new Date).getFullYear();
	$('#current_year').html(currentYear);
	// FIXED FORM JS
	var speed = 700;
	$('.animate_div_click img').on('click', function(e){
		var div_name= $(this).data('attr');
		var $$ = $(this),
		panelWidth = $('.'+div_name).outerWidth();
			if( $$.is('.myButton') ){
				$('.'+div_name).css('z-index',1);
				$('.'+div_name).animate({left:-400}, speed);
				$$.removeClass('myButton')
			} else {
				$('.'+div_name).css('z-index',50);
				$('.'+div_name).animate({left:0}, speed);
				$$.addClass('myButton')
			}
	});
	// SHOME MORE AND LESS CONTENT
	var showChar = 250;
	var moretext = "Show More";
	var ellipsestext = "...";
	var lesstext = "Show Less";
	$('.more').each(function(){
		var content = $(this).html();
		if(content.length > showChar){
			var c = content.substr(0, showChar);
			var h = content.substr(showChar, content.length - showChar);
			var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
			$(this).html(html);
		}
	});
	$(".morelink").click(function(){
		if($(this).hasClass("less")) {
			$(this).removeClass("less");
			$(this).html(moretext);
		} else {
			$(this).addClass("less");
			$(this).html(lesstext);
		}
			$(this).parent().prev().toggle();
			$(this).prev().toggle();
		return false;
	});
	// LOAD MORE NUMBER SHOW
	$('ul.site_gallery li:not(:last)').hide().filter(':lt(2)').show();
	var total_site_img = $('ul.site_gallery li').size();
	var remain_site_img = total_site_img - 3;
	$('.special_load #total_length').html(remain_site_img);
	// BOOTSTRAP SELECT TAG
	var mySelect = $('#first-disabled2');
	$('#special').on('click', function () {
		mySelect.find('option:selected').prop('disabled', true);
		mySelect.selectpicker('refresh');
	});
	$('#special2').on('click', function () {
		mySelect.find('option:disabled').prop('disabled', false);
		mySelect.selectpicker('refresh');
	});
	$('#basic2').selectpicker({
		liveSearch: true,
		maxOptions: 1
	});

	$('#city,#state,#locality').selectpicker({
		liveSearch: true,
		maxOptions: 1,
		size:6
	});
	// SELECT AND APPEND CITY NAME
	$('.select_location').on('change', function(){
		var optionSelected = $("option:selected", this);
		var valueSelected = this.value;
		$('.append_area ul').append('<li><div>'+valueSelected+'<span><i class="fa fa-times-circle-o fa-2x" aria-hidden="true"></i></span></div></li>');
	});
	// MODAL ISSUES FIXING
	$('.modal').on('hidden.bs.modal', function (e) {
		$("body").css("padding-right","0");
		if($('.modal.in').length > 0){
			$('body').addClass('modal-open');
		}
	});
	// TAB HIGHLIGHTING
	$('.header-content ul li .arrow-down').hide().eq(0).show();
	$('.header-content ul li .arrow-down2').hide().eq(0).show();
	$(".header-content ul li").click(function(){
		$('.header-content ul li .arrow-down').hide().eq($(this).index()).show();
		$('.header-content ul li .arrow-down2').hide().eq($(this).index()).show();
	});
	// OWL CAROUSEL
	$('#sp-two,#sp-three').owlCarousel({
		autoplay:true,
		loop: false,
		margin: 10,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:3
			},
			1000:{
				items:4
			}
		}
	});
	$('#sp-one').owlCarousel({
		autoplay:true,
		loop: true,
		margin: 10,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:3
			},
			1000:{
				items:6
			}
		}
	});

});