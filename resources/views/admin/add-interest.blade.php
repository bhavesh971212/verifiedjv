@extends('layouts.master')
@section('content')

<div class="bs-example">
    <h1 class="text-center">Add Interest</h1>
    <form class="form-horizontal" action="{{ URL::to('interest/add') }}" method="post">
        
        <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Interest</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="interested_type" placeholder="Interest Name" name="interested_type" value="{{ old('interested_type') }}">
                <span style="color:red;">{{ $errors->first('interested_type')  }}</span>
            </div>
        </div>
       {{ csrf_field() }}
        
        <div class="form-group">
            <div class="col-xs-offset-2 col-xs-10">
                <button type="submit" class="btn btn-primary">submit</button>
            </div>
        </div>
    </form>
</div>
@endsection