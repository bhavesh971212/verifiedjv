@extends('layouts.master')
@section('content')
<div class="bs-example">
    <h1 class="text-center">Add Locality</h1>
    <form class="form-horizontal" action="{{ URL::to('locality/add') }}" method="post">
         {{ csrf_field() }}
        <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Select City</label>
            <div class="col-xs-10">
                <select name="city" class="form-control">
                    @foreach($city as $c)
                        <option value="{{ $c->city_id }}">{{ $c->city_name }}</option>
                    @endforeach
                </select>
                <span style="color:red;">{{$errors->first('city')}}</span>
            </div>
        </div>
       
       <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Enter Locality</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputEmail" placeholder="Enter Locality" name="locality" value="{{ old('locality') }}">
                <span style="color:red;">{{$errors->first('locality')}}</span>
            </div>
        </div>

        <div class="form-group">
            <div class="col-xs-offset-2 col-xs-10">
                <button type="submit" class="btn btn-primary">submit</button>
            </div>
        </div>
    </form>
</div>
@endsection 
