@extends('layouts.master')
@section('content')
<div class="bs-example">
    <h1 class="text-center">Add Partners</h1>
    <form class="form-horizontal" enctype="multipart/form-data" action='{{URl("partners/add")}}' method="post">
         {{ csrf_field() }}
        <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Title</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputEmail" placeholder="Title" name="title" value="{{ old('title') }}">
                <span style="color:red;">{{$errors->first('title')}}</span>
            </div>
        </div>
       
        <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Upload Image</label>
            <div class="col-xs-10">
                <input type="file" class="form-control" id="inputEmail" placeholder="size"
                name="image">
                <span style="color:red;">{{$errors->first('image')}}</span>
            </div>
        </div>
        <div class="form-group">
            <div class="col-xs-offset-2 col-xs-10">
                <button type="submit" class="btn btn-primary">submit</button>
            </div>
        </div>
    </form>
</div>
@endsection 
