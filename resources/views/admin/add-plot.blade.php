@extends('layouts.master')
@section('content')

<div class="bs-example">
    <h1 class="text-center">Add Plot Type</h1>
    <form class="form-horizontal" action="{{ URL::to('plot/add') }}" method="post">
        
        <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Plot Type</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputEmail" placeholder="Plot Type Name" name="title" value="{{ old('title') }}">
                <span style="color:red;">{{ $errors->first('title')  }}</span>
            </div>
        </div>
       {{ csrf_field() }}
        
        <div class="form-group">
            <div class="col-xs-offset-2 col-xs-10">
                <button type="submit" class="btn btn-primary">submit</button>
            </div>
        </div>
    </form>
</div>
@endsection