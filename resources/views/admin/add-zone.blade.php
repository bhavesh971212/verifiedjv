@extends('layouts.master')
@section('content')

<div class="bs-example">
    <h1 class="text-center">Add Zone Type</h1>
    <form class="form-horizontal" action="{{ URL::to('zone/add') }}" method="post">
        
        <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Zone Type</label>
             <div class="col-xs-10">
                <input type="text" class="form-control" id="zone_type" placeholder="Zone Type Name" name="zone_type" value="{{ old('zone_type') }}">
                <span style="color:red;">{{ $errors->first('zone_type')  }}</span>
            </div>
        </div>
       {{ csrf_field() }}
        
        <div class="form-group">
            <div class="col-xs-offset-2 col-xs-10">
                <button type="submit" class="btn btn-primary">submit</button>
            </div>
        </div>
    </form>
</div>
@endsection