@extends('layouts.master')
@section('content')
  <div class="row tile_count">
    <div class="col-md-3 col-sm-3 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-user"></i> Total Users</span>
      <div class="count">{{ $data['total_users']}}</div>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-user-md"></i> Total Buyers</span>
      <div class="count green">{{ $data['buyers']}}</div>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-user"></i> Total Sellers</span>
      <div class="count">{{ $data['sellers']}}</div>
    </div>
    <div class="col-md-3 col-sm-3 col-xs-6 tile_stats_count">
      <span class="count_top"><i class="fa fa-users"></i> Total Properties</span>
      <div class="count">{{ $data['property']}}</div>
    </div>
  </div>
  <!-- /top tiles -->
@endsection
