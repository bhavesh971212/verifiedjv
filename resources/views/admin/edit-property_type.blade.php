@extends('layouts.master')
@section('content')

<div class="bs-example">
    <h1 class="text-center">Edit Property Type</h1>
<form class="form-horizontal" action="{{ URL::to('property_type/edit/'.$property_type->id) }}" method="post">
        
        <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Property Type</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="property_type" placeholder="Interest Name" name="property_type" value="{{ $property_type->property_type }}">
                <span style="color:red;">{{ $errors->first('property_type')  }}</span>
            </div>
        </div>
       {{ csrf_field() }}
        
        <div class="form-group">
            <div class="col-xs-offset-2 col-xs-10">
                <button type="submit" class="btn btn-primary">submit</button>
            </div>
        </div>
</form>
</div>
@endsection