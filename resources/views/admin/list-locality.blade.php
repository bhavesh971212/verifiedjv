@extends('layouts.master')
@section('content')

	<div class="row">
		<a class="btn btn-primary" href="{{ URL::to('locality/add') }}">Add Locality</a>
		<h1 class="text-center">Locality List</h1>
		<table id="partners-table" class="table table-bordered table-hover">
			<thead>
				<th>Sr No</th>
				<th>City Name</th>
				<th>Locality Name</th>
				<th>Action</th>
			</thead>
			<?php $count = 0; ?>
			@foreach($locality as $l)
			<tr> 
				<td>{{ ++$count }}</td>
				<td> {{ $l->city->city_name  }} </td>
				<td> {{ $l->locality_name }}</td>
                <td><a class="btn btn-primary" href="{{ URL::to('locality/edit/'.$l->slug) }}">Edit</a><a class="btn btn-danger" onclick="return confirm('Are you sure, you want to delete record')" href="{{ URL::to('locality/delete/'.$l->slug) }}">Delete</a></td>
			</tr>
			@endforeach

		</table>
	</div>
@endsection 	