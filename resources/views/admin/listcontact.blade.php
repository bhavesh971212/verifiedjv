@extends('layouts.master')
@section('content')
	<div class="row">
		<h1 class="text-center">Query List</h1>
		<table id="partners-table" class="table table-bordered table-hover">
			<thead>
				<th>Name</th>
				<th>Mobile</th>
				<th>Email</th>
				<th>Message</th>
			</thead>
			@foreach ($contacts as $d)
			<tr> 
				<td>{{$d->name}}</td>
				<td>{{$d->mobile}}</td>
				<td>{{$d->email_id}}</td>
				<td>{{$d->message}}</td>
                <!-- <td><?php if($d->admin_status==1) {?> <a class="btn btn-primary" href="{{ URL::to('partners/deactivate/'.$d->id) }}" onclick="return confirm('Are you sure U want to Deactivate?')">Deactivate</a> <?php } else {?><a class="btn btn-primary" href="{{ URL::to('partners/activate/'.$d->id) }}" onclick="return confirm('Are you sure U want to Activate?')">Activate</a> <?php } ?><a class="btn btn-primary" href="{{ URL::to('partners/edit/'.$d->slug) }}">Edit</a></td> -->
			</tr>
			@endforeach
		</table>
	</div>
@endsection 	