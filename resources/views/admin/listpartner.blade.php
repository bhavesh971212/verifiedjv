@extends('layouts.master')
@section('content')

	<div class="row">
		<a class="btn btn-primary" href="/partners/add">Add Partners</a>
		<h1 class="text-center">Partner List</h1>
		<table id="partners-table" class="table table-bordered table-hover">
			<thead>
				<th>Title</th>
				<th>logo</th>
				<th>Admin Status</th>
				<th>Action</th>
			</thead>
			@foreach ($partners as $d)
			<tr> 
				<td>{{ $d->title }}</td>
				<td><img height="150px" weight="150px" src="{{URL($d->partnermedia->name)}}"></td>
				<td>{{ $d->admin_status==1 ? 'Active' : 'Inactive' }}</td>
                <td><?php if($d->admin_status==1) {?> <a class="btn btn-primary" href="{{ URL::to('partners/deactivate/'.$d->id) }}" onclick="return confirm('Are you sure U want to Deactivate?')">Deactivate</a> <?php } else {?><a class="btn btn-primary" href="{{ URL::to('partners/activate/'.$d->id) }}" onclick="return confirm('Are you sure U want to Activate?')">Activate</a> <?php } ?><a class="btn btn-primary" href="{{ URL::to('partners/edit/'.$d->slug) }}">Edit</a></td>
			</tr>
			@endforeach
		</table>
	</div>
@endsection 	