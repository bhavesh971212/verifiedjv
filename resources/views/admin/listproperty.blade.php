@extends('layouts.master')
@section('content')
	<div class="row">
		<h1 class="text-center">Properties List</h1>
		 <form role="form" method="post" action='{{URl("zipFileDownload")}}' >
		<table id="property-table" class="table table-bordered table-hover">
			<thead>
				<th>Choose File to download</th>
				<th>UserName</th>
				<th>Phone</th>
				<th>Email</th>
				<th>Description</th>
				<th>location</th>
				<th>Category</th>
				<th>Size</th>
				<th>FAR</th>
				<th>Land Title</th>
				<th>Land Zone</th>
				<th>Land Frontage</th>
				<th>Access Road Width</th>
				<th>Other Category</th>
				<th>Ratio Owner : Builder</th>
				<th>Market Value</th>
				<th>Token Amount</th>
				<th>Owner Notes</th>
				<th>Approve status</th>
				<th>Deal status</th>
				<th>Actions</th>
			</thead>
			@foreach ($properties as $d)
			<tr>
				<td><input type="checkbox" name="file[]" class="cv_file" value={{$d->id}}></td>
				<td>{{ $d->user->name }}</td>
				<td>{{ $d->user->phone }}</td>
				<td>{{ $d->user->email }}</td>
				<td>{{ $d->land_description or 'No Description' }}</td>
				<td>{{ $d->locality }}</td>
				<td>{{ $d->project_category->project_cat_type }}</td>
				<td>{{ $d->land_size }} {{ $d->property_type->property_type }}</td>
				<td>{{ $d->far or 'Nothing' }}</td>
				<td>{{ $d->property_title->zone_type }}</td>
				<td>{{ $d->property_zone->zone_type }}</td>
				<td>{{ $d->frontage }} {{ $d->property_frontage->road_width_type }}</td>
				<td>{{ $d->road_width }} {{ $d->property_roadwidth->road_width_type }}</td>
				<td>{{ $d->other_criteria or 'Nothing' }}</td>
				<td>{{ $d->ratio_owner }} : {{ $d->ratio_builder }}</td>
				<td> @if($d->market_value != 0)
						{{ $d->market_value }} {{ $d->property_price->price_type }}
					 @else
					 	{{ 'Not Disclosed' }}
					 @endif
						</td>
				<td>{{ $d->token_amount }} {{ $d->property_token_price->price_type }}</td>
				<td>{{ $d->owner_notes or 'Nothing' }}</td>
				<td>{{ $d->approve==1 ? 'Approved' : 'Not Approve' }}</td>
				<td>{{ $d->deal_status==1 ? 'Completed' : 'Pending' }}</td>
                <td><?php if($d->approve==1)
                {?><a class="btn btn-primary" onclick="return confirm('Are you sure U want to Disapprove?')" href="{{ URL::to('/property/deactivate/'.$d->id) }}">DisApprove</a> <?php }else{?> <a class="btn btn-primary" onclick="return confirm('Are you sure U want to Approve?')" href="{{ URL::to('/property/activate/'.$d->id) }}">Approve</a> <?php }?> <a class="btn btn-primary" href="{{ URL::to('property-list/show/'.$d->id) }}">View</a>
                	<a class="btn btn-primary" href="{{ URL::to('property-list/edit/'.$d->id) }}">Edit</a>
                	<?php if($d->deal_status==1) {?><a class="btn btn-primary" onclick="return confirm('Are you sure U want to open the deal?')" href="{{ URL::to('property/undeal/'.$d->id) }}">UnDeal</a> <?php }else{?><a class="btn btn-primary" onclick="return confirm('Are you sure U want to close the deal?')"href="{{ URL::to('property/deal/'.$d->id) }}">Deal close</a><?php }?></td>
			</tr>
			@endforeach
		</table>
		<br>
		<button type="submit" id="download_file" class="btn btn-info">Download Files</button>
		</form>
	</div>
@endsection
