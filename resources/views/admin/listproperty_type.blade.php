@extends('layouts.master')
@section('content')

@if(Session::has('message'))
	<p class="alert alert-success">{{ Session::get('message') }}</p>
@endif
	<div class="row">
		<a class="btn btn-primary" href="{{ URL::to('property_type/add') }}">Add Property Type</a>
		<h1 class="text-center">Property Type List</h1>
		<table id="partners-table" class="table table-bordered table-hover">
			<thead>
				<th>Sr No</th>
				<th>Title</th>
				<th>Action</th>
			</thead>
			<?php $i=0 ?>
			@foreach($property_type as $p)
			<tr> 
				<td>{{ ++$i }}</td>
				<td>{{ $p->property_type }}</td>
                <td><a class="btn btn-primary" href="{{ URL::to('property_type/edit/'.$p->id) }}">Edit</a>
					<a class="btn btn-danger" onclick="return confirm('Are You Sure , You Want To Delete This Record')" href="{{ URL::to('property_type/delete/'.$p->id) }}">Delete</a>
                </td>
			</tr>
			@endforeach
		</table>
	</div>
@endsection