@extends('layouts.master')
@section('content')
	<div class="row">
		<h1 class="text-center">Requirments  List</h1>
		<table id="partners-table" class="table table-bordered table-hover">
			<thead>
				<th>Sr No</th>
				<th>User Name</th>
				<th>Budget</th>
				<th>Plot Size</th>
				<th>Localities</th>
				<th>Admin Status</th>
				<th>Action</th>
			</thead>
			<?php $i =0;  ?>
			@foreach ($requirements as $d)				
			<tr> 
				<td>{{ ++$i }}</td>
				<td>{{ $d->user->name }}</td>
				<td>{{ $d->min_budget }} - {{ $d->max_budget }}</td>
				<td>{{ $d->min_plot_size }} - {{ $d->max_plot_size }} {{ $d->property_type->property_type }} </td>
				<td> @for($i = 0; $i< count($d->user_locality);$i++)
						{{$i}} ) {{ $d->user_locality[$i]->locality_name}} 
						<?php echo "<br>"; ?>
					 @endfor
				</td>
				<td>{{ $d->admin_status==1 ? 'Active' : 'Inactive' }}</td>
                <td><?php if($d->admin_status==1) {?> <a class="btn btn-primary" href="{{ URL::to('requirements/deactivate/'.$d->id) }}" onclick="return confirm('Are you sure U want to Deactivate?')">Deactivate</a> <?php } else {?><a class="btn btn-primary" href="{{ URL::to('requirements/activate/'.$d->id) }}" onclick="return confirm('Are you sure U want to Activate?')">Activate</a> <?php } ?></td>
			</tr>
			@endforeach
		</table>
	</div>
@endsection 	