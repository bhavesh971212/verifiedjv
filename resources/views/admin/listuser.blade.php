@extends('layouts.master')

@section('content')
	<div class="row">
		<h1 class="text-center">Users  List</h1>
		<table id="users-table" class="table table-bordered table-hover">
			<thead>
				<th>Name</th>
				<th>Email</th>
				<th>Phone</th>
				<th>Role</th>
				<th>Admin Status</th>
				<th>Action</th>
			</thead>
			@foreach ($userlists as $d)
			<?php 
				 if($d->role==1)
				   continue;
				?> 
			<tr> 
				<td>{{ $d->name }}</td>
				<td>{{ $d->email }}</td>
				<td>{{ $d->phone }}</td>
				<td>{{ $d->role==2 ? 'Seller' : 'Buyer' }}</td>
				<td>{{ $d->admin_status==1 ? 'Active' : 'Inactive' }}</td>
                <td><?php if($d->admin_status==1) {?> <a class="btn btn-primary" href="{{ URL::to('user/deactivate/'.$d->id) }}" onclick="return confirm('Are you sure U want to Deactivate?')">Deactivate</a> <?php } else {?><a class="btn btn-primary" href="{{ URL::to('user/activate/'.$d->id) }}" onclick="return confirm('Are you sure U want to Activate?')">Activate</a> <?php } ?></td>
			</tr>
			@endforeach
		</table>
	</div>
@endsection 	