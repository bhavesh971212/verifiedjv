@extends('layouts.master')
@section('content')
	<div class="row">
		<h1 class="text-center">Property Details</h1>
		<div class="col-xs-12" role="tabpanel" data-example-id="togglable-tabs">
			<div id="myTabContent" class="tab-content">
				<div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
					<form action="{{ URL::to('property-list/edit/{$properties->id}') }}" method="post" >
						{{ csrf_field() }}
					<table class="table table-bordered table-hover">
						<tr>
							<th class="col-xs-2">User Name</th>
							<td class="col-xs-10">{{ $properties->user->name }}</td>
						</tr>
						<tr>
							<th class="col-xs-2">Email</th>
							<td class="col-xs-10">{{ $properties->user->email }}</td>
						</tr>
						<tr>
							<th class="col-xs-2">Phone</th>
							<td class="col-xs-10">{{ $properties->user->phone }}</td>
						</tr>
						<tr>
							<th class="col-xs-2">Description</th>
							<td class="col-xs-10"><textarea class="form-control" rows="3" name="property_description">{{$properties->land_description}}</textarea>
							</td>
						</tr>
						<tr>
							<th class="col-xs-2">Size</th>
							<td class="col-xs-10">
									<input type="text" name="property_area" value="{{ $properties->land_size }}"  class="numbers_with_decimal">
									<select name="property_type">
												<option value="">Select Units</option>
										@foreach($property_type as $pt)
											<option value="{{ $pt->id }}"  {{ $properties->property_type->id == $pt->id ? 'selected="Selected"':'' }}  >{{ $pt->property_type }}
											</option>
										@endforeach
									</select>
							</td>
						</tr>
						<tr>
							<th class="col-xs-2">Category</th>
							<td class="col-xs-10">
								<select name="project_cat">
										<option>Select Category</option>
										@foreach($project_cat as $pc)
										<option value="{{$pc->id}}" {{ $properties->project_category->id == $pc->id ? 'selected="Selected"':''}} >{{$pc->project_cat_type}}</option>
										@endforeach
								</select>
							</td>
						</tr>
						<tr>
							<th class="col-xs-2">Land Title</th>
							<td class="col-xs-10">
								<select name="title">
										<option>Select Title</option>
										@foreach($zone as $z)
										<option value="{{$z->id}}" {{ $properties->property_title->id == $z->id ? 'selected="Selected"':''}}>{{$z->zone_type}}</option>
										@endforeach
								</select>
							</td>
						</tr>
						<tr>
							<th class="col-xs-2">Land Zone</th>
							<td class="col-xs-10">
								<select name="zone">
										<option>Select Zone</option>
										@foreach($zone as $z)
										<option value="{{$z->id}}" {{ $properties->property_zone->id == $z->id ? 'selected="Selected"':''}} >{{$z->zone_type}}</option>
										@endforeach
								</select>
							</td>
						</tr>
						<tr>
							<th class="col-xs-2">Land Frontage</th>
							<td class="col-xs-10">
								<input type="text" name="frontage"  class="numbers_with_decimal" value="{{ $properties->frontage }}">
								<select name="frontage_type">
										<option>Select Frontage</option>
										@foreach($roadwidth as $r)
										<option value="{{$r->id}}" {{$properties->property_frontage->id == $r->id ? 'selected="Selected"' : '' }} >{{$r->road_width_type}}</option>
										@endforeach
								</select>
							</td>
						</tr>
						<tr>
							<th class="col-xs-2">Road Width</th>
							<td class="col-xs-10">
								<input type="text" name="road_width"  class="numbers_with_decimal" value="{{$properties->road_width}}">
								<select name="road_width_type">
										<option>Select Width</option>
										@foreach($roadwidth as $r)
										<option value="{{$r->id}}" {{$properties->property_roadwidth->id == $r->id ? 'selected="Selected"' : '' }}>{{$r->road_width_type}}</option>
										@endforeach
								</select>
							</td>
						</tr>
						<tr>
							<th class="col-xs-2">FAR</th>
							<td class="col-xs-10">
								<textarea class="form-control" rows="2" name="far">{{ $properties->far }}</textarea>
							</td>
						</tr>
						<tr>
							<th class="col-xs-2">Location</th>
							<td class="col-xs-10">
							<input type="text" class="form-control autocomplete" name="locality"  value="{{$properties->locality}}">
 					    </td>
						</tr>
						<tr>
							<th class="col-xs-2">Location Latitude</th>
							<td class="col-xs-10">
								<input type="text" name="locality_lat"  class="form-control locality_lat" value="{{$properties->locality_lat}}">
							</td>
						</tr>
						<tr>
							<th class="col-xs-2">Location Longitude</th>
							<td class="col-xs-10">
							<input type="text" name="locality_lng"  class="form-control locality_lng" value="{{$properties->locality_lng}}">
							</td>
						</tr>
						<tr>
							<th class="col-xs-2">Address</th>
							<td class="col-xs-10">
								<textarea class="form-control" rows="3" name="address">{{ $properties->address }}</textarea>
							</td>
						</tr>
						<tr>
							<th class="col-xs-2">Address Latitude</th>
							<td class="col-xs-10">
								<input type="text" name="address_lat"  class="form-control" value="{{$properties->address_lat}}">
							</td>
						</tr>
						<tr>
							<th class="col-xs-2">Address Longitude</th>
							<td class="col-xs-10">
								<input type="text" name="address_lng"  class="form-control" value="{{$properties->address_lng}}">
							</td>
						</tr>
						<tr>
							<th class="col-xs-2">Other Criteria</th>
							<td class="col-xs-10">
										<input type="text" name="other_criteria"  class="form-control" value="{{$properties->other_criteria}}">
							</td>
						</tr>
						<tr>
							<th class="col-xs-2">Ratio Owner:Bulder</th>
							<td class="col-xs-10">
								<input type="text"  name="ratio_owner" class="only_numbers ratio_owner" value="{{ $properties->ratio_owner }}">
								<input type="text"  name="ratio_builder" class="only_numbers ratio_builder" value="{{ $properties->ratio_builder }}">
								<span class="invalid_ratio"></span>
							</td>
						</tr>
						<tr>
							<th class="col-xs-2">Market Value</th>
							<td class="col-xs-10">
								<input type="text"  name="budget" class="only_numbers" value="{{ $properties->market_value }}">
								<select id="basic" class="selectpicker" name="budget_type">
									<option value="">Select Price Unit</option>
										@foreach($price as $p)
											<option value="{{ $p->id }}" {{$properties->market_value_price_type == $p->id ? 'selected="Selected"' : '' }}>{{ $p->price_type }}
											</option>
										@endforeach
								</select>
						</td>
						</tr>
						<tr>
							<th class="col-xs-2">Token Amount</th>
							<td class="col-xs-10">
								<input type="text"  name="token_amout" class="only_numbers" value="{{$properties->token_amount}}">
								<select id="basic" class="selectpicker" name="token_type">
									<option value="">Select Area</option>
									@foreach($price as $p)
										<option value="{{ $p->id }}" {{$properties->token_price_type == $p->id ? 'selected="Selected"' : '' }}>{{ $p->price_type }}
										</option>
									@endforeach
								</select>
							</td>
						</tr>
						<tr>
							<th class="col-xs-2">Owner Notes</th>
							<td class="col-xs-10">
								<textarea class="form-control" rows="3" name="owner_notes">{{ $properties->owner_notes }}</textarea>
							</td>
						</tr>
						<tr>
							<th class="col-xs-2">Admin Notes</th>
							<td class="col-xs-10">
								<textarea class="form-control" rows="3" name="admin_notes">{{ $properties->admin_notes }}</textarea>
							</td>
						</tr>
					     @foreach($properties->imagemedia as $media)
					     	<tr>
						     	<th class="col-xs-2">Image</th>
									<td class="col-xs-10"><img src='{{URL("$media->name")}}' style="height: 200px;width: 200px";></img></td>
								</th>
							</tr>
                         @endforeach
                          @foreach($properties->documentmedia as $media)
					     	<tr>
						     	<th class="col-xs-2">document</th>
									<td class="col-xs-10"><img src='{{URL("$media->name")}}' style="height: 200px;width: 200px";></img></td>
								</th>
							</tr>
                         @endforeach
					</table>
					<input type ="submit" class="btn btn-success" value="Update">
				</form>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		$(document).ready(function(){
				$('.autocomplete').geocomplete()
													.bind("geocode:result", function(event, result){
															var lat = result.geometry.location.lat();
															var lng = result.geometry.location.lng();
															var add = result.formatted_address;
															$('.locality_lat').val(lat);
															$('.locality_lng').val(lng);
								});

				$('.ratio_owner').keyup(function(){
							 		$('invalid_ratio').html('');
							 		if($(this).val() > 100 || $(this).val() < 0){
							 			$('.invalid_ratio').html('Invalid Range');
							 			return false;
							 		} else{
							 			var ratio_builder = 100-$(this).val();
							 			$('.ratio_builder').val(ratio_builder);
							 		}

			 });
		});

	</script>
@endsection
