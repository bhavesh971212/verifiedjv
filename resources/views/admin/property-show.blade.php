@extends('layouts.master')
@section('content')
	<div class="row">
		<h1 class="text-center">Property Details</h1>
		<div class="col-xs-12" role="tabpanel" data-example-id="togglable-tabs">
			<div id="myTabContent" class="tab-content">
				<div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
					<table class="table table-bordered table-hover">
						<tr>
							<th class="col-xs-2">User Name</th>
							<td class="col-xs-10">{{ $properties->user->name }}</td>
						</tr>
						<tr>
							<th class="col-xs-2">Email</th>
							<td class="col-xs-10">{{ $properties->user->email }}</td>
						</tr>
						<tr>
							<th class="col-xs-2">Phone</th>
							<td class="col-xs-10">{{ $properties->user->phone }}</td>
						</tr>
						<tr>
							<th class="col-xs-2">Details</th>
							<td class="col-xs-10">{{ $properties->land_description or 'Nothing' }}</td>
						</tr>
						<tr>
							<th class="col-xs-2">Size</th>
							<td class="col-xs-10">{{ $properties->land_size }} {{ $properties->property_type->property_type }}</td>
						</tr>
						<tr>
							<th class="col-xs-2">Category</th>
							<td class="col-xs-10">{{ $properties->project_category->project_cat_type }}</td>
						</tr>
						<tr>
							<th class="col-xs-2">Land Title</th>
							<td class="col-xs-10">{{ $properties->property_title->zone_type }}</td>
						</tr>
						<tr>
							<th class="col-xs-2">Land Zone</th>
							<td class="col-xs-10">{{ $properties->property_zone->zone_type }} </td>
						</tr>
						<tr>
							<th class="col-xs-2">Land Frontage</th>
							<td class="col-xs-10">{{ $properties->frontage }} {{ $properties->property_frontage->road_width_type }}</td>
						</tr>
						<tr>
							<th class="col-xs-2">Road Width</th>
							<td class="col-xs-10">{{ $properties->road_width }} {{ $properties->property_roadwidth->road_width_type }}</td>
						</tr>
						<tr>
							<th class="col-xs-2">FAR</th>
							<td class="col-xs-10">{{ $properties->far or 'Nothing' }} </td>
						</tr>
						<tr>
							<th class="col-xs-2">Location</th>
							<td class="col-xs-10">{{ $properties->locality }} </td>
						</tr>
						<tr>
							<th class="col-xs-2">Location Latitude</th>
							<td class="col-xs-10">{{ $properties->locality_lat }} </td>
						</tr>
						<tr>
							<th class="col-xs-2">Location Longitude</th>
							<td class="col-xs-10">{{ $properties->locality_lng }} </td>
						</tr>
						<tr>
							<th class="col-xs-2">Address</th>
							<td class="col-xs-10">{{ $properties->address or 'Nothing' }} </td>
						</tr>
						<tr>
							<th class="col-xs-2">Address Latitude</th>
							<td class="col-xs-10">{{ $properties->address_lat or 'Nothing' }} </td>
						</tr>
						<tr>
							<th class="col-xs-2">Address Longitude</th>
							<td class="col-xs-10">{{ $properties->address_lng or 'Nothing' }} </td>
						</tr>
						<tr>
							<th class="col-xs-2">Other Criteria</th>
							<td class="col-xs-10">{{ $properties->other_criteria or 'Nothing' }}</td>
						</tr>
						<tr>
							<th class="col-xs-2">Ratio Owner:Bulder</th>
							<td class="col-xs-10">{{ $properties->ratio_owner }} : {{ $properties->ratio_builder }}</td>
						</tr>
						<tr>
							<th class="col-xs-2">Market Value</th>
							<td class="col-xs-10">
							 @if($properties->market_value != 0)
								{{ $properties->market_value }} {{ $properties->property_price->price_type }}
					 		@else
					 			{{ 'Not Disclosed' }}
					 		@endif
						</td>
						</tr>
						<tr>
							<th class="col-xs-2">Token Amount</th>
							<td class="col-xs-10">{{ $properties->token_amount }} {{ $properties->property_token_price->price_type }}</td>
						</tr>
						<tr>
							<th class="col-xs-2">Owner Notes</th>
							<td class="col-xs-10">{{ $properties->owner_notes or 'Nothing' }}</td>
						</tr>
						<tr>
							<th class="col-xs-2">Admin Notes</th>
							<td class="col-xs-10">{{ $properties->admin_notes or 'Nothing' }}</td>
						</tr>
					     @foreach($properties->imagemedia as $media)
					     	<tr>
						     	<th class="col-xs-2">Image</th>
									<td class="col-xs-10"><img src='{{URL("$media->name")}}' style="height: 200px;width: 200px";></img></td>
								</th>
							</tr>
                         @endforeach
                          @foreach($properties->documentmedia as $media)
					     	<tr>
						     	<th class="col-xs-2">document</th>
									<td class="col-xs-10"><img src='{{URL("$media->name")}}' style="height: 200px;width: 200px";></img></td>
								</th>
							</tr>
                         @endforeach
					</table>
				</div>
			</div>
		</div>
	</div>
@endsection
