<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Example of Bootstrap 3 Horizontal Form Layout</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> 
<style type="text/css">
    .bs-example{
      margin: 20px;
    }
  /* Fix alignment issue of label on extra small devices in Bootstrap 3.2 */
    .form-horizontal .control-label{
        padding-top: 7px;
    }
</style>
<style type="text/css">
/* Always set the map height explicitly to define the size of the div
 * element that contains the map. */
#map {
  height: 100%;
}
/* Optional: Makes the sample page fill the window. */
html, body {
  height: 100%;
  margin: 0;
  padding: 0;
}
</style>
<link type="text/css" rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500">
<style>
  #locationField, #controls {
    position: relative;
    width: 480px;
  }
  #autocomplete {
    position: absolute;
    top: 0px;
    left: 0px;
    width: 99%;
  }
  .label {
    text-align: right;
    font-weight: bold;
    width: 100px;
    color: #303030;
  }
  #address {
    border: 1px solid #000090;
    background-color: #f0f0ff;
    width: 480px;
    padding-right: 2px;
  }
  #address td {
    font-size: 10pt;
  }
  .field {
    width: 99%;
  }
  .slimField {
    width: 80px;
  }
  .wideField {
    width: 200px;
  }
  #locationField {
    height: 20px;
    margin-bottom: 2px;
  }
</style>
</head>
<body>
    <form role="form" class="form-horizontal" method="POST" action='{{URL("edit-requirements/$requirents->id")}}'>
              {{ csrf_field() }}
              <h1>Buyers Requirements</h1>
        <div class="form-group">
          <label for="inputEmail" class="control-label col-xs-2">Title</label>
          <div class="col-xs-10">
              <input class="form-control" name="title" value="{{$requirents->title}}" placeholder="Enter title" type="text" value="{{$requirents->title}}"></input>
              <span style="color:red;">{{$errors->first('title')}}</span>
          </div>
        </div>        
        <div class="form-group">
          <label for="inputEmail" class="control-label col-xs-2">Location</label>
          <div class="col-xs-10">
              <input id="autocomplete" class="form-control" name="location" value="{{$requirents->location}}" placeholder="Enter your address" onFocus="geolocate()" type="text"></input>
              <span style="color:red;">{{$errors->first('location')}}</span>
          </div>
        </div>
        <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Min size</label>
            <div class="col-xs-10">
                <input id="autocomplete" class="form-control" name="minsize" value="{{ $requirents->min_plot_size}}" placeholder="Enter your minimum plot size"  type="text"></input>
                <span style="color:red;">{{$errors->first('minsize')}}</span>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Size Unit</label>
            <div class="col-xs-10">
                <select name="sizeunit">
                  <option value="">Select size unit</option>  
                  <option value="Acre"   @if($requirents->sizeunit=="Acre") {{ 'selected' }} @endif>Acre</option>
                  <option value="Bigha"  @if($requirents->sizeunit=="bigha") {{ 'selected' }} @endif >Bigha</option>
                  <option value="Kattah" @if($requirents->sizeunit=="Kattah") {{ 'selected' }} @endif>Kattah</option>
                  <option value="Square" @if($requirents->sizeunit=="Square") {{ 'selected' }} @endif>Square</option>
                  <option value="Feet"   @if($requirents->sizeunit=="Feet") {{ 'selected' }} @endif>Feet</option>
                  <option value="Square yards" @if($requirents->sizeunit=="Square yards") {{ 'selected' }} @endif>Square yards</option>
                  <option value="Square meters" @if($requirents->sizeunit=="Square meters") {{ 'selected' }} @endif>Square meters</option>
                  <option value="Ground" @if($requirents->sizeunit=="Ground") {{ 'selected' }} @endif>Ground</option>
                  <option value="Ghunta" @if($requirents->sizeunit=="Ghunta") {{ 'selected' }} @endif>Ghunta</option>
                </select>
                 <span style="color:red;">{{$errors->first('sizeunit')}}</span>
         </div>
        </div>
        <div class="form-group">
          <label for="inputEmail" class="control-label col-xs-2">Max size</label>
          <div class="col-xs-10">
              <input id="autocomplete" class="form-control" name="maxsize" value="{{ $requirents->max_plot_size }}" placeholder="Enter your maximum plot size"  type="text"></input>
              <span style="color:red;">{{$errors->first('maxsize')}}</span>
          </div>
         </div>
         <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Size Unit</label>
            <div class="col-xs-10">
                <select name="sizeunit">
                  <option value="">Select size unit</option>  
                  <option value="Acre"   @if($requirents->sizeunit=="Acre") {{ 'selected' }} @endif>Acre</option>
                  <option value="Bigha"  @if($requirents->sizeunit=="bigha") {{ 'selected' }} @endif >Bigha</option>
                  <option value="Kattah" @if($requirents->sizeunit=="Kattah") {{ 'selected' }} @endif>Kattah</option>
                  <option value="Square" @if($requirents->sizeunit=="Square") {{ 'selected' }} @endif>Square</option>
                  <option value="Feet"   @if($requirents->sizeunit=="Feet") {{ 'selected' }} @endif>Feet</option>
                  <option value="Square yards" @if($requirents->sizeunit=="Square yards") {{ 'selected' }} @endif>Square yards</option>
                  <option value="Square meters" @if($requirents->sizeunit=="Square meters") {{ 'selected' }} @endif>Square meters</option>
                  <option value="Ground" @if($requirents->sizeunit=="Ground") {{ 'selected' }} @endif>Ground</option>
                  <option value="Ghunta" @if($requirents->sizeunit=="Ghunta") {{ 'selected' }} @endif>Ghunta</option>
                </select>
                 <span style="color:red;">{{$errors->first('sizeunit')}}</span>
         </div>
        </div>
         <div> 
          <div class="form-group">
          <label for="inputEmail" class="control-label col-xs-2">Min Price</label>
          <div class="col-xs-10">
              <input id="autocomplete" class="form-control" name="minprice" value="{{ $requirents->min_budget }}" placeholder="Enter your minimum Price range" type="text"></input>
              <span style="color:red;">{{$errors->first('minprice')}}</span>
          </div>
        </div>
        <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Min price Unit</label>
            <div class="col-xs-10">
                <select name="priceunit">
                  <option value="">Select Min price Unit</option>  
                  <option value="Crores" @if($requirents->priceunit=="Crores") {{ 'selected' }} @endif>Crores</option>
                  <option value="Lakh" @if($requirents->priceunit=="Lakh") {{ 'selected' }} @endif>Lakh</option>
                </select>
                 <span style="color:red;">{{$errors->first('priceunit')}}</span>
           </div>
        </div>
        <div>
          <div class="form-group">
          <label for="inputEmail" class="control-label col-xs-2">Max Price</label>
          <div class="col-xs-10">
              <input id="autocomplete" class="form-control" name="maxprice" value="{{ $requirents->max_budget }}" placeholder="Enter your maximum Price range" type="text"></input>
              <span style="color:red;">{{$errors->first('maxprice')}}</span>
          </div>
        </div>
         </div>  
               <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Max price Unit</label>
            <div class="col-xs-10">
                <select name="priceunit">
                  <option value="">Select Max price Unit</option>  
                  <option value="Crores" @if($requirents->priceunit=="Crores") {{ 'selected' }} @endif>Crores</option>
                  <option value="Lakh" @if($requirents->priceunit=="Lakh") {{ 'selected' }} @endif>Lakh</option>
                </select>
                 <span style="color:red;">{{$errors->first('priceunit')}}</span>
           </div>
        </div>  
                <div class="form-group">
                  <div class="col-md-6">
                    <button type="submit" class="btn btn-default submit">
                      Submit
                    </button>
                  </div>
              </div>
              <div class="clearfix"></div>
            </form>
    </body> 
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBqAa1nsNtFCwNnGj8_ux8XOsTFGzypHQs&libraries=places&callback=initAutocomplete"
        async defer></script>
 <script type="text/javascript">
function initAutocomplete() 
{
  autocomplete = new google.maps.places.Autocomplete(
  (document.getElementById('autocomplete')),
      {types: ['geocode']});
  autocomplete.addListener('place_changed', fillInAddress);
}
 </script>   
  </html>