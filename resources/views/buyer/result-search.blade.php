<html>
<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBqAa1nsNtFCwNnGj8_ux8XOsTFGzypHQs"></script>
    <script src="js/jquery.geocomplete.min.js"></script> 
</head>
<body>
	  <form role="form" action="{{ url('search_result') }}">
              <h1>Landing Selling Login</h1> 
              <div class="row">
                  <input id="location" type="text" class="form-control" name="location" value="{{$loc}}">

                  @if ($errors->has('location'))
                    <span class="help-block">
                      <strong>{{ $errors->first('location') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="row">
                <div class="form-group">
                  <div class="col-md-6">
                    <button type="submit" class="btn btn-default submit">
                      Search
                    </button>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
        </form>
         <form role="form" action="{{ url('search_result') }}" id="filter">
              <h1>Filter</h1> 
              {{ csrf_field() }}
              <div class="row">
                  <input type="text" class="form-control" name="min_price" Placeholder="Min Price">
              </div>
              <div class="row">
                  <input type="text" class="form-control" name="max_price" Placeholder="Max Price">
              </div>
                  <input type="hidden" name="location" id="loc">
              <div class="row">
                <div class="form-group">
                  <div class="col-md-6">
                    <button type="submit" class="btn btn-default submit">
                      filter
                    </button>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
        </form>
        <?php var_dump($properties); ?>
		</body>	
		<script>
$(document).ready(function(){
     $("#location").geocomplete();
     $("#loc").val($("#location").val());
    $('body').on('submit', '#filter', function(event)
    {
       event.preventDefault();
        var data = new FormData($("#filter")[0]);
        console.log(data)
        $.ajax({
        type:"POST",  
        url: '/search_filter',
        dataType:'json',
        processData: false, 
        contentType: false,
        data:data,
        success: function(success_data) 
        { 
         
        }
           });   
    });  
 });
</script>
	</html>