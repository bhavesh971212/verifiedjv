<html>
<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBqAa1nsNtFCwNnGj8_ux8XOsTFGzypHQs"></script>
    <script src="js/jquery.geocomplete.min.js"></script> 
</head>
<body>
	  <form role="form" action="{{ url('search_result') }}">
              <h1>Landing Selling Login</h1> 
              <div class="row">
                  <input id="location" type="text" class="form-control" name="location">

                  @if ($errors->has('location'))
                    <span class="help-block">
                      <strong>{{ $errors->first('location') }}</strong>
                    </span>
                  @endif
                </div>
              </div>
              <div class="row">
                <div class="form-group">
                  <div class="col-md-6">
                    <button type="submit" class="btn btn-default submit">
                      Search
                    </button>
                  </div>
                </div>
              </div>
              <div class="clearfix"></div>
            </form>
		</body>	
		<script>
$(document).ready(function(){
     $("#location").geocomplete();
 });
</script>
	</html>