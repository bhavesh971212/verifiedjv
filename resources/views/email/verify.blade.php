<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <h2>Verify Your Email Address</h2>

        <div>
            Thanks for creating an account with the verifiedJV.
            Please click <a href="{{ URL::to('register/Verify/' . $confirmation_code ) }}">here</a> to verify your email address.
        </div>

    </body>
</html>