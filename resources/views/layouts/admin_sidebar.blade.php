<!-- sidebar menu -->
<div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
	<div class="menu_section">
		<h3>&nbsp;</h3>
		<ul class="nav side-menu">
			<li>
				<a href="{{ URL::to('admin-dashboard') }}">Home</a>
			</li>
			<li>
				<a>Users <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
					<li>
						<a href="{{ URL::to('user-list') }}">Users List</a>
					</li>
				</ul>
			</li>
			<li>
				<a>Property<span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
					<li><a href="{{ URL::to('property-list') }}">Property List</a></li>
				</ul>
			</li>
			<li>
				<a>Partners <span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
					<li><a href="{{ URL::to('partnerlist') }}">Partners List</a></li>
				</ul>
			</li>
			<li>
				<a>Requirements<span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
					<li><a href="{{ URL::to('requirementslist') }}">Requirements list</a></li>
				</ul>
			</li>
			<li>
				<a>Users Query<span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
					<li><a href="{{ URL::to('querylist') }}">Query list</a></li>
				</ul>
			</li>
			<li>
				<a>Manage Plot<span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
					<li><a href="{{ URL::to('plotlist') }}">Plot list</a></li>
				</ul>
			</li>
			<li>
				<a>Manage Locality<span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
					<li><a href="{{ URL::to('locality/list') }}">Locality list</a></li>
				</ul>
			</li>
			<li>
				<a>Manage Interest<span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
					<li><a href="{{ URL::to('interestlist') }}">Interest list</a></li>
				</ul>
			</li>
			<li>
				<a>Manage Property Type<span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
					<li><a href="{{ URL::to('property_typelist') }}">Property Type list</a></li>
				</ul>
			</li>

			<li>
				<a>Manage Zone Type<span class="fa fa-chevron-down"></span></a>
				<ul class="nav child_menu">
					<li><a href="{{ URL::to('zonelist') }}">Zone Type list</a></li>
				</ul>
			</li>
		</ul>
	</div>
</div>
<!-- /sidebar menu -->