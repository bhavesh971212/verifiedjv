<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <!-- Meta, title, CSS, favicons, etc. -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Lsp | Admin Dashboard</title>

        <!-- Bootstrap -->
        <link href="/bower_components/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="/bower_components/gentelella/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">

        <!-- DataTables -->
        <link href="/bower_components/datatables/media/css/dataTables.bootstrap.css" rel="stylesheet">

        <!-- Select 2 -->
        <link href="/bower_components/select2/dist/css/select2.min.css" rel="stylesheet">

        <link rel="stylesheet" href="/bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css" />

        <!-- bootstrap-wysiwyg -->
        <link href="/bower_components/gentelella/vendors/google-code-prettify/bin/prettify.min.css" rel="stylesheet">

        <!-- iCheck -->
        <link href="/bower_components/gentelella/vendors/iCheck/skins/flat/green.css" rel="stylesheet">

        <!-- Custom Theme Style -->
        <link href="/bower_components/gentelella/build/css/custom.min.css" rel="stylesheet">
        <link href="/bower_components/datatables/media/css/buttons.dataTables.css" rel="stylesheet">
        <!-- jQuery -->
        <script src="/bower_components/gentelella/vendors/jquery/dist/jquery.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBqAa1nsNtFCwNnGj8_ux8XOsTFGzypHQs&libraries=places"></script>

    </head>

    <body class="nav-md">
        <div class="container body">
            <div class="main_container">
                <div class="col-md-3 left_col">
                    <div class="left_col scroll-view">
                        <div class="navbar nav_title" style="border: 0;">
                            <a href="{{ URL::to('admin-dashboard') }}" class="site_title"><i class="fa fa-user-md"></i> <span>Lsp</span></a>
                        </div>

                        <div class="clearfix"></div>

                        <!-- menu profile quick info -->
                        <div class="profile">
                            <div class="profile_pic">
                                <img src="{{URL('/image/default-profile-pic.png') }}" class="img-circle profile_img">
                            </div>
                            <div class="profile_info">
                                <span>Welcome,</span>
                                <h2>{{ Auth::user()->name }}</h2>
                            </div>
                        </div>
                        <!-- /menu profile quick info -->

                        <br />

                        @include('layouts.admin_sidebar')
                    </div>
                </div>

                <!-- top navigation -->
                <div class="top_nav">
                    <div class="nav_menu">
                        <nav class="" role="navigation">
                            <div class="nav toggle">
                                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
                            </div>

                            <ul class="nav navbar-nav navbar-right">
                                <li class="">
                                    <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                        <img src="{{URL('/image/default-profile-pic.png') }}" alt="">{{ Auth::user()->name }}
                                        <span class=" fa fa-angle-down"></span>
                                    </a>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right">
                                        <li>
                                            <a href="{{ url('/adminlogout') }}"><i class="fa fa-sign-out pull-right"></i> Log Out</a>
                                        </li>
                                    </ul>
                                </li>

                                @include('layouts.admin_notifications')
                            </ul>
                        </nav>
                    </div>
                </div>
                <!-- /top navigation -->

                <!-- page content -->
                <div class="right_col" role="main">
                    <div class="row">
                        @if (session('error'))
                            <div class="alert alert-danger alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                {{ session('error') }}
                            </div>
                        @endif

                        @if (session('status'))
                            <div class="alert alert-success alert-dismissible fade in" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
                                {{ session('status') }}
                            </div>
                        @endif
                    </div>
                    @yield('content')
                </div>
                <!-- /page content -->
            </div>
        </div>

        <!-- Bootstrap -->
        <script src="/bower_components/gentelella/vendors/bootstrap/dist/js/bootstrap.min.js"></script>

        <!-- Bootstrap Bootbox -->
        <script src="/bower_components/bootbox.js/bootbox.js"></script>

        <!-- bootstrap-daterangepicker -->
        <script src="/bower_components/gentelella/vendors/moment/min/moment.min.js"></script>
        <!-- <script src="/bower_components/gentelella/vendors/bootstrap-daterangepicker/daterangepicker.js"></script> -->

        <script type="text/javascript" src="/bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>

        <!-- Datatables -->
        <script src="/bower_components/datatables/media/js/jquery.dataTables.js"></script>
        <script src="/bower_components/datatables/media/js/dataTables.bootstrap.js"></script>

        <!-- Select 2 -->
        <script src="/bower_components/select2/dist/js/select2.min.js"></script>

        <!-- Custom Theme Scripts -->
        <script src="/bower_components/gentelella/build/js/custom.min.js"></script>

        <!-- bootstrap-wysiwyg -->
        <script src="/bower_components/gentelella/vendors/bootstrap-wysiwyg/js/bootstrap-wysiwyg.min.js"></script>
        <script src="/bower_components/gentelella/vendors/jquery.hotkeys/jquery.hotkeys.js"></script>
        <script src="/bower_components/gentelella/vendors/google-code-prettify/src/prettify.js"></script>

        <!-- iCheck -->
        <script src="/bower_components/gentelella/vendors/iCheck/icheck.min.js"></script>
        <script type="text/javascript" language="javascript" src="/bower_components/datatables/media/js/dataTables.buttons.js"></script>
        <script type="text/javascript" language="javascript" src="//cdnjs.cloudflare.com/ajax/libs/jszip/2.5.0/jszip.min.js"></script>
        <script type="text/javascript" language="javascript" src="/bower_components/datatables/media/js/buttons.html5.js"></script>
        <script src="/js/main.js"></script>
        <script type="text/javascript" src="/bower_components/geocomplete/jquery.geocomplete.min.js"></script>

        <script>

                $('.autocomplete').geocomplete();
                $('.numbers_with_decimal').on('keypress keyup blur',function(){
                     $(this).val($(this).val().replace(/[^0-9\.]/g,''));
                     if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                            event.preventDefault();
                     }
                 });

                 $('.only_numbers').on('keypress keyup blur',function(){
                  this.value = this.value.replace(/[^0-9]/g,'');
                });
          
        </script>

    </body>
</html>
