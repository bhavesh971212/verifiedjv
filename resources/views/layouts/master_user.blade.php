<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Verified Joint Veuture</title>
	<!-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:200,200i,300,300i,400,400i,600,600i,700,700i,900,900i" rel="stylesheet">-->
	<link rel="stylesheet" type="text/css" href="css/reset.css">
	<link rel="stylesheet" type="text/css" href="bower_components/bootstrap/dist/css/bootstrap.min.css" >
	<link rel="stylesheet" type="text/css" href="bower_components/components-font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="bower_components/owl.carousel/dist/assets/owl.carousel.min.css">
	<link rel="stylesheet" type="text/css" href="bower_components/owl.carousel/dist/assets/owl.theme.default.min.css">
	<link rel="stylesheet" type="text/css" href="bower_components/bootstrap-select/dist/css/bootstrap-select.css">
	<link rel="stylesheet" type="text/css" href="bower_components/jquery-colorbox/colorbox.css">
	<link rel="stylesheet" type="text/css" href="bower_components/dropzone/dist/dropzone.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<script type="text/javascript" src="bower_components/jquery/dist/jquery.min.js"></script>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBqAa1nsNtFCwNnGj8_ux8XOsTFGzypHQs&libraries=places"></script>
</head>

<body>

	 @yield('content')
	 @include('partials.modals')


<!-- SCRIPT FILES HERE -->
<script type="text/javascript" src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<script type="text/javascript" src="bower_components/owl.carousel/dist/owl.carousel.min.js"></script>
<script type="text/javascript" src="bower_components/bootstrap-select/dist/js/bootstrap-select.js"></script>
<script type="text/javascript" src="bower_components/geocomplete/jquery.geocomplete.min.js"></script>
<script type="text/javascript" src="bower_components/jquery-colorbox/jquery.colorbox.js"></script>
<script type="text/javascript" src="bower_components/dropzone/dist/dropzone.js"></script>
<script type="text/javascript" src="js/custom.js"></script>

<script>
	function openNav() {
		document.getElementById("mySidenav").style.width = "250px";
	}
	function closeNav() {
		document.getElementById("mySidenav").style.width = "0";
	}
	$('.autocomplete').geocomplete();
	$('.selectstate').on('change',function(){
		var state = $(this).val();
		$.ajax({
			type:"POST",
			url: $('.getcity').val(),
			data:{"_token": "{{ csrf_token() }}","state":state},
			success:function(res){
				var select = $('#city');
				select.empty();
				select.html(res);
				select.selectpicker('val', res);
				$('#city').selectpicker('refresh');

			}
		});
	});
	$('.selectcity').on('change',function(){
		var city = $(this).val();
		$.ajax({
			type:"POST",
			url: $('.getlocality').val(),
			data:{"_token": "{{ csrf_token() }}","city":city},
			success:function(res){
				$('#locality').html(res);
				$('#locality').selectpicker('refresh');
			}
		});
	});
	$('.numbers_with_decimal').on('keypress keyup blur',function(){
		 $(this).val($(this).val().replace(/[^0-9\.]/g,''));
         if ((event.which != 46 || $(this).val().indexOf('.') != -1) && (event.which < 48 || event.which > 57)) {
                event.preventDefault();
         }
	});

	$('.only_numbers').on('keypress keyup blur',function(){
		this.value = this.value.replace(/[^0-9]/g,'');
	});
</script>
</body>
</html>
