<section id="index_header" class="land_main_banner">
  <div class="container">
    <header>
      <div class="row">
        <div class="col-md-6">
          <ul class="header_left">
            <li><a href="{{ url('/') }}"><img src="images/index_logo.png" width="100" alt=""></a></li>
            <li><a href="javascript:void(0);" id="ph_num">+91 9878834567</a></li>
            <li><a href="javascript:void(0);" id="mail_id">info@verified.com</a></li>
          </ul>
        </div>
        <div class="col-md-6">
          <ul class="header_right">
            <li>
              <div class="dropdown">
                <button class="btn dropdown-toggle toggle_logo_btn" type="button" id="post_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Post&nbsp;&nbsp;<span class="caret"></span></button>
                <ul class="dropdown-menu pull-right" aria-labelledby="post_menu">
                  <li><a href="{{ url('post_add') }}">Post Adds</a></li>
                  <li><a href="{{ url('post_requirement') }}">Post Requirements</a></li>
                </ul>
              </div>
            </li>
            <li>
              <div class="dropdown">
                <img src="images/default_user.png" width="35" height="35" alt="default_userName" class="dropdown-toggle" id="userMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                <ul class="dropdown-menu pull-right" aria-labelledby="userMenu">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                </ul>
              </div>
            </li>
            <li><a href="javascript:void(0);" data-toggle="modal" data-target="#login-myModal">Login</a></li>
            <li><a href="javascript:void(0);" onclick="openNav()"><i class="fa fa-bars" aria-hidden="true"></i></a></li>
          </ul>
        </div>
      </div>
    </header>
    <div class="header-content text-center">
      <div class="row">
        <div class="col-md-12">
          <h1>Find your perfect joint venture deal</h1>
          <ul class="nav nav-tabs">
            <li id="tabone" class="active"><a data-toggle="tab" href="#home">Land</a>
              <div class="arrow-down"></div>
              <div class="arrow-down2"></div>
            </li>
            <li id="tabtwo"><a data-toggle="tab" href="#menu1">Requirement</a>
              <div class="arrow-down"></div>
              <div class="arrow-down2"></div>
            </li>
          </ul>
          <div class="tab-content">
            <div id="home" class="tab-pane active">
              <input type="text" name="name" value="" placeholder="Enter Land, city... "><button class="btn search-box">Search</button>
            </div>
            <div id="menu1" class="tab-pane">
              <input type="text" name="name" value="" placeholder="Area,Sq feet etc...."><button class="btn search-box">Search</button>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="animated-image">
    <a class="box" href="javascript:void(0);">
      <div class="dot-img" id="divAnimate"></div>
    </a>
      </div>
</section>
