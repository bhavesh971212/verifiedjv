	<header>
		<section id="jvm-header">
			<div class="container-fluid">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="jvm-pad">
							<ul>
								<li><a href="{{ url('/') }}"><img src="image/logo.png" alt="Verified Joint Venture" width="95"></a></li>
								<li><i class="fa fa-phone" aria-hidden="true"></i>+91 9878834567</li>
								<li><i class="fa fa-envelope-o" aria-hidden="true"></i>info@verified.com</li>
							</ul>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="jvm-pad jvm-flot">
							<ul>
								<li>
									<div class="dropdown">
										<button class="btn btn-default dropdown-toggle" type="button" id="demo" data-toggle="dropdown">Post &nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i></button>
										<ul class="dropdown-menu" role="menu" aria-labelledby="demo">
											<li role="presentation"><a role="menuitem" tabindex="-1" href="{{ url('post_add') }}">Post Ads</a></li>
											<li role="presentation" class="divider"></li>
											<li role="presentation"><a role="menuitem" tabindex="-1" href="{{ url('post_requirement') }}">Post A Requirements</a></li>
										</ul>
									</div>
								</li>
								@if(isset(Auth::user()->name))
								<li>
									<p>{{ Auth::user()->name }}</p>
								</li>
								<li><a href="{{ url('logout') }}" style="cursor: pointer;margin: 5px 0 0">Logout</a></li>
								@else
								<li>
									<p data-toggle="modal" data-target="#login-myModal" style="cursor: pointer;margin: 5px 0 0">Login</p>
								</li>
								@endif
							</ul>
						</div>
					</div>
				</div>
				<div class="row" id="mysearch">
				<input type="hidden" name="check" value="" id="check">
					<div class="col-xs-12 col-sm-12 col-md-12">
						<div class="header-content text-center">
							<h1>Find your perfect joint venture deal</h1>
							<ul class="nav nav-tabs">
								<li id="tabone" class="active"><a data-toggle="tab" class="land"href="#home">Land</a>
									<div class="arrow-down"></div>
									<div class="arrow-down2"></div>
								</li>
								<li id="tabtwo"><a data-toggle="tab" href="#menu1" class="requirement">Requirement</a>
									<div class="arrow-down"></div>
									<div class="arrow-down2"></div>
								</li>
							</ul>
							<div class="tab-content">
				<form class="form-horizontal" action="{{ URL::to('search_results') }}">

								<div id="home" class="tab-pane fade in active">
									<input type="text" name="name" value="" id="location_land" placeholder="Enter Land, city... ">
									<input type="hidden" name="lat_land" id="lat_land" class="lat_land">
									<input type="hidden" name="lng_land" id="lng_land" class="lng_land">
									<button class="btn search-box">Search</button>
								</div>

				</form>

			<form class="form-horizontal" action="{{ URL::to('search_results') }}">

								<div id="menu1" class="tab-pane fade">
									<input type="text" name="name" value="" id="location_req" placeholder="Enter Requirement, city... ">
							<input type="hidden" name="lat_req" id="lat_req" class="lat_req">
							<input type="hidden" name="lng_req" id="lng_req" class="lng_req">

                         <a href=""><button class="btn search-box">Search</button>
								</div>

					</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</header>
<script type="text/javascript">
$(document).ready(function(){

	$('#mysearch').on('click', function() {
	var a=$('#check').val();
    if(a=="1"){

       autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('location_land')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);

    }else{

    	autocomplete = new google.maps.places.Autocomplete(
            /** @type {!HTMLInputElement} */(document.getElementById('location_req')),
            {types: ['geocode']});

        // When the user selects an address from the dropdown, populate the address
        // fields in the form.
        autocomplete.addListener('place_changed', fillInAddress);

    }

});


function fillInAddress() 
{
	var a=$('#check').val();

if(a=="1"){
	
  geocoder = new google.maps.Geocoder();
    var address = document.getElementById("location_land").value;
    geocoder.geocode( { 'address': address}, function(results, status) {
    	
      if (status==google.maps.GeocoderStatus.OK){

$('.lat_land').val(results[0].geometry.location.lat());
$('.lng_land').val(results[0].geometry.location.lng());


      } 

});

}else if(a=="2"){
geocoder = new google.maps.Geocoder();
    var address = document.getElementById("location_req").value;
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == google.maps.GeocoderStatus.OK) {
$('.lat_req').val(results[0].geometry.location.lat());
$('.lng_req').val(results[0].geometry.location.lng());


              } 

});

}

}


$("#check").val("1");

$('.land').on('click',function() {
$("#check").val("1");
});

$('.requirement').on('click', function() {
$("#check").val("2");
});

});

</script>

	<!-- ** GOOGLE AUTO SUGGEST SCRIPTS STARTS -->
	<script src="http://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBqAa1nsNtFCwNnGj8_ux8XOsTFGzypHQs"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/geocomplete/1.7.0/jquery.geocomplete.min.js"></script>
	<!-- ** GOOGLE AUTO SUGGEST SCRIPTS ENDS -->