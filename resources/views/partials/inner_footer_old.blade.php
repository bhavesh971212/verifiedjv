	<footer id="inner_footer">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<ul class="text-left">
						<li><a href="javascript:void(0);">About Us</a></li>
						<li><a href="javascript:void(0);">Recent</a></li>
						<li><a href="javascript:void(0);">Updates</a></li>
						<li><a href="javascript:void(0);">Career</a></li>
						<li><a href="javascript:void(0);">Contact</a></li>
						<li><a href="javascript:void(0);">Sitemap</a></li>
					</ul>
				</div>
				<div class="col-md-6">
					<ul class="text-right">
						<li><a href="javascript:void(0);">Terms and Condition</a></li>
						<li><a href="javascript:void(0);">Privacy and Legal</a></li>
						<li>&copy; <span id="current_year"></span><a href="javascript:void(0);"> &nbsp;Verified Join ventures Pvt.Ltd.</a></li>
					</ul>
				</div>
			</div>
		</div>
	</footer>