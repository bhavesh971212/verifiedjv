<section id="inner_header">
	<header>
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<ul class="header_left">
						<li><a href="{{ url('/') }}"><img src="images/white-logo.png" alt="Verified Joint Ventures" width="100"></a></li>
						<li><a href="javascript:void(0);" id="call_icon">+91 9878834567</a></li>
						<li><a href="javascript:void(0);" id="mail_icon">info@verified.com</a></li>
					</ul>
				</div>
				<div class="col-md-6">
					<ul class="header_right nav_bar_link">
						<li>
							<div class="dropdown">
								<button class="btn dropdown-toggle" type="button" id="post_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Post&nbsp;&nbsp;<span class="caret"></span></button>
								<ul class="dropdown-menu pull-right" aria-labelledby="post_menu">
									<li><a href="{{ url('post_add') }}">Post Adds</a></li>
									<li><a href="{{ url('post_requirement') }}">Post Requirements</a></li>
								</ul>
							</div>
						</li>
						<li><a href="javascript:void(0);" data-toggle="modal" data-target="#login-myModal">Login</a></li>
						<li><a href="javascript:void(0);" onclick="openNav()"><i class="fa fa-bars" aria-hidden="true"></i></a></li>
					</ul>
				</div>
			</div>
		</div>
	</header>
</section>
