	<header id="inner_header">
		<div class="container">
			<div class="row">
				<div class="col-md-6">
					<ul class="text-left">
						<li><a href="{{ url('/') }}"><img src="image/white-logo.png" alt="Verified Joint Ventures" width="90"></a></li>
						<li class="call_icon"><a href="javascript:void(0);">+91 9878834567</a></li>
						<li class="mail_icon"><a href="javascript:void(0);">info@verified.com</a></li>
					</ul>
				</div>
				<div class="col-md-6">
					<ul class="text-right">
						<li>
							<div class="dropdown">
								<button class="btn dropdown-toggle" type="button" id="post_menu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Post&nbsp;<span class="caret"></span></button>
								<ul class="dropdown-menu" aria-labelledby="post_menu">
									<li><a href="{{ url('post_add') }}">Post Adds</a></li>
									<li><a href="{{ url('post_requirement') }}">Post Requirements</a></li>
								</ul>
							</div>
						</li>
						@if(isset(Auth::user()->name))
						<li>
							<p>{{ Auth::user()->name }}</p>
						</li>
						<li><a href="{{ url('logout') }}">Logout</a></li>
						@else
						<li><a href="javascript:void(0);" data-toggle="modal" data-target="#login-myModal">Login</a></li>
						@endif
					</ul>
				</div>
			</div>
		</div>
	</header>