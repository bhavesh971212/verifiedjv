<!-- REGISTER MODAL HERE -->
<div id="header-myModal" class="modal fade" role="dialog">
	<div class="modal-dialog">  
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title">Create an new account</h3>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
								<div class="text-center">
									<div id="login-r-button">
										<ul class="list-inline">
											<li><img src="image/6.png" alt=""></li>
											<li><img src="image/6.png" alt=""></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<form action="/register" name="sign_up">
							<div class="col-xs-12 col-sm-12 col-md-12">
								<div class="text-center">
                                    <div id="login-r-button">
                                        <ul  class="list-inline">
                                          <li>
                                            <div>
                                                <input id="radio-1" value="3" class="radio-custom" name="usertype" type="radio" checked>
                                                <label for="radio-1" class="radio-custom-label">Land Seeker</label>
                                            </div>
                                          </li>
                                          <li>
                                            <div>
                                                <input id="radio-2" value="2" class="radio-custom" name="usertype" type="radio">
                                                <label for="radio-2" class="radio-custom-label">Land Owner</label>
                                            </div>
                                          </li>
                                        </ul>
                                        <span class="error eruser"></span>
                                    </div>
                              	</div>
                              		<div class="login-model-form">
										<div class="form-group">
											<input type="text" class="form-control rname" name="name" id="usr" placeholder="Full Name">
											<span class="error ername"></span>
										</div>
										{{csrf_field()}}
										<div class="form-group">
											<input type="tel" class="form-control rphone" name="phone" id="tele-phone"  placeholder="Mobile Number">
											<span class="error erphone"></span>
										</div>
										<div class="form-group">
											<input type="text" class="form-control remail" name="email" id="email"  placeholder=" Email id" >
											<span class="error eremail"></span>
										</div>
										<div class="form-group">
											<input type="password" class="form-control rpassword" name="password" id="pwd"  placeholder="Password*" >
											<span class="error erpassword"></span>
										</div>
										<div class="form-group">
											<input type="password" class="form-control rcpassword" name="cpassword" id="pwd"  placeholder="Confirm Password*" >
											<span class="error ercpassword"></span>
										</div>
										<ul class="list-inline vjv-user">
											<li>
												<button type="submit" id="set-alert2" name="usertype" class="text-center">Submit</button>
											</li>
											<li>
												<p class="vjv-account-p1">Are you a VJV user?</p>
												<p class="vjv-account-p2">If you have a account.<a data-dismiss="modal" data-toggle="modal" href="#login-myModal"> Login</a></p>
											</li>
										</ul>
                              		</div>
								</div>
							</form>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="row">
							<div class="col-sm-12 col-md-12 col-sm-12">
								<div class="new-account-rightpart">
									<ul>
										<li>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium.</li>
										<li>Voluptatum deleniti atque corrupti quos dolores et quas molestias.</li>
										<li>Soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime.</li>
										<li>Omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis.</li>
										<li>Password should have at least 8 characters including 1 special character.</li>
										<li>By signing up you agree to our terms and privacy policy.</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-12 col-md-12 col-sm-12">
								<div class="new-account-image">
									<img class="img-responsive" src="image/login.png" alt="">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- LOGIN MODAL -->
<div id="login-myModal" class="modal fade" role="dialog">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h3 class="modal-title">Login</h3>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
								<form action="/login" name="login_form">
									<div class="login-model-form">
									{{csrf_field()}}
										<div class="form-group">
											<input type="text" class="form-control lemail" id="email" name="lemail" placeholder="Email id" >
											<span class="error elemail"></span>
										</div>
										<div class="form-group">
											<input type="password" class="form-control lpassword" id="pwd" placeholder="Password*" name="lpassword" >
											<span class="error elpassword"></span>
											<p class="login-model-p">Forgot you <a href="#">Password</a>?</p>
										</div>
										<div class="login-checkbox">
											<input id="checkbox-1" class="checkbox-custom" name="checkbox-1" type="checkbox" checked>
											<label for="checkbox-1" class="checkbox-custom-label">Stay Logged in</label>
										</div>
										<ul class="list-inline vjv-user">
											<li>
												<button type="submit" id="set-alert2" class="text-center">Login</button></li>
											<li>
												<p class="vjv-account-p1">New to VJV?</p>
												<p class="vjv-account-p2">Get started now.<a href="javascript:void(0);" data-dismiss="modal" data-toggle="modal" data-target="#header-myModal">Register</a></p>
											</li>
										</ul>
									</div>
								</form>
							</div>
						</div>
					</div>
					<div class="col-xs-12 col-sm-6 col-md-6">
						<div class="row">
								<div class="col-sm-12 col-md-12 col-sm-12">
									<div class="new-account-image text-center">
										<img class="img-responsive" src="image/login.png" alt="">
									</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Otp Modal -->
<div class="modal fade" id="otpModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form name="otp_form" action="/registerOtp"> 
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
      			<input type="hidden" class="form-control otpemail" name="otpemail"  >
      			<input type="hidden" class="form-control otpphone" name="otpphone"  >
      			{{csrf_field()}}
				<input type="text" class="form-control otpcode" name="otpcode"  placeholder="Enter Otp*" >
				<span class="error eotpcode"></span>
      </div>
      <div class="modal-footer">
        <button type="submit" class="btn btn-default" >Submit</button>
        <button type="button" class="btn btn-primary">Resend Otp</button>
      </div>
    </form>
    </div>
  </div>
</div>



<!-- SOCIAL ICONS CODE -->
<div id="social_icons">
	<ul>
		<li><a href="javascript:void(0);" target="_blank"><img src="image/facebook.png" width="35" height="35" alt="venture_facebook"></a></li>
		<li><a href="javascript:void(0);" target="_blank"><img src="image/google.png" width="35" height="35" alt="venture_google"></a></li>
		<li><a href="javascript:void(0);" target="_blank"><img src="image/twitter.png" width="35" height="35" alt="venture_twitter"></a></li>
		<li><a href="javascript:void(0);" target="_blank"><img src="image/linkedin.png" width="35" height="35" alt="venture_linkedin"></a></li>
		<li><a href="javascript:void(0);" target="_blank"><img src="image/youtube.png" width="35" height="35" alt="venture_youtube"></a></li>
	</ul>
</div>
<!-- COMMON PROPERTY ALERT CODE -->
<div class="property_alert animate_div_click">
	<img src="image/property.png" height="120" data-attr="property_alert">
	<div id="feedback-inner">
		<div class="text-center">
			<h3>Set a Property Alert</h3>
			<p>Get matching properties delivered in your inbox as soon as they're uploaded</p>
			<form action="">
				<ul  class="list-inline">
					<li>
						<div>
							<input id="radio-1" class="radio-custom" name="radio-group" type="radio" checked>
							<label for="radio-1" class="radio-custom-label">Land Seeker</label>
						</div>
					</li>
					<li>
						<div>
							<input id="radio-2" class="radio-custom" name="radio-group" type="radio">
							<label for="radio-2" class="radio-custom-label">Land Owner</label>
						</div>
					</li>
				</ul>
				<div class="form-group">
					<input type="text" class="form-control" id="usr" placeholder="Your Name*">
				</div>
				<div class="form-group">
					<input type="tel" class="form-control" id="tele-phone" placeholder="Your Mobile Number*">
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="email" placeholder="Your Email id*">
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="location" placeholder="Preffered Location">
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="pwd" placeholder="Land Size" required="">
				</div>
			</form>
		</div>
	</div>
</div>
<!-- COMMON FEEDBACK ALERT CODE -->
<div class="feedback_alert animate_div_click">
	<img src="image/feedback.png" height="120" data-attr="feedback_alert">
	<div id="feedback-inner">
		<div class="text-center">
			<h3>WE ARE ALL EARS!!!</h3>
			<p>Tell us what you love, tell us what you hate, &amp; tell us what you want us to improve</p>
			<form action="">
				<div class="form-group">
					<input type="text" class="form-control" id="usr" placeholder=" Your Name*" required="">
				</div>
				<div class="form-group">
					<input type="text" class="form-control" id="email" placeholder=" Your Email id*" required="">
				</div>
				<div class="form-group">
					<textarea  rows="5" id="comment" placeholder="Your feedback*"></textarea>
				</div>
				<button type="submit" class="btn  text-center" id="set-alert1">Submit</button>
			</form>
		</div>
	</div>
</div>

<script>
	$(document).ready(function(){
		$('form[name="sign_up"]').on('submit',function(e){
			e.preventDefault();
			$('.error').html('');

			var $radios = $('input:radio[name=usertype]');
          	if($radios.is(':checked') === false) {
              	$('.eruser').html('Select User Type');
          	}

			if($('.rname').val() == ''){
				$('.ername').html('Enter Name');
				return false;
			}

			var nameFormat = /^[a-zA-Z\s]+$/;
			if(!nameFormat.test($('.rname').val())){
				$('.ername').html('Enter Only Alphabet');
				return false;
			}

			if($('.rphone').val() == ''){
				$('.erphone').html('Enter Mobile Number');
				return false;
			}

			var phoneFormat = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
			if(!phoneFormat.test($('.rphone').val())){
				$('.erphone').html('Enter 10 Digits');
				return false;
			}

			if($('.remail').val() == ''){
				$('.eremail').html('Enter Email Id');
				return false;
			}

			var emailFormat = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
			if(!emailFormat.test($('.remail').val())){
				$('.eremail').html('Enter Valid Email Id');
				return false;
			}

			if($('.rpassword').val() == ''){
				$('.erpassword').html('Enter Password');
				return false;
			}

			var passwordFormat = /^.{6,}$/;
			if(!passwordFormat.test($('.rpassword').val())){
				$('.erpassword').html('Password Should be atleast 6 characters');
				return false;
			}

			if($('.rcpassword').val() == ''){
				$('.ercpassword').html('Enter Confirm Password');
				return false;
			}

			if($('.rcpassword').val() != $('.rpassword').val()){
				$('.ercpassword').html('Password does not match');
				return false;
			}

			var data = $('form[name="sign_up"]').serialize();
			$.ajax({
				type:"POST",
				data: $(this).serialize() ,
				url: $(this).attr('action'),
				success:function(res){
					if(res == 1){
						alert('Email Already Exist');
					} else if(res == 2){
						alert('Phone Number Already Exist');
					} else if(res == 4) {
						alert("Something Went Wrong");
					}  else {
						var a = $.parseJSON(res);
						$('#header-myModal').modal('hide');
						$('#otpModal').modal('show');
						$('.otpemail').val(a.email);
						$('.otpphone').val(a.phone);
					}
				}
			});
		});

		$('form[name="otp_form"]').on('submit',function(e){
			e.preventDefault();
			$('.error').html('');
			if($('.otpcode').val() == ''){
				$('.eotpcode').html('Enter OTP');
				return false;
			}

			$.ajax({
				type:"POST",
				url: $(this).attr('action'),
				data: $(this).serialize(),
				success: function(res){
					if(res == 1){
						alert('Successfully Registered');
						$('#otpModal').modal('hide');
						$('#login-myModal').modal('show');
					} else if(res == 2){
						alert('Invalid Otp');
					} else if(res == 3){
						alert('Something Went Wrong');
					}
				}
			});
		});

		$('form[name="login_form"]').on('submit',function(e){
			e.preventDefault();
			$('.error').html('');
			if($('.lemail').val() == ''){
				$('.elemail').html('Enter Email Id');
				return false;
			}

			var emailFormat = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
			if(!emailFormat.test($('.lemail').val())){
				$('.elemail').html('Enter Valid Email Id');
				return false;
			}

			if($('.lpassword').val() == ''){
				$('.elpassword').html('Enter Password');
				return false;
			}

			$.ajax({
				type:"POST",
				url: $(this).attr('action'),
				data: $(this).serialize(),
				success: function(res){
					if(res == 2){
						alert('Your Account is Not Activated');
					} else if(res == 3){
						alert('Invalid UserName/Password');
					} else if(res == 4){
						alert('Something Went Wrong');
					} else {
						alert('Successfully Logged In');
						window.location.reload(true);
					}  
				}
			});
		});
	});
</script>