<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Landing Selling | Login</title>

    <!-- Bootstrap -->
    <link href="/bower_components/gentelella/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="/bower_components/gentelella/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- Animate.css -->
    <link href="https://colorlib.com/polygon/gentelella/css/animate.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="/bower_components/gentelella/build/css/custom.min.css" rel="stylesheet">
  </head>
  <style>
  body{
    background-color: #525252;
}
.centered-form{
	margin-top: 60px;
}

.centered-form .panel{
	background: rgba(255, 255, 255, 0.8);
	box-shadow: rgba(0, 0, 0, 0.3) 20px 20px 20px;
}
  </style>
  <body class="login">
		<div class="container">
		        <div class="row centered-form">
		        <div class="col-xs-12 col-sm-8 col-md-4 col-sm-offset-2 col-md-offset-4">
		        	<div class="panel panel-default">
		        		<div class="panel-heading">
					    		<h3 class="panel-title">Please sign up for Bootsnipp <small>It's free!</small></h3>
					 			</div>
					 			<div class="panel-body">
					    		<form role="form" method="post" action="{{URL('register')}}">
					    			<div class="radio">
									 <label><input type="radio" name="role" value="2" checked>Seller</label>
									</div>
									<div class="radio">
									  <label><input type="radio" name="role" value="3">Buyer</label>
									</div> 
					    			<div class="row">
					    				<div class="col-xs-6 col-sm-6 col-md-6">
					    					<div class="form-group">
					                        <input type="text" name="name" id="name" class="form-control input-sm" placeholder="First Name">
					    					<span style="color: red;">{{$errors->first('name')}}</span>
					    					</div>
					    				</div>
					    			</div>

					    			<div class="form-group">
					    				<input type="email" name="email" id="email" class="form-control input-sm" placeholder="Email Address">
					    			    <span style="color: red;">{{$errors->first('email')}}</span>
					    			    {{csrf_field()}}
					    			</div>
                                    <div class="form-group">
					    				<input type="number" name="phone" id="email" class="form-control input-sm" placeholder="Phone">
					    			    <span style="color: red;">{{$errors->first('phone')}}</span>
					    			    {{csrf_field()}}
					    			</div>
					    			<div class="row">
					    				<div class="col-xs-6 col-sm-6 col-md-6">
					    					<div class="form-group">
					    						<input type="password" name="password" id="password" class="form-control input-sm" placeholder="Password">
					    					   <span style="color: red;">{{$errors->first('password')}}</span>
					    					</div>
					    				</div>
					    				<div class="col-xs-6 col-sm-6 col-md-6">
					    					<div class="form-group">
					    						<input type="password" name="password_confirmation" id="password_confirmation" class="form-control input-sm" placeholder="Confirm Password">
					    					    <span style="color: red;">{{$errors->first('password_confirmation')}}</span>
					    					</div>
					    				</div>
					    			</div>	
					    			<input type="submit" value="Register" class="btn btn-info btn-block">
					    		</form>
					    	</div>
			    		</div>
		    		</div>
		    	</div>
		    </div>
	</body>
</html>		    