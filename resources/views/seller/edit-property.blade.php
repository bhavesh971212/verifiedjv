<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Example of Bootstrap 3 Horizontal Form Layout</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script> 
<!-- <script src="https://maps.googleapis.com/maps/api/js?libraries=places&key=AIzaSyBqAa1nsNtFCwNnGj8_ux8XOsTFGzypHQs"></script> -->
<script src="{{URL('js/jquery.geocomplete.min.js')}}"></script> 
<style type="text/css">
    .bs-example{
    	margin: 20px;
    }
	/* Fix alignment issue of label on extra small devices in Bootstrap 3.2 */
    .form-horizontal .control-label{
        padding-top: 7px;
    }
</style>
<style>
  #locationField, #controls {
    position: relative;
    width: 480px;
  }
  #autocomplete {
    position: absolute;
    top: 0px;
    left: 0px;
    width: 99%;
  }
  .label {
    text-align: right;
    font-weight: bold;
    width: 100px;
    color: #303030;
  }
  #address {
    border: 1px solid #000090;
    background-color: #f0f0ff;
    width: 480px;
    padding-right: 2px;
  }
  #address td {
    font-size: 10pt;
  }
  .field {
    width: 99%;
  }
  .slimField {
    width: 80px;
  }
  .wideField {
    width: 200px;
  }
  #locationField {
    height: 20px;
    margin-bottom: 2px;
  }
</style>
</head>
<body>
<div class="bs-example">
    <form class="form-horizontal" enctype="multipart/form-data" action='{{URL("edit-property/$property->id")}}' method="post">
         {{ csrf_field() }}
        <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Title</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputEmail" placeholder="Title" name="title" value="{{$property->title}}">
                <span style="color:red;">{{$errors->first('title')}}</span>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Property Details</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputEmail" placeholder="details" name="details" value="{{$property->details}}">
                <span style="color:red;">{{$errors->first('details')}}</span>
            </div>
        </div>
             <div class="form-group">
                <label for="inputEmail" class="control-label col-xs-2">Size</label>
                <div class="col-xs-10">
                    <input type="text" class="form-control" id="inputEmail" value="{{$property->size}}" placeholder="size"
                    name="size">
                    <span style="color:red;">{{$errors->first('size')}}</span>
                </div>
            </div>
          <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Size Unit</label>
            <div class="col-xs-10">
                <select name="sizeunit">
                  <option value="">Select size unit</option>  
                  <option value="Acre"   @if($property->sizeunit=="Acre") {{ 'selected' }} @endif>Acre</option>
                  <option value="Bigha"  @if($property->sizeunit=="bigha") {{ 'selected' }} @endif >Bigha</option>
                  <option value="Kattah" @if($property->sizeunit=="Kattah") {{ 'selected' }} @endif>Kattah</option>
                  <option value="Square" @if($property->sizeunit=="Square") {{ 'selected' }} @endif>Square</option>
                  <option value="Feet"   @if($property->sizeunit=="Feet") {{ 'selected' }} @endif>Feet</option>
                  <option value="Square yards" @if($property->sizeunit=="Square yards") {{ 'selected' }} @endif>Square yards</option>
                  <option value="Square meters" @if($property->sizeunit=="Square meters") {{ 'selected' }} @endif>Square meters</option>
                  <option value="Ground" @if($property->sizeunit=="Ground") {{ 'selected' }} @endif>Ground</option>
                  <option value="Ghunta" @if($property->sizeunit=="Ghunta") {{ 'selected' }} @endif>Ghunta</option>
                </select>
                 <span style="color:red;">{{$errors->first('sizeunit')}}</span>
         </div>
        </div>
            <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Location</label>
            <div class="col-xs-10">
              <input id="autocomplete" class="form-control" name="location" value="{{ old('location') }}" placeholder="Enter your address" onFocus="geolocate()" type="text"></input>
              <span style="color:red;"></span>
            </div>
        </div>
                <table id="address">
              <tr>
                <td class="label">Street address</td>
                <td class="slimField"><input class="field" id="street_number"
                      disabled="true" name="street_address" value="{{$property->street_address}}"></input><span style="color:red;">{{$errors->first('street_address')}}</span></td>
                <td class="wideField" colspan="2"><input class="field" id="route"
                      disabled="true" name="route" required value="{{$property->route}}"></input><span style="color:red;">{{$errors->first('route')}}</span></td>
              </tr>
              <tr>
                <td class="label">City</td>
                <td class="wideField" colspan="3"><input class="field" id="locality"
                      disabled="true" name="city" required value="{{$property->city}}"></input><span style="color:red;">{{$errors->first('city')}}</span></td>
              </tr>
              <tr>
                <td class="label">State</td>
                <td class="slimField"><input class="field"
                      id="administrative_area_level_1" disabled="true" name="state" required value="{{$property->state}}"></input><span style="color:red;">{{$errors->first('state')}}</span></td>
                <td class="label">Zip code</td>
                <td class="wideField">
                <input class="field" id="postal_code" type="number" disabled="true" name="postal_code" required value="{{$property->zipcode}}"></input><span style="color:red;">{{$errors->first('postal_code')}}</span></td>
              </tr>
              <tr>
                <td class="label">Country</td>
                <td class="wideField" colspan="3"><input class="field"
                      id="country" disabled="true" name="country" required value="{{$property->country}}"></input><span style="color:red;">{{$errors->first('country')}}</span></td>
              </tr>
       </table>
        <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Upload Image</label>
            <div class="col-xs-10">
                <input type="file" class="form-control" id="inputEmail" placeholder="size"
                name="image">
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Access Road Width</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputEmail" 
                name="access_road_width" value="{{$property->access_road_width}}">
                <span style="color:red;">{{$errors->first('land_frontage')}}</span>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Land Frontage</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputEmail" 
                name="land_frontage" value="{{$property->land_frontage}}">
                 <span style="color:red;">{{$errors->first('land_frontage')}}</span>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Land Zone</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputEmail" 
                name="land_zone" value="{{$property->land_zone}}">
                 <span style="color:red;">{{$errors->first('land_zone')}}</span>
            </div>
        </div>
          <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Land Zone Unit</label>
            <div class="col-xs-10">
                <select name="land_zone_unit">
                  <option value="">Select Land Zone unit</option>  
                  <option value="Agriculture" @if($property->land_zone_unit=="Agriculture") {{ 'selected' }} @endif>Agriculture</option>
                  <option value="Residential" @if($property->land_zone_unit=="Residential") {{ 'selected' }} @endif>Residential</option>
                  <option value="Industrial" @if($property->land_zone_unit=="Industrial") {{ 'selected' }} @endif>Industrial</option>
                  <option value="Commercial" @if($property->land_zone_unit=="Commercial") {{ 'selected' }} @endif>Commercial</option>
                </select>
                 <span style="color:red;">{{$errors->first('land_zone_unit')}}</span>
           </div>
        </div>
        <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Land Title</label>
            <div class="col-xs-10">
                <select name="land_title">
                  <option value="">Select Land Title</option>  
                  <option value="Agriculture" @if($property->land_title=="Agriculture") {{ 'selected' }} @endif>Agriculture</option>
                  <option value="Residential" @if($property->land_title=="Residential") {{ 'selected' }} @endif>Residential</option>
                  <option value="Industrial" @if($property->land_title=="Industrial") {{ 'selected' }} @endif>Industrial</option>
                  <option value="Commercial" @if($property->land_title=="Commercial") {{ 'selected' }} @endif>Commercial</option>
                  <option value="Patta" @if($property->land_title=="Patta") {{ 'selected' }} @endif>Patta</option>
                  <option value="Society" @if($property->land_title=="Society") {{ 'selected' }} @endif>Society</option>
                </select>
                 <span style="color:red;">{{$errors->first('land_title')}}</span>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Market Value</label>
            <div class="col-xs-10">
                <input type="number" class="form-control" id="inputEmail" 
                name="market_value" value="{{$property->market_value}}">
                <span style="color:red;">{{$errors->first('market_value')}}</span>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Market value Unit</label>
            <div class="col-xs-10">
                <select name="market_value_unit">
                  <option value="">Select Market Value Unit</option>  
                  <option value="Crores" @if($property->market_value_unit=="Crores") {{ 'selected' }} @endif>Crores</option>
                  <option value="Lakh" @if($property->market_value_unit =="Lakh") {{ 'selected' }} @endif>Lakh</option>
                </select>
                 <span style="color:red;">{{$errors->first('market_value_unit')}}</span>
           </div>
        </div>
        <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Advance</label>
            <div class="col-xs-10">
                <input type="number" class="form-control" id="inputEmail" 
                name="advance" value="{{$property->market_value}}">
                <span style="color:red;">{{$errors->first('advance')}}</span>
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Advance value Unit</label>
            <div class="col-xs-10">
                <select name="advance_value_unit">
                  <option value="">Select Advance Value Unit</option>  
                  <option value="Crores" @if($property->advance_value_unit=="Crores") {{ 'selected' }} @endif>Crores</option>
                  <option value="Lakh" @if($property->advance_value_unit=="Lakh") {{ 'selected' }} @endif>Lakh</option>
                </select>
                 <span style="color:red;">{{$errors->first('advance_value_unit')}}</span>
           </div>
        </div>
        <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Ratio</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputEmail" 
                name="ratio" value="{{$property->ratio}}">
            </div>
        </div>
         <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Fsi</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputEmail" 
                name="fsi" value="{{$property->fsi}}">
            </div>
        </div>
         <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Other Category</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputEmail" 
                name="other_category" value="{{$property->other_category}}">
            </div>
        </div>
       <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Project Category</label>
             <div class="col-xs-10">
                <select name="project_category">
                  <option value="" >Select Project Category</option>  
                  <option value="Apartments" @if($property->category=="Apartments") {{ 'selected' }} @endif>Apartments</option>
                  <option value="Township" @if($property->category=="Township") {{ 'selected' }} @endif>Township</option>
                  <option value="Hotel" @if($property->category=="Hotel") {{ 'selected' }} @endif>Hotel</option>
                  <option value="Resort" @if($property->category=="Resort") {{ 'selected' }} @endif>Resort</option>
                  <option value="Industrial" @if($property->category=="Industrial") {{ 'selected' }} @endif>Industrial</option>
                  <option value="Villa" @if($property->category=="Villa") {{ 'selected' }} @endif>Villa</option>
                  <option value="IT Park" @if($property->category=="IT Park") {{ 'selected' }} @endif>IT Park</option>
                  <option value="Plotting" @if($property->category=="Plotting") {{ 'selected' }} @endif>Plotting</option>
                  <option value="Retail/Shopping" @if($property->category=="Retail/Shopping") {{ 'selected' }} @endif>Retail/Shopping</option>
                  <option value="Office" @if($property->category=="Office") {{ 'selected' }} @endif>Office</option>
                </select>
                 <span style="color:red;">{{$errors->first('project_category')}}</span>
           </div>
        </div>
        <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Owner Notes</label>
            <div class="col-xs-10">
                <input type="text" class="form-control" id="inputEmail" 
                name="owner_notes" value="{{$property->owner_notes}}">
            </div>
        </div>
        <div class="form-group">
            <label for="inputEmail" class="control-label col-xs-2">Upload documents</label>
            <div class="col-xs-10">
                <input type="file" class="form-control" id="inputEmail" placeholder="size"
                name="documents[]" multiple="multiple">
            </div>
        </div> 
      
        <div class="form-group">
            <div class="col-xs-offset-2 col-xs-10">
                <button type="submit" class="btn btn-primary">submit</button>
            </div>
        </div>
    </form>
</div>

</body>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBqAa1nsNtFCwNnGj8_ux8XOsTFGzypHQs&libraries=places&callback=initAutocomplete"
        async defer></script>
 <script type="text/javascript">

var placeSearch, autocomplete;
var componentForm = {
  street_number: 'short_name',
  route: 'long_name',
  locality: 'long_name',
  administrative_area_level_1: 'short_name',
  country: 'long_name',
  postal_code: 'short_name'
};

function initAutocomplete() 
{
  autocomplete = new google.maps.places.Autocomplete(
  (document.getElementById('autocomplete')),
      {types: ['geocode']});
  autocomplete.addListener('place_changed', fillInAddress);
}

function fillInAddress() 
{
  var place = autocomplete.getPlace();

  for (var component in componentForm) {
    document.getElementById(component).value = '';
    document.getElementById(component).disabled = false;
  }
  for (var i = 0; i < place.address_components.length; i++) {
    var addressType = place.address_components[i].types[0];
    if (componentForm[addressType]) {
      var val = place.address_components[i][componentForm[addressType]];
      document.getElementById(addressType).value = val;
    }
  }
}
function geolocate() 
{
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
      var geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
      };
      var circle = new google.maps.Circle({
        center: geolocation,
        radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());
    });
  }
}
 </script>   
</html>                                 		