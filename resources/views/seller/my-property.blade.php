@extends('layouts.master')

@section('content')
	<div class="row">
		<h1 class="text-center">My Properties</h1>
		<table id="property-table" class="table table-bordered table-hover">
			<thead>
				<th>Title</th>
				<th>Details</th>
				<th>Category</th>
				<th>Size</th>
				<th>Fsi</th>
				<th>Land Title</th>
				<th>Land Zone</th>
				<th>Land Frontage</th>
				<th>Access Road Width</th>
				<th>Other Category</th>
				<th>Ratio</th>
				<th>Market Value</th>
				<th>Advance</th>
				<th>Owner Notes</th>
				<th>Actions</th>
			</thead>
			@foreach ($my_properties as $d)
			<tr>
				<td>{{ $d->title }}</td>
				<td>{{ $d->details }}</td>
				<td>{{ $d->category }}</td>
				<td>{{ $d->size }} {{ $d->sizeunit }}</td>
				<td>{{ $d->fsi }}</td>
				<td>{{ $d->land_title }}</td>
				<td>{{ $d->land_zone }} {{ $d->land_zone_unit }}</td>
				<td>{{ $d->land_frontage }} {{ $d->land_frontage_unit }}</td>
				<td>{{ $d->access_road_width }} {{ $d->road_width_unit }}</td>
				<td>{{ $d->other_category }}</td>
				<td>{{ $d->ratio }}</td>
				<td>{{ $d->market_value }} {{ $d->market_value_unit }}</td>
				<td>{{ $d->advance }} {{ $d->advance_value_unit }}</td>
				<td>{{ $d->owner_notes }}</td>
                <td><a class="btn btn-primary" href="{{ URL::to('property-list/show/'.$d->slug) }}">View</a> 
                	<a class="btn btn-primary" href="{{ URL::to('edit-property/'.$d->slug) }}">Edit</a> 
                    <a class="btn btn-primary" onclick="return confirm('Are you sure?')"  href="{{ URL::to('delete-property/'.$d->slug) }}">Delete</a>
                </td>
			</tr>
			@endforeach
		</table>
	</div>
@endsection