@extends('layouts.master_user')

@section('content')
@include('partials.header')
<section id="jvm-recently">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="rect-property">
          <h2 class="text-center">Recently added Properties</h2>
          <div id="sp-two" class="owl-carousel owl-theme">
            <div class="item">
              <div class="rect-inner">
                <img class="img-responsive" src="images/8.jpg" alt="">
                <div class="rect-info">
                  <h4>Brigade Enterprises Ltd</h4>
                  <ul class="list-inline">
                    <li><span>LOCATION<img src="images/location.png"></span></li>
                    <li>Doddballapur</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>PLOT AREA<img src="images/area.png"></span></li>
                    <li>174240 Sq.ft (4 Acres)</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="rect-inner">
                <img class="img-responsive" src="images/10.jpg" alt="">
                <div class="rect-info">
                  <h4>Brigade Enterprises Ltd</h4>
                  <ul class="list-inline">
                    <li><span>LOCATION<img src="images/location.png"></span></li>
                    <li>Doddballapur</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>PLOT AREA<img src="images/area.png"></span></li>
                    <li>174240 Sq.ft (4 Acres)</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="rect-inner">
                <img class="img-responsive" src="images/9.jpg" alt="">
                <div class="rect-info">
                  <h4>Brigade Enterprises Ltd</h4>
                  <ul class="list-inline">
                    <li><span>LOCATION<img src="images/location.png"></span></li>
                    <li>Doddballapur</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>PLOT AREA<img src="images/area.png"></span></li>
                    <li>174240 Sq.ft (4 Acres)</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="rect-inner">
                <img class="img-responsive" src="images/11.jpg" alt="">
                <div class="rect-info">
                  <h4>Brigade Enterprises Ltd</h4>
                  <ul class="list-inline">
                    <li><span>LOCATION<img src="images/location.png"></span></li>
                    <li>Doddballapur</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>PLOT AREA<img src="images/area.png"></span></li>
                    <li>174240 Sq.ft (4 Acres)</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="rect-inner">
                <img class="img-responsive" src="images/8.jpg" alt="">
                <div class="rect-info">
                  <h4>Brigade Enterprises Ltd</h4>
                  <ul class="list-inline">
                    <li><span>LOCATION<img src="images/location.png"></span></li>
                    <li>Doddballapur</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>PLOT AREA<img src="images/area.png"></span></li>
                    <li>174240 Sq.ft (4 Acres)</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="rect-inner">
                <img class="img-responsive" src="images/10.jpg" alt="">
                <div class="rect-info">
                  <h4>Brigade Enterprises Ltd</h4>
                  <ul class="list-inline">
                    <li><span>LOCATION<img src="images/location.png"></span></li>
                    <li>Doddballapur</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>PLOT AREA<img src="images/area.png"></span></li>
                    <li>174240 Sq.ft (4 Acres)</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="rect-inner">
                <img class="img-responsive" src="images/9.jpg" alt="">
                <div class="rect-info">
                  <h4>Brigade Enterprises Ltd</h4>
                  <ul class="list-inline">
                    <li><span>LOCATION<img src="images/location.png"></span></li>
                    <li>Doddballapur</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>PLOT AREA<img src="images/area.png"></span></li>
                    <li>174240 Sq.ft (4 Acres)</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="rect-inner">
                <img class="img-responsive" src="images/11.jpg" alt="">
                <div class="rect-info">
                  <h4>Brigade Enterprises Ltd</h4>
                  <ul class="list-inline">
                    <li><span>LOCATION<img src="images/location.png"></span></li>
                    <li>Doddballapur</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>PLOT AREA<img src="images/area.png"></span></li>
                    <li>174240 Sq.ft (4 Acres)</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="rect-requirement">
          <h2 class="text-center">Recently added Requirements</h2>
          <div id="sp-three" class="owl-carousel owl-theme">
            <div class="item">
              <div class="rect-inner">
                <h3>Brigade Enterprises Ltd</h3>
                <div class="rect-info">
                  <div class="rect-extrainfo">
                    <p class="rect-p">REQUIRED AREA</p>
                    <p><strong>1500 - 1700 sq feet </strong></p>
                  </div>
                  <div class="rect-extrainfo">
                    <p class="rect-p">BUDGET</p>
                    <p><strong>25 - 30 Lacs </strong></p>
                  </div>
                  <div class="rect-extrainfo">
                    <p class="rect-p">LOCATION</p>
                    <p><strong>Yelahanka, Bangalore </strong></p>
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="rect-inner">
                <h3>Brigade Enterprises Ltd</h3>
                <div class="rect-info">
                  <div class="rect-extrainfo">
                    <p class="rect-p">REQUIRED AREA</p>
                    <p><strong>1500 - 1700 sq feet </strong></p>
                  </div>
                  <div class="rect-extrainfo">
                    <p class="rect-p">BUDGET</p>
                    <p><strong>25 - 30 Lacs </strong></p>
                  </div>
                  <div class="rect-extrainfo">
                    <p class="rect-p">LOCATION</p>
                    <p><strong>Yelahanka, Bangalore </strong></p>
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="rect-inner">
                <h3>Brigade Enterprises Ltd</h3>
                <div class="rect-info">
                  <div class="rect-extrainfo">
                    <p class="rect-p">REQUIRED AREA</p>
                    <p><strong>1500 - 1700 sq feet </strong></p>
                  </div>
                  <div class="rect-extrainfo">
                    <p class="rect-p">BUDGET</p>
                    <p><strong>25 - 30 Lacs </strong></p>
                  </div>
                  <div class="rect-extrainfo">
                    <p class="rect-p">LOCATION</p>
                    <p><strong>Yelahanka, Bangalore </strong></p>
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="rect-inner">
                <h3>Brigade Enterprises Ltd</h3>
                <div class="rect-info">
                  <div class="rect-extrainfo">
                    <p class="rect-p">REQUIRED AREA</p>
                    <p><strong>1500 - 1700 sq feet </strong></p>
                  </div>
                  <div class="rect-extrainfo">
                    <p class="rect-p">BUDGET</p>
                    <p><strong>25 - 30 Lacs </strong></p>
                  </div>
                  <div class="rect-extrainfo">
                    <p class="rect-p">LOCATION</p>
                    <p><strong>Yelahanka, Bangalore </strong></p>
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="rect-inner">
                <h3>Brigade Enterprises Ltd</h3>
                <div class="rect-info">
                  <div class="rect-extrainfo">
                    <p class="rect-p">REQUIRED AREA</p>
                    <p><strong>1500 - 1700 sq feet </strong></p>
                  </div>
                  <div class="rect-extrainfo">
                    <p class="rect-p">BUDGET</p>
                    <p><strong>25 - 30 Lacs </strong></p>
                  </div>
                  <div class="rect-extrainfo">
                    <p class="rect-p">LOCATION</p>
                    <p><strong>Yelahanka, Bangalore </strong></p>
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="rect-inner">
                <h3>Brigade Enterprises Ltd</h3>
                <div class="rect-info">
                  <div class="rect-extrainfo">
                    <p class="rect-p">REQUIRED AREA</p>
                    <p><strong>1500 - 1700 sq feet </strong></p>
                  </div>
                  <div class="rect-extrainfo">
                    <p class="rect-p">BUDGET</p>
                    <p><strong>25 - 30 Lacs </strong></p>
                  </div>
                  <div class="rect-extrainfo">
                    <p class="rect-p">LOCATION</p>
                    <p><strong>Yelahanka, Bangalore </strong></p>
                  </div>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="rect-inner">
                <h3>Brigade Enterprises Ltd</h3>
                <div class="rect-info">
                  <div class="rect-extrainfo">
                    <p class="rect-p">REQUIRED AREA</p>
                    <p><strong>1500 - 1700 sq feet </strong></p>
                  </div>
                  <div class="rect-extrainfo">
                    <p class="rect-p">BUDGET</p>
                    <p><strong>25 - 30 Lacs </strong></p>
                  </div>
                  <div class="rect-extrainfo">
                    <p class="rect-p">LOCATION</p>
                    <p><strong>Yelahanka, Bangalore </strong></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="why-us">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-12 col-md-12">
        <h3 class="text-center">Why Choose Us?</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-4 col-md-4">
        <div class="why-us-inner">
          <div class="why-us-position text-center">
            <img class="img-responsive" src="images/5.png" alt="">
            <h4>Developers</h4>
          </div>
          <div class="why-us-content">
            <ul>
              <li>Professional treatment</li>
              <li>Transparent dealing</li>
              <li>Land availability across India</li>
              <li>Multiple land parcels to choose from  for any deal</li>
              <li>Filter by criteria</li>
              <li>Set up property alerts</li>
              <li>Instantly access initial land documents</li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4">
        <div class="why-us-inner">
          <div class="why-us-position text-center">
            <img class="img-responsive" src="images/6.png" alt="">
            <h4>Land Owners</h4>
          </div>
          <div class="why-us-content">
            <ul>
              <li>Professional treatment</li>
              <li>Transparent dealing</li>
              <li>Direct partnership with numerous developers</li>
              <li>Access to outstation developers &amp; MNCs</li>
              <li>Secure document storage &amp; sharing</li>
              <li>Choose best offer from various developers</li>
              <li>Assured gift on successful transaction</li>
            </ul>
          </div>
        </div>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4">
        <div class="why-us-inner">
          <div class="why-us-position text-center">
            <img class="img-responsive" src="images/7.png" alt="">
            <h4>Real Estate Agents</h4>
          </div>
          <div class="why-us-content">
            <ul>
              <li>Professional treatment</li>
              <li>Transparent dealing</li>
              <li>Direct partnership with numerous developers</li>
              <li>Use our infrastructure &amp; brand name to enhance your credibility<sup>*</sup></li>
              <li>Work directly on live requirements</li>
              <li>Set up property alerts</li>
              <li>Pan India Coverage</li>
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<section id="partner-deals">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12">
        <div class="rectproperty">
          <h2 class="text-center">Our Partners</h2>
          <div id="sp-one" class="owl-carousel owl-theme">
            <div class="item">
              <img src="images/12.png" alt="" class="img-responsive">
            </div>
            <div class="item">
              <img src="images/13.png" alt="" class="img-responsive">
            </div>
            <div class="item">
              <img src="images/14.png" alt="" class="img-responsive">
            </div>
            <div class="item">
              <img src="images/15.png" alt="" class="img-responsive">
            </div>
            <div class="item">
              <img src="images/16.png" alt="" class="img-responsive">
            </div>
            <div class="item">
              <img src="images/17.png" alt="" class="img-responsive">
            </div>
          </div>
        </div>
        <div class="rect-property">
          <h2 class="text-center">Recent Deals</h2>
          <div id="sp-two" class="owl-carousel owl-theme">
            <div class="item">
              <div class="rect-inner">
                <img class="img-responsive" src="images/10.jpg" alt="">
                <div class="rect-info">
                  <h4><a href="">Brigade Enterprises Ltd</a></h4>
                  <ul class="list-inline">
                    <li><span>LOCATION<img src="images/location.png"></span></li>
                    <li>Doddballapur</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>PLOT AREA<img src="images/area.png"></span></li>
                    <li>174240 Sq.ft (4 Acres)</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>RATIO<img src="images/ratio.png"></span></li>
                    <li>60 : 40</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>ADVANCE<img src="images/perc.png"></span></li>
                    <li>15%</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="rect-inner">
                <img class="img-responsive" src="images/8.jpg" alt="">
                <div class="rect-info">
                  <h4><a href="">Brigade Enterprises Ltd</a></h4>
                  <ul class="list-inline">
                    <li><span>LOCATION<img src="images/location.png"></span></li>
                    <li>Doddballapur</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>PLOT AREA<img src="images/area.png"></span></li>
                    <li>174240 Sq.ft (4 Acres)</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>RATIO<img src="images/ratio.png"></span></li>
                    <li>60 : 40</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>ADVANCE<img src="images/perc.png"></span></li>
                    <li>15%</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="rect-inner">
                <img class="img-responsive" src="images/9.jpg" alt="">
                <div class="rect-info">
                  <h4><a href="">Brigade Enterprises Ltd</a></h4>
                  <ul class="list-inline">
                    <li><span>LOCATION<img src="images/location.png"></span></li>
                    <li>Doddballapur</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>PLOT AREA<img src="images/area.png"></span></li>
                    <li>174240 Sq.ft (4 Acres)</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>RATIO<img src="images/ratio.png"></span></li>
                    <li>60 : 40</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>ADVANCE<img src="images/perc.png"></span></li>
                    <li>15%</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="rect-inner">
                <img class="img-responsive" src="images/11.jpg" alt="">
                <div class="rect-info">
                  <h4><a href="">Brigade Enterprises Ltd</a></h4>
                  <ul class="list-inline">
                    <li><span>LOCATION<img src="images/location.png"></span></li>
                    <li>Doddballapur</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>PLOT AREA<img src="images/area.png"></span></li>
                    <li>174240 Sq.ft (4 Acres)</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>RATIO<img src="images/ratio.png"></span></li>
                    <li>60 : 40</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>ADVANCE<img src="images/perc.png"></span></li>
                    <li>15%</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="rect-inner">
                <img class="img-responsive" src="images/8.jpg" alt="">
                <div class="rect-info">
                  <h4><a href="">Brigade Enterprises Ltd</a></h4>
                  <ul class="list-inline">
                    <li><span>LOCATION<img src="images/location.png"></span></li>
                    <li>Doddballapur</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>PLOT AREA<img src="images/area.png"></span></li>
                    <li>174240 Sq.ft (4 Acres)</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>RATIO<img src="images/ratio.png"></span></li>
                    <li>60 : 40</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>ADVANCE<img src="images/perc.png"></span></li>
                    <li>15%</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="rect-inner">
                <img class="img-responsive" src="images/10.jpg" alt="">
                <div class="rect-info">
                  <h4><a href="">Brigade Enterprises Ltd</a></h4>
                  <ul class="list-inline">
                    <li><span>LOCATION<img src="images/location.png"></span></li>
                    <li>Doddballapur</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>PLOT AREA<img src="images/area.png"></span></li>
                    <li>174240 Sq.ft (4 Acres)</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>RATIO<img src="images/ratio.png"></span></li>
                    <li>60 : 40</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>ADVANCE<img src="images/perc.png"></span></li>
                    <li>15%</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="rect-inner">
                <img class="img-responsive" src="images/8.jpg" alt="">
                <div class="rect-info">
                  <h4><a href="">Brigade Enterprises Ltd</a></h4>
                  <ul class="list-inline">
                    <li><span>LOCATION<img src="images/location.png"></span></li>
                    <li>Doddballapur</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>PLOT AREA<img src="images/area.png"></span></li>
                    <li>174240 Sq.ft (4 Acres)</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>RATIO<img src="images/ratio.png"></span></li>
                    <li>60 : 40</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>ADVANCE<img src="images/perc.png"></span></li>
                    <li>15%</li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="item">
              <div class="rect-inner">
                <img class="img-responsive" src="images/9.jpg" alt="">
                <div class="rect-info">
                  <h4><a href="">Brigade Enterprises Ltd</a></h4>
                  <ul class="list-inline">
                    <li><span>LOCATION<img src="images/location.png"></span></li>
                    <li>Doddballapur</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>PLOT AREA<img src="images/area.png"></span></li>
                    <li>174240 Sq.ft (4 Acres)</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>RATIO<img src="images/ratio.png"></span></li>
                    <li>60 : 40</li>
                  </ul>
                  <ul class="list-inline">
                    <li><span>ADVANCE<img src="images/perc.png"></span></li>
                    <li>15%</li>
                  </ul>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <hr>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-md-8">
        <ul id="jvm-navlist" class="list-inline">
          <li><a href="">About Us</a></li>
          <li> <a href="">Recent &amp; Updates</a></li>
          <li><a href="">Careers</a></li>
          <li><a href="">Contact</a></li>
          <li><a href="">Sitemap</a></li>
          <li><a href="">Privacy &amp; Legal</a></li>
        </ul>
      </div>
      <div class="col-xs-12 col-sm-4 col-md-4">
        <p class="copy-2017">&copy;2017 Verified <a href="">Joint Ventures Pvt. Ltd</a></p>
      </div>
    </div>
  </div>
</section>
@endsection
