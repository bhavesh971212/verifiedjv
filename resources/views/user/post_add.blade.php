@extends('layouts.master_user')

@section('content')
@include('partials.inner_header')
<section class="post_section_main">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="width_80_per white_bg_color" id="post_section_form">
          <article>
            <div class="row">
              <div class="col-md-6">
                <h3 class="SourceSemiBold">Post a Property</h3>
              </div>
              <div class="col-md-6">
                <div class="text-right my_custom_drop_btn">
                  <a href="post-requirement.php">Post Your Requirements&nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i></a>
                </div>
              </div>
            </div>
          </article>
          <article>
            <p class="special_badge">Property Details</p>
            <div class="form_section SourceSemiBold">
              <form action="">
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="">Project Name</label>
                      <input type="text" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="">Title<sup>*</sup></label>
                      <select class="form-control selectpicker">
                        <option value="">Residential</option>
                        <option value="">Residential</option>
                        <option value="">Residential</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="">Land Zone<sup>*</sup></label>
                      <select class="form-control selectpicker">
                        <option value="">Residential</option>
                        <option value="">Residential</option>
                        <option value="">Residential</option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="">Property Catergory<sup>*</sup></label>
                      <select class="form-control selectpicker">
                        <option value="">Residential</option>
                        <option value="">Residential</option>
                        <option value="">Residential</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="">Property area<sup>*</sup></label>
                      <div class="row">
                        <div class="col-md-6">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-6 bg_select">
                          <select class="form-control selectpicker">
                            <option value="">Residential</option>
                            <option value="">Residential</option>
                            <option value="">Residential</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="">Expected market value</label>
                      <div class="row">
                        <div class="col-md-6">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-6 bg_select">
                          <select class="form-control selectpicker">
                            <option value="">Residential</option>
                            <option value="">Residential</option>
                            <option value="">Residential</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="">Deposit<sup>*</sup></label>
                      <div class="row">
                        <div class="col-md-6">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-6 bg_select">
                          <select class="form-control selectpicker">
                            <option value="">Residential</option>
                            <option value="">Residential</option>
                            <option value="">Residential</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="">Ratio (Owner : Builder)<sup>*</sup></label>
                      <div class="row common">
                        <div class="col-md-6">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-6">
                          <input type="text" class="form-control">
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="">Road width<sup>*</sup></label>
                      <div class="row">
                        <div class="col-md-6">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-6 bg_select">
                          <select class="form-control selectpicker">
                            <option value="">Residential</option>
                            <option value="">Residential</option>
                            <option value="">Residential</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label for="">Land Frontage</label>
                      <div class="row">
                        <div class="col-md-6">
                          <input type="text" class="form-control">
                        </div>
                        <div class="col-md-6 bg_select">
                          <select class="form-control selectpicker">
                            <option value="">Residential</option>
                            <option value="">Residential</option>
                            <option value="">Residential</option>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="">Property description<sup>*</sup></label>
                      <textarea class="form-control" rows="4" placeholder="4 acre joint venture land available in pune. This joint venture property in pune is situated at Kanhe phata, near Sai Baba Sevadham."></textarea>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="">Any other criteria</label>
                      <input type="text" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="">FAR</label>
                      <input type="text" class="form-control">
                    </div>
                  </div>
                  <div class="col-md-9">
                    <div class="form-group">
                      <label for="">Owner’s Notes</label>
                      <input type="text" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="">City<sup>*</sup></label>
                      <select class="form-control selectpicker">
                        <option value=""></option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-3">
                    <div class="form-group">
                      <label for="">City locality</label>
                      <select class="form-control selectpicker">
                        <option value=""></option>
                      </select>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="">Address</label>
                      <input type="text" class="form-control" placeholder="60 & 61 1st Floor, Dr. Rajkumar Road, Rajaji Nagar, , Karnataka 560021">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <label for="">Paste the URL link</label>
                      <input type="text" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="form-group">
                      <h4 class="text-center">OR</h4>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="">Latitude</label>
                      <input type="text" class="form-control">
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="form-group">
                      <label for="">Longitude</label>
                      <input type="text" class="form-control">
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div class="property_map form-group">
                      <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3887.7270655947846!2d77.55686221428437!3d12.98930081800538!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3bae3d9a0fffff8b%3A0xdac2494122c74956!2sIndglobal!5e0!3m2!1sen!2sin!4v1491542320160" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                  </div>
                </div>
              </form>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <h4>Property Images</h4>
                    <form action="/images/result" id="my-awesome-dropzone_1" enctype="multipart/form-data" class="dropzone dz-file-preview">
                        <input name="file" type="hidden" multiple  />
                        <div class="dz-message" data-dz-message="">
                          <h3 class="SourceSemiBold">Drag and Drop Images Here </h3>
                          <p>photos format must be in .jpg, gif, .png size &lt;5mb </p><br>
                          <img src="images/icons/gal_icon.png" width="40" alt="">
                          <h3>OR</h3>
                          <span>Select Files</span>
                        </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <h4>Property Documents</h4>
                    <form action="/images/result" id="my-awesome-dropzone_2" enctype="multipart/form-data" class="dropzone dz-file-preview">
                        <input name="file" type="hidden" multiple  />
                        <div class="dz-message" data-dz-message="">
                          <h3 class="SourceSemiBold">Drag and Drop Pdf, jgp, png Here</h3>
                          <p>to upload (max 5mb) </p><br>
                          <img src="images/icons/upload_icon.png" width="40" alt="">
                          <h3>OR</h3>
                          <span>Select Files</span>
                        </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="row">
                <div class="col-md-12">
                  <div class="form-group">
                    <ul class="list-inline">
                      <li><button type="submit" class="cmmn_btn save_btn">Save</button></li>
                      <li><button type="submit" class="cmmn_btn submit_btn">Submit</button></li>
                      <li><button type="reset" class="cmmn_btn cancel_btn">Cancel</button></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </article>
        </div>
      </div>
    </div>
  </div>
</section>
@include('partials.inner_footer')
<script type="text/javascript">
$(document).ready(function(){
  $("div#my-awesome-dropzone_1").dropzone({ url: "/upload" });
  $("div#my-awesome-dropzone_2").dropzone({ url: "/upload" });
});
</script>
@endsection
