@extends('layouts.master_user')

@section('content')
@include('partials.inner_header')

	<div class="post-ad-inner">
		<form action="post_add" method="POST" name="post_add">
		<input type="hidden" name="status" value="1">
		<!-- 1 = Submit, 2 = Save-->
			<div class="post-ad-details">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<ul class="list-inline post-border">
								<li><h3 class="post-color">Post a ads</h3></li>
								<li class="post-req">
									<div class="dropdown">
										<button class="btn btn-default dropdown-toggle" type="button" id="demo" data-toggle="dropdown">Post Your Requirement&nbsp;<span class="caret"></span></button>
										<ul class="dropdown-menu" role="menu" aria-labelledby="demo">
											<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Post a ads</a></li>
											<li role="presentation" class="divider"></li>
											<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Post a requirements</a></li>
										</ul>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="property-content">
					<p>Property Details</p>
				</div>
				<div class="container">
					<div class="post-form">
						{{ csrf_field() }}
						<div class="row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>Title<span class="requirement-span">*</span></label>
									<select id="basic" class="selectpicker" name="title">
										<option>Select Title</option>
										@foreach($zone as $z)
										<option value="{{$z->id}}">{{$z->zone_type}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>Zone<span class="requirement-span">*</span></label>
									<select id="basic" class="selectpicker" name="zone">
										<option>Select Zone</option>
										@foreach($zone as $z)
										<option value="{{$z->id}}">{{$z->zone_type}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>Project Category<span class="requirement-span">*</span></label>
									<select id="basic" name="project_cat" class="selectpicker">
										<option>Select Category</option>
										@foreach($project_cat as $pc)
										<option value="{{$pc->id}}">{{$pc->project_cat_type}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group form-marg">
									<label>Property area<span class="requirement-span">*</span></label>
									<div class="combine-input-select">
										<div class="row">
											<div class="col-xs-6 col-sm-6 col-md-6">
												<input type="text" name="property_area"  class="numbers_with_decimal">
											</div>
											<div class="col-xs-6 col-sm-6 col-md-6">
												<select id="basic" class="selectpicker" name="property_type">
												<option value="">Select Units</option>
										@foreach($property_type as $pt)
											<option value="{{ $pt->id }}">{{ $pt->property_type }}
											</option>
										@endforeach
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>Expected market value</label>
									<input type="text"  name="budget" class="only_numbers">
									<select id="basic" class="selectpicker" name="budget_type">
										<option value="">Select Price Unit</option>
										@foreach($price as $p)
											<option value="{{ $p->id }}">{{ $p->price_type }}
											</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>Token advance<span class="requirement-span">*</span></label>
									<input type="text"  name="token_amout" class="only_numbers">
									<select id="basic" class="selectpicker" name="token_type">
										<option value="">Select Area</option>
										@foreach($price as $p)
											<option value="{{ $p->id }}">{{ $p->price_type }}
											</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>Ratio (Owner : Builder) In %<span class="requirement-span">*</span></label>
									<input type="text"  name="ratio_owner" class="only_numbers ratio_owner">
									<input type="hidden"    name="ratio_builder" class="only_numbers ratio_builder">
									<input type="text" disabled  class="only_numbers ratio_builder">
									<span class="invalid_ratio"></span>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>Road width<span class="requirement-span">*</span></label>
									<input type="text" name="road_width"  class="numbers_with_decimal">
									<select id="basic" class="selectpicker" name="road_width_type">
										<option>Select Width</option>
										@foreach($roadwidth as $r)
										<option value="{{$r->id}}">{{$r->road_width_type}}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>Frontage<span class="requirement-span">*</span></label>
									<input type="text" name="frontage"  class="numbers_with_decimal">
									<select id="basic" class="selectpicker" name="frontage_type">
										<option>Select Frontage</option>
										@foreach($roadwidth as $r)
										<option value="{{$r->id}}">{{$r->road_width_type}}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
								<div class="form-group message-something">
									<label for="comment">Property description</label>
									<textarea class="form-control" rows="3" id="comment1" placeholder="4 acer joint venture land available in pune.This joint venture property in pune is suited at kanhe phata, near sai baba sevadham" name="property_description"></textarea>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
								<div class="form-group message-something">
									<label for="comment">Any Other Criteria</label>
									<textarea class="form-control" rows="3" id="comment1" placeholder="4 acer joint venture land available in pune.This joint venture property in pune is suited at kanhe phata, near sai baba sevadham" name="other_criteria"></textarea>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>FAR</label>
									<input type="text"  name="far_text">
								</div>
							</div>
							<div class="col-xs-12 col-sm-8 col-md-8">
								<div class="form-group">
									<label>Owner Notes</label>
									<input type="text" name="owner_notes"  class="">

								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="property-content">
					<p>Property Locality</p>
				</div>
				<div class="property-location">
					<div class="container">
						<div class="post-form">
							<div class="row">
								<div class="col-xs-12 col-sm-3 col-md-3">
									<div class="form-group">
										<input type="hidden" class="getcity" value="{{ URL::to('getCity') }}">
										<label>State</label>
										<select  name="state"  class="selectstate" id="state">
											<option value="">Select State</option>
											@foreach($state as $s)
											<option value="{{ $s->state_id }}">{{ $s->state_name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-3 col-md-3">
									<div class="form-group">
										<input type="hidden" class="getlocality" value="{{ URL::to('getLocality') }}">
										<label>City</label>
										<select name="city" class="selectcity" id="city">
											<option value="">Select City</option>
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label>City Locality<span class="requirement-span">*</span></label>
										<!-- <select id="locality" class="selectlocality" name="locality">
											<option value="">Select City Locality</option>
										</select> -->
										<input class="autocomplete
										" placeholder="Enter your address"
             							type="text" name="locality">
             							<input type="hidden" name="locality_lat" class="locality_lat" value="">
             							<input type="hidden" name="locality_lng" class="locality_lng" value="">
									</div>
								</div>
								<div class="col-xs-12 col-sm-6 col-md-6">
									<div class="form-group">
										<label>Address</label>
										<input type="text" class="form-control address" name="address" id="address" placeholder=" 60 &amp; 61 1st Floor,Dr Rajkumar road,Rajaji Nagar,Karnataka 560021">
										<input type="hidden" class="address_lat" name="address_lat">
										<input type="hidden" class="address_lng" name="address_lng">
									</div>
									<div class="form-group">
										<label>Lat</label>
										<input type="text" class="form-control latitude" name="address_lat2" >
									</div>
									<div class="form-group">
										<label>Long</label>
										<input type="text" class="form-control longitude" name="address_lng2" >
										<button class="btn btn-info showInMap">Locate In Map</button>
									</div>

								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12">
									<div style="width: 100%" class="post-iframe" id="map" >

									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="property-content">
					<p>Property Photos</p>
				</div>
				<div class="property-photo">
					<div class="container">
						<div class="post-form">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12">
									<div class="photoupload">
										<p>Photos format must be in .jpg, .gif &amp; .png. size < 5MB</p>
										<ul class="list-inline">
											<li>
												<label for="fileupload" class="fileupload"></label>
												<input type="file" name="fileupload" value="fileupload" id="fileupload">
												<i class="fa fa-plus-circle file-11 fa-3x" aria-hidden="true"></i>
											</li>
											<li>
												<label for="fileupload-1" class="fileupload"></label>
												<input type="file" name="fileupload-1" value="fileupload" id="fileupload">
												<i class="fa fa-plus-circle file-11 fa-3x" aria-hidden="true"></i>
											</li>
											<li>
												<label for="fileupload-2" class="fileupload"></label>
												<input type="file" name="fileupload-2" value="fileupload" id="fileupload">
												<i class="fa fa-plus-circle file-11 fa-3x" aria-hidden="true"></i>
											</li>
											<li>
												<label for="fileupload-3" class="fileupload"></label>
												<input type="file" name="fileupload-3" value="fileupload" id="fileupload">
												<i class="fa fa-plus-circle file-11 fa-3x" aria-hidden="true"></i>
											</li>
											<li>
												<label for="fileupload-4" class="fileupload"></label>
												<input type="file" name="fileupload-4" value="fileupload" id="fileupload">
												<i class="fa fa-plus-circle file-11 fa-3x" aria-hidden="true"></i>
											</li>
											<li>
												<label for="fileupload-5" class="fileupload"></label>
												<input type="file" name="fileupload-5" value="fileupload" id="fileupload">
												<i class="fa fa-plus-circle file-11 fa-3x" aria-hidden="true"></i>
											</li>
										</ul>
										<div class="photoupload-absolute">
											<p class="text-center main-photo">This will be our main photo</p>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12">
									<div class="photoupload">
										<ul class="list-inline">
											<li>
												<label for="fileupload-6" class="fileupload"></label>
												<input type="file" name="fileupload-6" value="fileupload" id="fileupload">
												<i class="fa fa-plus-circle file-11 fa-3x" aria-hidden="true"></i>
											</li>
											<li>
												<label for="fileupload-7" class="fileupload"></label>
												<input type="file" name="fileupload-7" value="fileupload" id="fileupload">
												<i class="fa fa-plus-circle file-11 fa-3x" aria-hidden="true"></i>
											</li>
											<li>
												<label for="fileupload-8" class="fileupload"></label>
												<input type="file" name="fileupload-8" value="fileupload" id="fileupload">
												<i class="fa fa-plus-circle file-11 fa-3x" aria-hidden="true"></i>
											</li>
											<li>
												<label for="fileupload-9" class="fileupload"></label>
												<input type="file" name="fileupload-9" value="fileupload" id="fileupload">
												<i class="fa fa-plus-circle file-11 fa-3x" aria-hidden="true"></i>
											</li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				{{-- <div class="property-content">
					<p>Contact Details</p>
				</div>
				<div class="contact-details">
					<div class="container">
						<div class="post-form">
							<div class="row">
								<div class="col-xs-12 col-sm-4 col-md-4">
									<div class="form-group">
										<label for="usr">Name</label>
										<input type="text" class="form-control" id="usr" placeholder="Brigade Developer" >
									</div>
								</div>
								<div class="col-xs-12 col-sm-4 col-md-4">
									<div class="form-group">
										<label for="pwd">Mobile Number</label>
										<input type="tel" name="usrtel" class="form-control" placeholder="+91 1234567890" >
										<div class="checkbox">
											<label><input type="checkbox">Display my number</label>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-4 col-md-4">
									<div class="form-group">
										<label for="email">Email Id</label>
										<input type="email" class="form-control" id="email" placeholder="info@brigadedeveloper.com" >
										<div class="checkbox">
											<label><input type="checkbox">Display my number</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div> --}}
				<div class="contact-details">
					<div class="container">
						<div class="post-form ">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12">
									<p class="tems-rules">By clicking 'Submit' you agree to our <a href="#">Terms of use</a> &amp;<a href="#"> Posting Rules</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="contact-details contat-pad">
					<div class="container">
						<div class="post-form ">
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-6">
									<ul class="list-inline">
										<li><button type="submit" class="btn  contact-submit" value="1">Submit</button></li>
										<li><button type="cancel" class="btn  contact-cancel">Cancel</button></li>
										<li><button type="submit" class="btn contact-submit" value="2">Save</button></li>
									</ul>
								</div>

							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
@include('partials.inner_footer')
 	<script>

 	$(document).ready(function(){

 	$('.autocomplete').geocomplete()
					  .bind("geocode:result", function(event, result){
					  		var lat = result.geometry.location.lat();
                  			var lng = result.geometry.location.lng();
                  			var add = result.formatted_address;
                  			$('.locality_lat').val(lat);
                  			$('.locality_lng').val(lng);
					  });

 	$('.contact-submit').on('click',function(e){
 		e.preventDefault();

 		if($(this).val() == 2)
			$('input[name=status]').val(2);
 		$.ajax({
 			type:"POST",
 			url:$(this).attr('action'),
 			data:$('form[name="post_add"]').serialize(),
 			success:function(res){
 				alert('success');
 			}
 		});
 	});

 	$('.ratio_owner').keyup(function(){
 		$('invalid_ratio').html('');
 		if($(this).val() > 100 || $(this).val() < 0){
 			$('.invalid_ratio').html('Invalid Range');
 			return false;
 		} else{
 			var ratio_builder = 100-$(this).val();
 			$('.ratio_builder').val(ratio_builder);
 		}

 	});


	     var geocoder;
		 var map;
		 var marker;
	     function initialize() {
	      	 geocoder = new google.maps.Geocoder();
	      	 var myLatLng  = new google.maps.LatLng(20.593684,78.962880);
    		 map = new google.maps.Map(document.getElementById("map"),
    			{
		        zoom: 4,
		        center: myLatLng,
		        mapTypeId: google.maps.MapTypeId.ROADMAP
    			});

    		marker = new google.maps.Marker(
		    {
		        position: myLatLng,
		        map: map,
		        title: 'India'
		    });
	    }

	    $('.showInMap').on('click' , function(e){
	    	  e.preventDefault();
	    	  marker.setMap(null);
	    	  var lat;
	    	  var lng;
	    	  if($('.address').val() != ''){
	    	  	  var address = $('.address').val();
			      geocoder.geocode( { 'address': address}, function(results, status) {
				    if (status == google.maps.GeocoderStatus.OK) {
				       lat = results[0].geometry.location.lat();
                  	   lng = results[0].geometry.location.lng();
                  	  map.setCenter(results[0].geometry.location);
				      map.setZoom(15);
				      marker = new google.maps.Marker({
				          map: map,
				          position: results[0].geometry.location
				      });
				     $('.address_lat').val(lat);
				     $('.address_lng').val(lng);
				    } else {
				      alert('Geocode was not successful for the following reason: ' + status);
				    }
			  	 });
	    	  } else{
	    	  		lat = $('.latitude').val();
	    	  		lng = $('.longitude').val();
	    	  		var myLatlng = new google.maps.LatLng(lat, lng);
		    		  map = new google.maps.Map(document.getElementById("map"),
		    		    {
					        zoom: 15,
					        center: myLatlng,
					        mapTypeId: google.maps.MapTypeId.ROADMAP
					    });
		    		  marker = new google.maps.Marker(
				    	{
				       	 position: myLatlng,
				       	 map: map,
				    	});
			  }
		});

	      google.maps.event.addDomListener(window, 'load', initialize);


	});
    </script>
@endsection
