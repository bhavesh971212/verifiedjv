@extends('layouts.master_user')

@section('content')
@include('partials.inner_header')
<section class="post_section_main">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="width_80_per white_bg_color" id="post_section_form">
					<article>
						<div class="row">
							<div class="col-md-6">
								<h3 class="SourceSemiBold">Post your requirements</h3>
							</div>
							<div class="col-md-6">
								<div class="text-right my_custom_drop_btn">
									<a href="post-add.php">Post a Property&nbsp;<i class="fa fa-angle-down" aria-hidden="true"></i></a>
								</div>
							</div>
						</div>
					</article>
					<article>
						<p class="special_badge">Requirement Details</p>
						<div class="form_section SourceSemiBold">
							<form action="">
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label for="">I want a<sup>*</sup></label>
											<select class="form-control selectpicker">
												<option value="">Residential</option>
												<option value="">Residential</option>
												<option value="">Residential</option>
											</select>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label for="">I am interested in<sup>*</sup></label>
											<select class="form-control selectpicker">
												<option value="">Residential</option>
												<option value="">Residential</option>
												<option value="">Residential</option>
											</select>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label for="">Property Size<sup>*</sup></label>
											<div class="row">
												<div class="col-md-6">
													<input type="text" class="form-control">
												</div>
												<div class="col-md-6 bg_select">
													<select class="form-control selectpicker">
														<option value="">Residential</option>
														<option value="">Residential</option>
														<option value="">Residential</option>
													</select>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-4">
										<div class="form-group">
											<label for="">Approx budget<sup>*</sup></label>
											<div class="row">
												<div class="col-md-6">
													<input type="text" class="form-control">
												</div>
												<div class="col-md-6 bg_select">
													<select class="form-control selectpicker">
														<option value="">Residential</option>
														<option value="">Residential</option>
														<option value="">Residential</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label for="">City<sup>*</sup></label>
											<select class="form-control selectpicker">
												<option value="">Residential</option>
												<option value="">Residential</option>
												<option value="">Residential</option>
											</select>
										</div>
									</div>
									<div class="col-md-4">
										<div class="form-group">
											<label for="">City Localities (Upto 5)<sup>*</sup></label>
											<input type="text" class="form-control" id="enter_area">
											<div class="appended_areas">
												<ul></ul>
											</div>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<label for="">Few words about my requirement</label>
											<textarea rows="10" class="form-control" placeholder="Few word..."></textarea>
										</div>
									</div>
								</div>
								<div class="row">
									<div class="col-md-12">
										<div class="form-group">
											<p class="agree_rule">By clicking 'Submit' you agree to our <a href="javascript:void(0);">Terms Of Use</a> &amp; <a href="">Posting Rules.</a></p>
											<ul class="list-inline">
												<li><button type="submit" class="cmmn_btn save_btn">Save</button></li>
												<li><button type="submit" class="cmmn_btn submit_btn">Submit</button></li>
												<li><button type="reset" class="cmmn_btn cancel_btn">Cancel</button></li>
											</ul>
										</div>
									</div>
								</div>
							</form>
						</div>
					</article>
				</div>
			</div>
		</div>
	</div>
</section>
@include('partials.inner_footer')
@endsection
