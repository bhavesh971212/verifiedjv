@extends('layouts.master_user')

@section('content')
@include('partials.inner_header')

	<div class="post-ad-inner">
		<form action="post_requirement" method="post" name="post_requirement">
		<input type="hidden" name="status" value="1">
		<!-- 1 = Submit, 2 = Save--> 
			<div class="post-ad-details">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 col-sm-12 col-md-12">
							<ul class="list-inline post-border">
								<li><h3 class="post-color">Post Your Requirement</h3></li>
								<li class="post-req">
									<div class="dropdown">
										<button class="btn btn-default dropdown-toggle" type="button" id="demo" data-toggle="dropdown">Post Ads&nbsp;<span class="caret"></span></button>
										<ul class="dropdown-menu" role="menu" aria-labelledby="demo">
											<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Post Ads</a></li>
											<li role="presentation"><a role="menuitem" tabindex="-1" href="#">Post Requirements</a></li>
										</ul>
									</div>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="property-content">
					<p>Property Details</p>
				</div>
				<div class="container">
					<div class="post-form">
						<div class="row">
							{{ csrf_field() }}
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>I am<span class="requirement-span"><sup>*</sup></span></label>

									<select id="basic" name="seeker_type" class="selectpicker">
									<option value="">Select</option>
									@foreach($seeker_type as $s)
										<option value="{{ $s->id }}">{{ $s->seeker_type }}</option>
									@endforeach
									</select>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>I want to buy a<span class="requirement-span"><sup>*</sup></span></label>
									<select id="basic" name="interested_type" class="selectpicker">
										<option value="">Select</option>
										@foreach($interested_type as $it)
										<option value="{{ $it->id }}">{{ $it->interested_type }}</option>
										@endforeach
									</select>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>Re-sale Plot Only<span class="requirement-span"><sup>*</sup></span></label>
									<select id="basic" name="resale_type" class="selectpicker">
										<option value="">Select Plot</option>
										@foreach($resale_type as $r)
											<option value="{{ $r->id }}">{{ $r->resale_type }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>Property Area<span class="requirement-span"><sup>*</sup></span></label>
									<div class="combine-input-select">
										<div class="row">
											<div class="col-xs-6 col-sm-6 col-md-6">
												<input type="text" name="min_plot_size"  class="numbers_with_decimal">
												<input type="text"  name="max_plot_size" class="numbers_with_decimal">
											</div>
											<div class="col-xs-6 col-sm-6 col-md-6">
												<select id="basic" name="property_type" class="selectpicker">
													<option value="">Select Units</option>
													@foreach($property_type as $pt)
														<option value="{{ $pt->id }}">{{ $pt->property_type }}
														</option>
													@endforeach
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>Approx Budget</label>
											<input type="text"  name="min_budget" class="only_numbers">
											<input type="text" name="max_budget" class="only_numbers">
											<select id="basic" name="price_type" class="selectpicker">
												<option value="">Select Price</option>
												@foreach($price as $p)
												<option value="{{ $p->id }}">{{ $p->price_type }}
														</option>
												@endforeach
											</select>
								</div>
							</div>
							<div class="col-xs-12 col-sm-4 col-md-4">
								<div class="form-group">
									<label>Preferred Facing Direction</label>
									<select id="basic" name="direction" class="selectpicker">
										<option value="">Select</option>
										@foreach($direction as $d)
										<option value="{{ $d->id }}">{{ $d->direction_name }}</option>
										@endforeach
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12 col-sm-12 col-md-12">
								<div class="form-group message-something">
									<label for="comment">Few Words About My Requirements</label>
									<textarea name="requirement_description" class="form-control" rows="3" id="comment1" placeholder="Few words...." ></textarea>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="property-content">
					<p>Property Locality</p>
				</div>
				<div class="property-location location-pad-doun">
					<div class="container">
						<div class="post-form">
							<div class="row">
								<div class="col-xs-12 col-sm-3 col-md-3">
									<div class="form-group">
										<input type="hidden" class="getcity" value="{{ URL::to('getCity') }}">
										<label>State</label>
										<select  name="state"  class="selectstate" id="state">
											<option value="">Select State</option>
											@foreach($state as $s)
											<option value="{{ $s->state_id }}">{{ $s->state_name }}</option>
											@endforeach
										</select>
									</div>
								</div>
								<div class="col-xs-12 col-sm-3 col-md-3">
									<div class="form-group">
										<input type="hidden" class="getlocality" value="{{ URL::to('getLocality') }}">
										<label>City</label>
										<select name="city" class="selectcity" id="city">
											<option value="">Select City</option>
										</select>
									</div>
								</div>
								<div class="col-sm-offset-1 col-md-offset-1  col-xs-12 col-sm-3 col-md-3">
									<div class="form-group">
										<label>City Location (upto 5)<span class="requirement-span"><sup>*</sup></span></label>
										<select id="locality" class="selectlocality">
											<option value="">Select City Locality</option>
										</select>
										<input class="autocomplete" placeholder="Enter your address"
             							type="text"></input>
									</div>
									<div class="append_area">
										<ul class="list-inline">
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<!-- <div class="property-content">
					<p>Contact Details</p>
				</div> 
				<div class="contact-details">
					<div class="container">
						<div class="post-form">
							<div class="row">
								<div class="col-xs-12 col-sm-4 col-md-4">
									<div class="form-group">
										<label for="usr">Name</label>
										<input type="text" class="form-control" id="usr" placeholder="Brigade Developer" required="">
									</div>
								</div>
								<div class="col-xs-12 col-sm-4 col-md-4">
									<div class="form-group">
										<label for="pwd">Mobile Number</label>
										<input type="tel" name="usrtel" class="form-control" placeholder="+91 1234567890" required="">
										<div class="display_check_none">
											<input type="checkbox" class="custom_checkbox" id="disp_numb">
											<label for="disp_numb" class="custom_checkbox_label">Display My Number</label>
										</div>
									</div>
								</div>
								<div class="col-xs-12 col-sm-4 col-md-4">
									<div class="form-group">
										<label for="email">Email Id</label>
										<input type="email" class="form-control" id="email" placeholder="info@brigadedeveloper.com" required="">
										<div class="display_check_none">
											<input type="checkbox" class="custom_checkbox" id="disp_mail">
											<label for="disp_mail" class="custom_checkbox_label">Display My Email</label>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>-->
				<div class="contact-details">
					<div class="container">
						<div class="post-form ">
							<div class="row">
								<div class="col-xs-12 col-sm-12 col-md-12">
									<p class="tems-rules">By clicking 'Submit' you agree to our <a href="javascript:void(0);">Terms of use</a> &amp;<a href="javascript:void(0);"> Posting Rules</a></p>
								</div>
							</div>
						</div>
					</div>
				</div> 
				<div class="contact-details contat-pad">
					<div class="container">
						<div class="post-form ">
							<div class="row">
								<div class="col-xs-12 col-sm-6 col-md-6">
									<ul class="list-inline">
										<li><button type="button" class="btn  contact-submit" value="1">Submit</button>
										
										</li>
										<li><button type="cancel" class="btn  contact-cancel">Cancel</button></li>
										<li><button type="button" class="btn  contact-submit" value="2">Save</button></li>
									</ul>
								</div>
								<!-- <div class="col-xs-12 col-sm-6 col-md-6">
									<button type="submit" class="btn contact-submit preview">Preview</button>
								</div> -->
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
@include('partials.inner_footer')
<script type="text/javascript">
	$(document).ready(function(){

	$('.autocomplete').geocomplete()
					  .bind("geocode:result", function(event, result){
					  		var lat = result.geometry.location.lat();
                  			var lng = result.geometry.location.lng();
                  			var add = result.formatted_address;
                  			$('.append_area ul').append('<li class="locality_li"><div>'+add+'<span><i class="fa fa-times-circle-o fa-2x closelocality" aria-hidden="true"></i></span></div><input type="hidden" value="'+add+'" name="address[]"><input type="hidden" value="'+lat+'" name="lat[]"><input type="hidden" value="'+lng+'" name="lng[]"></li>');
                  			$('.autocomplete').val('');
					  });

	$('.contact-submit').on('click',function(){
		if($(this).val() == 2)
			$('input[name=status]').val(2);


		$.ajax({
			type:"POST",
			url:$(this).attr('action'),
			data:$('form[name="post_requirement"]').serialize(),
			success:function(res){
				/*if(res == 1){
					alert('Success');
				} else {
					alert('Fail');
				}*/
				alert('Success');
			}
		});
	});
	

	
	// SELECT AND APPEND CITY NAME
	$('.selectlocality').on('change', function(){
		var valueSelected = $(this).find(":selected").text();
		$('.append_area ul').append('<li class="locality_li"><div>'+valueSelected+'<span><i class="fa fa-times-circle-o fa-2x closelocality" aria-hidden="true"></i></span></div><input type="hidden" value="'+$(this).val()+'" name="local[]"></li>');
	});

	$('body').on('click','.closelocality',function(){
		$(this).parents('.locality_li').remove();
	});
});
</script>
@endsection
