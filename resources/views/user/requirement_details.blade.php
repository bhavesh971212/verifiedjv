@extends('layouts.master_user')

@section('content')
@include('partials.inner_header')
<section id="property_detail_section">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="property_main_left">
					<div class="cmmn_white_bg">
						<div class="plot_img_section">
							<div class="requirement_detail_head">
								<div class="row">
									<div class="col-md-11">
										<h2 class="SourceSemiBold">Brigade Enterprise</h2>
										<h4 class="SourceSemiBold">Commercial Plot</h4>
									</div>
									<div class="col-md-1">
										<button class="no_border_btn"><img src="images/icons/color_favour.png" width="20" alt=""></button>
									</div>
								</div>
								<div class="margin_top">
									<div class="row">
										<div class="col-md-8">
											<h3 class="SourceSemiBold">Bengaluru, Karnataka</h3>
										</div>
										<div class="col-md-4 right">
											<button class="SourceBold contact_logo_btn">Contact Details</button>
										</div>
									</div>
								</div>
							</div>
						</div>
						<article>
							<div class="row">
								<div class="col-md-7"><h3 class="SourceSemiBold">Required Details</h3></div>
								<div class="col-md-5 text-right">
									<ul class="list-inline user_short_cuts">
										<li class="print"><span data-toggle="tooltip" data-placement="bottom" title="Print Now"></span></li>
										<li class="pdf"><span data-toggle="tooltip" data-placement="bottom" title="Download Now"></span></li>
										<li class="phone"><span data-toggle="tooltip" data-placement="bottom" title="Contact"></span></li>
									</ul>
								</div>
							</div>
						</article>
						<div class="plot_detail_content">
							<div class="row">
								<div class="col-md-4">
									<table>
										<tr>
											<td>Posted By</td>
											<td><button class="spl">Individual</button></td>
										</tr>
										<tr>
											<td>Interested In</td>
											<td><button class="spl">Commercial Plot</button></td>
										</tr>
									</table>
								</div>
								<div class="col-md-4">
									<table>
										<tr>
											<td>Approx Budget</td>
											<td><button class="spl">20 t0 30 Crores</button></td>
										</tr>
										<tr>
											<td>Required Area</td>
											<td><button class="spl">5 to 6 acres</button></td>
										</tr>
									</table>
								</div>
								<div class="col-md-4">
									<table>
										<tr>
											<td>Interested In</td>
											<td><button class="spl">Re-Sale only</button></td>
										</tr>
									</table>
								</div>
							</div>
						</div>
					</div>
					<div class="cmmn_white_bg">
						<article>
							<h3 class="SourceSemiBold">Requirement Description</h3>
						</article>
						<div class="plot_detail_content">
							<p>Brigate Plots, A seamless amalgamation of luxury, comfort and style blend to provide a truly sophisticated lifestyle. These Residential Apartments in Bangalore are beautifully planned keeping in mind the architecture which can soothe your senses whenever you step into your house after a tiring day from work. Century Ethos by Century Real Estate in Bellary Road strives for customer satisfaction and believes in building world-class projects without compromising on quality standards, innovation and timely delivery. </p>
						</div>
					</div>
					<div class="cmmn_white_bg">
						<article>
							<h3 class="SourceSemiBold">Required Localities</h3>
						</article>
						<div class="plot_detail_content">
							<div class="row">
								<div class="col-md-2">
									<div class="document-section">
										<figure><img src="images/location.png" alt=""></figure>
										<figcaption>RajajiNagar</figcaption>
									</div>
								</div>
								<div class="col-md-2">
									<div class="document-section">
										<figure><img src="images/location.png" alt=""></figure>
										<figcaption>Malleshwaram</figcaption>
									</div>
								</div>
								<div class="col-md-2">
									<div class="document-section">
										<figure><img src="images/location.png" alt=""></figure>
										<figcaption>Yeswanthpur</figcaption>
									</div>
								</div>
								<div class="col-md-2">
									<div class="document-section">
										<figure><img src="images/location.png" alt=""></figure>
										<figcaption>Kamanahalli</figcaption>
									</div>
								</div>
								<div class="col-md-2">
									<div class="document-section">
										<figure><img src="images/location.png" alt=""></figure>
										<figcaption>Shivanahalli</figcaption>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="property_detail_right">
					<div class="cmmn_white_bg">
						<div class="title_box"><h4>Recent Plots/Lands</h4></div>
						<ul class="similar_search">
							<li>
								<div class="row">
									<div class="col-md-5 col-xs-12">
										<figure><a href=""><img src="images/similar_plot1.jpg" class="img-responsive" alt=""></a></figure>
									</div>
									<div class="col-md-7 col-xs-12">
										<a href="javascript:void(0);">Mahaveer Palatium</a>
										<div>
											<p class="location">Location</p>
											<h5>Doddballapur</h5>
										</div>
										<div>
											<p class="space">SPACE</p>
											<h5>174240 Sq.ft (4 Acres)</h5>
										</div>
										<div>
											<p class="price">PRICE RANGE</p>
											<h5>995/Sq.ft -  4500/Sq.ft</h5>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="row">
									<div class="col-md-5 col-xs-12">
										<figure><a href=""><img src="images/similar_plot1.jpg" class="img-responsive" alt=""></a></figure>
									</div>
									<div class="col-md-7 col-xs-12">
										<a href="javascript:void(0);">Pyramid Developer</a>
										<div>
											<p class="location">Location</p>
											<h5>Doddballapur</h5>
										</div>
										<div>
											<p class="space">SPACE</p>
											<h5>174240 Sq.ft (4 Acres)</h5>
										</div>
										<div>
											<p class="price">PRICE RANGE</p>
											<h5>995/Sq.ft -  4500/Sq.ft</h5>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="row">
									<div class="col-md-5 col-xs-12">
										<figure><a href=""><img src="images/one.jpg" class="img-responsive" alt=""></a></figure>
									</div>
									<div class="col-md-7 col-xs-12">
										<a href="javascript:void(0);">Mantri Developer</a>
										<div>
											<p class="location">Location</p>
											<h5>Doddballapur</h5>
										</div>
										<div>
											<p class="space">SPACE</p>
											<h5>174240 Sq.ft (4 Acres)</h5>
										</div>
										<div>
											<p class="price">PRICE RANGE</p>
											<h5>995/Sq.ft -  4500/Sq.ft</h5>
										</div>
									</div>
								</div>
							</li>
							<li>
								<div class="row">
									<div class="col-md-5 col-xs-12">
										<figure><a href=""><img src="images/similar_plot1.jpg" class="img-responsive" alt=""></a></figure>
									</div>
									<div class="col-md-7 col-xs-12">
										<a href="javascript:void(0);">Mahaveer Palatium</a>
										<div>
											<p class="location">Location</p>
											<h5>Doddballapur</h5>
										</div>
										<div>
											<p class="space">SPACE</p>
											<h5>174240 Sq.ft (4 Acres)</h5>
										</div>
										<div>
											<p class="price">PRICE RANGE</p>
											<h5>995/Sq.ft -  4500/Sq.ft</h5>
										</div>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@include('partials.inner_footer')
@endsection
