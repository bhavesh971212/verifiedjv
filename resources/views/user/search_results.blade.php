@extends('layouts.master_user')

@section('content')
@include('partials.inner_header')

<section id="result_page_section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="filer_section white_bg_color">
					<div class="row">
						<div class="col-md-6 col-xs-12 less_pad_custom">
							<div class="row">
								<div class="col-md-9 less_pad_custom">
									<div class="border_div">
										<select name="" id="">
											<option value="">Land</option>
											<option value="">Plot</option>
										</select>
										<input type="text" placeholder="Neighourhood, City....">
										<button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
									</div>
								</div>
								<div class="col-md-3 less_pad_custom">
									<div class="row">
										<div class="col-md-12 less_pad_custom">
											<select id="basic" class="selectpicker">
												<option>Popular Type</option>
												<option>High Level</option>
												<option>Low Level</option>
												<option>Mid Level</option>
											</select>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 col-xs-12 less_pad_custom">
							<div class="row">
								<div class="col-md-5 less_pad_custom">
									<div class="row">
										<div class="col-md-6 less_pad_custom" style="position: relative;">
											<input type="text" placeholder="Price Range" id="price_range" class="form-control">
											<div class="min_max_div" style="display: none;">
												<article>
													<div class="row">
														<div class="col-md-6">
															<label for=""><small>Min</small></label>
															<input type="text" class="min_budget_value">
														</div>
														<div class="col-md-6">
															<label for=""><small>Max</small></label>
															<input type="text" class="max_budget_value">
														</div>
													</div>
												</article>
												<article>
													<ul class="min_value" style="display: none;">
														<li><i class="fa fa-inr" aria-hidden="true"></i><span>100000</span></li>
														<li><i class="fa fa-inr" aria-hidden="true"></i><span>200000</span></li>
														<li><i class="fa fa-inr" aria-hidden="true"></i><span>300000</span></li>
														<li><i class="fa fa-inr" aria-hidden="true"></i><span>400000</span></li>
														<li><i class="fa fa-inr" aria-hidden="true"></i><span>500000</span></li>
													</ul>
													<ul class="max_value text-right" style="display: none;">
														<li><i class="fa fa-inr" aria-hidden="true"></i><span>200000</span></li>
														<li><i class="fa fa-inr" aria-hidden="true"></i><span>300000</span></li>
														<li><i class="fa fa-inr" aria-hidden="true"></i><span>400000</span></li>
														<li><i class="fa fa-inr" aria-hidden="true"></i><span>500000</span></li>
														<li><i class="fa fa-inr" aria-hidden="true"></i><span>600000</span></li>
													</ul>
												</article>
											</div>
										</div>
										<div class="col-md-6 less_pad_custom">
											<select id="basic" class="selectpicker">
												<option>Area Unit</option>
												<option>24Lakhs</option>
												<option>25Lakhs</option>
												<option>28Crores</option>
											</select>
										</div>
									</div>
								</div>
								<div class="col-md-7 less_pad_custom">
									<div class="row">
										<div class="col-md-4 less_pad_custom">
											<a href="javascript:void(0);" class="same_btn_list" id="more_filter">More Filters</a>
										</div>
										<div class="col-md-4 less_pad_custom"><a href="javascript:void(0);" class="same_btn_list" id="apply">Apply Now</a></div>
										<div class="col-md-4 less_pad_custom"><a href="javascript:void(0);" class="same_btn_list" id="reset">Reset</a></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="more_filter_div white_bg_color" style="display: none;">
						<ul class="filterMainList">
							<li>
								<h4>Lorem IPSUM</h4>
								<ul>
									<li>
										<div>
											<input class="custom_checkbox" id="filter_one" type="checkbox">
											<label for="filter_one" class="custom_checkbox_label">Filter One</label>
										</div>
									</li>
									<li>
										<div>
											<input class="custom_checkbox" id="filter_two" type="checkbox">
											<label for="filter_two" class="custom_checkbox_label">Filter Two</label>
										</div>
									</li>
									<li>
										<div>
											<input class="custom_checkbox" id="filter_three" type="checkbox">
											<label for="filter_three" class="custom_checkbox_label">Filter Three</label>
										</div>
									</li>
									<li>
										<div>
											<input class="custom_checkbox" id="filter_four" type="checkbox">
											<label for="filter_four" class="custom_checkbox_label">Filter Four</label>
										</div>
									</li>
									<li>
										<div>
											<input class="custom_checkbox" id="filter_five" type="checkbox">
											<label for="filter_five" class="custom_checkbox_label">Filter Five</label>
										</div>
									</li>
								</ul>
							</li>
							<li>
								<h4>Lorem IPSUM</h4>
								<ul>
									<li>
										<div>
											<input class="custom_checkbox" id="filter_six" type="checkbox">
											<label for="filter_six" class="custom_checkbox_label">Filter</label>
										</div>
									</li>
									<li>
										<div>
											<input class="custom_checkbox" id="filter_seven" type="checkbox">
											<label for="filter_seven" class="custom_checkbox_label">Filter</label>
										</div>
									</li>
									<li>
										<div>
											<input class="custom_checkbox" id="filter_eight" type="checkbox">
											<label for="filter_eight" class="custom_checkbox_label">Filter</label>
										</div>
									</li>
									<li>
										<div>
											<input class="custom_checkbox" id="filter_nine" type="checkbox">
											<label for="filter_nine" class="custom_checkbox_label">Filter</label>
										</div>
									</li>
									<li>
										<div>
											<input class="custom_checkbox" id="filter_ten" type="checkbox">
											<label for="filter_ten" class="custom_checkbox_label">Filter</label>
										</div>
									</li>
								</ul>
							</li>
							<li>
								<h4>Lorem IPSUM</h4>
								<ul>
									<li>
										<div>
											<input class="custom_checkbox" id="filter_11" type="checkbox">
											<label for="filter_11" class="custom_checkbox_label">Filter</label>
										</div>
									</li>
									<li>
										<div>
											<input class="custom_checkbox" id="filter_12" type="checkbox">
											<label for="filter_12" class="custom_checkbox_label">Filter</label>
										</div>
									</li>
									<li>
										<div>
											<input class="custom_checkbox" id="filter_13" type="checkbox">
											<label for="filter_13" class="custom_checkbox_label">Filter</label>
										</div>
									</li>
									<li>
										<div>
											<input class="custom_checkbox" id="filter_14" type="checkbox">
											<label for="filter_14" class="custom_checkbox_label">Filter</label>
										</div>
									</li>
									<li>
										<div>
											<input class="custom_checkbox" id="filter_15" type="checkbox">
											<label for="filter_15" class="custom_checkbox_label">Filter</label>
										</div>
									</li>
								</ul>
							</li>
							<li>
								<h4>Lorem IPSUM</h4>
								<ul>
									<li>
										<div>
											<input class="custom_checkbox" id="filter_16" type="checkbox">
											<label for="filter_16" class="custom_checkbox_label">Filter</label>
										</div>
									</li>
									<li>
										<div>
											<input class="custom_checkbox" id="filter_17" type="checkbox">
											<label for="filter_17" class="custom_checkbox_label">Filter</label>
										</div>
									</li>
									<li>
										<div>
											<input class="custom_checkbox" id="filter_18" type="checkbox">
											<label for="filter_18" class="custom_checkbox_label">Filter</label>
										</div>
									</li>
									<li>
										<div>
											<input class="custom_checkbox" id="filter_19" type="checkbox">
											<label for="filter_19" class="custom_checkbox_label">Filter</label>
										</div>
									</li>
									<li>
										<div>
											<input class="custom_checkbox" id="filter_20" type="checkbox">
											<label for="filter_20" class="custom_checkbox_label">Filter</label>
										</div>
									</li>
								</ul>
							</li>
							<li>
								<h4>Lorem IPSUM</h4>
								<ul>
									<li>
										<div>
											<input class="custom_checkbox" id="filter_21" type="checkbox">
											<label for="filter_21" class="custom_checkbox_label">Filter</label>
										</div>
									</li>
									<li>
										<div>
											<input class="custom_checkbox" id="filter_22" type="checkbox">
											<label for="filter_22" class="custom_checkbox_label">Filter</label>
										</div>
									</li>
									<li>
										<div>
											<input class="custom_checkbox" id="filter_23" type="checkbox">
											<label for="filter_23" class="custom_checkbox_label">Filter</label>
										</div>
									</li>
									<li>
										<div>
											<input class="custom_checkbox" id="filter_24" type="checkbox">
											<label for="filter_24" class="custom_checkbox_label">Filter</label>
										</div>
									</li>
									<li>
										<div>
											<input class="custom_checkbox" id="filter_25" type="checkbox">
											<label for="filter_25" class="custom_checkbox_label">Filter</label>
										</div>
									</li>
								</ul>
							</li>
						</ul>
						<div class="clear"></div>
					</div>
			</div>
		</div>
		<div class="main_result">
			<div class="row">
				<div class="col-md-6">
					<div class="result_left_section">
						<div class="white_bg_color" id="map_look_good">
							<div id="map_result"></div>
						</div>
						<div class="white_bg_color" id="contact_form">
							<div class="contact_form_start">
								<div class="text-right">
									<h6 id="close_form"><i class="fa fa-times-circle-o" aria-hidden="true"></i></h6>
								</div>
								<div class="text-center"><h5>CONTACT US</h5></div>
								<form action="">
									<div class="form-group">
										<input type="text" class="form-control" placeholder="Your Name*">
									</div>
									<div class="form-group">
										<input type="text" class="form-control" placeholder="Your Phone Number*">
									</div>
									<div class="form-group">
										<input type="email" class="form-control" placeholder="Your Email*">
									</div>
									<div class="form-group">
										<textarea name="" id="" rows="4" placeholder="Your Message" class="form-control"></textarea>
									</div>
									<div class="form-group">
										<button type="submit" class="submit_full_btn">SUBMIT</button>
									</div>
								</form>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 less_padding">
					<div class="result_right_section">
						<div class="white_bg_color">
							<div class="title_box_sort">
								<div class="row">
									<div class="col-md-8">
										<h4>Showing 28 results for <span>Residential Projects</span></h4>
									</div>
									<div class="col-md-4 text-right sort_by_select">
										<label for="">Sort By</label>
										<select name="" id="sort_by_select">
											<option value="">Relevent</option>
											<option value="">Abcdef</option>
											<option value="">Bcedddd</option>
										</select>
									</div>
								</div>
							</div>
							<div class="result_listing">
								<ul>
									<li>
										<div class="row">
											<div class="col-md-8">
												<div class="row">
													<div class="col-md-5">
														<div class="left_result">
															<figure>
																<a href=""><img src="images/result/1.jpg"  alt=""></a>
															</figure>
														</div>
													</div>
													<div class="col-md-7">
														<div class="center_result">
															<a href="">Brigade Developer</a>
															<div>
																<p class="location">Location</p>
																<h4>Doddballapur</h4>
															</div>
															<div>
																<p class="space">Space</p>
																<h4>174240 Sq.ft (4 Acres)</h4>
															</div>
															<div>
																<p class="price_tag">Price Range</p>
																<h4><i class="fa fa-inr" aria-hidden="true"></i> 995/Sq.ft - 4500/Sq.ft</h4>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<div class="text-right post_date">
													<h5>Last modified on 14<sup>th</sup> Feb 17</h5>
												</div>
												<div class="right_result">
													<ul class="list-inline">
														<li><button class="contact_logo_btn">CONTACT</button></li>
														<li> <button class="favour_btn"><i class="fa fa-heart" aria-hidden="true"></i></button></li>
													</ul>
												</div>
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<div class="col-md-8">
												<div class="row">
													<div class="col-md-5">
														<div class="left_result">
															<figure>
																<a href=""><img src="images/result/1.jpg"  alt=""></a>
															</figure>
														</div>
													</div>
													<div class="col-md-7">
														<div class="center_result">
															<a href="">Brigade Developer</a>
															<div>
																<p class="location">Location</p>
																<h4>Doddballapur</h4>
															</div>
															<div>
																<p class="space">Space</p>
																<h4>174240 Sq.ft (4 Acres)</h4>
															</div>
															<div>
																<p class="price_tag">Price Range</p>
																<h4><i class="fa fa-inr" aria-hidden="true"></i> 995/Sq.ft - 4500/Sq.ft</h4>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<div class="text-right post_date">
													<h5>Last modified on 14<sup>th</sup> Feb 17</h5>
												</div>
												<div class="right_result">
													<ul class="list-inline">
														<li><button class="contact_logo_btn">CONTACT</button></li>
														<li> <button class="favour_btn"><i class="fa fa-heart" aria-hidden="true"></i></button></li>
													</ul>
												</div>
											</div>
										</div>
									</li>
									<li>
										<div class="row">
											<div class="col-md-8">
												<div class="row">
													<div class="col-md-5">
														<div class="left_result">
															<figure>
																<a href=""><img src="images/result/1.jpg"  alt=""></a>
															</figure>
														</div>
													</div>
													<div class="col-md-7">
														<div class="center_result">
															<a href="">Brigade Developer</a>
															<div>
																<p class="location">Location</p>
																<h4>Doddballapur</h4>
															</div>
															<div>
																<p class="space">Space</p>
																<h4>174240 Sq.ft (4 Acres)</h4>
															</div>
															<div>
																<p class="price_tag">Price Range</p>
																<h4><i class="fa fa-inr" aria-hidden="true"></i> 995/Sq.ft - 4500/Sq.ft</h4>
															</div>
														</div>
													</div>
												</div>
											</div>
											<div class="col-md-4">
												<div class="text-right post_date">
													<h5>Last modified on 14<sup>th</sup> Feb 17</h5>
												</div>
												<div class="right_result">
													<ul class="list-inline">
														<li><button class="contact_logo_btn">CONTACT</button></li>
														<li> <button class="favour_btn"><i class="fa fa-heart" aria-hidden="true"></i></button></li>
													</ul>
												</div>
											</div>
										</div>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

@include('partials.inner_footer')
<script>
	function initMap() {
		var myLatLng = {lat: 12.954517, lng: 77.715671};
		var map = new google.maps.Map(document.getElementById('map_result'), {
			zoom: 13,
			center: myLatLng,
			scrollwheel: false
		});
			var marker = new google.maps.Marker({
			position: myLatLng,
			map: map,
			title: 'Pavani Royal'
		});
	}
  google.maps.event.addDomListener(window, 'load', initMap);
</script>
@endsection
