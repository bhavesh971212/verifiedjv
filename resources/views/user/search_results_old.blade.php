@extends('layouts.master_user')

@section('content')
@include('partials.inner_header')

	<section id="result_page_section">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="filer_section white_bg_color">
						<div class="row">
							<div class="col-md-6 col-xs-12 less_pad_custom">
								<div class="row">
									<div class="col-md-6 less_pad_custom">
										<div class="border_div">
											<select name="" id="">
												<option value="">Land</option>
												<option value="">Plot</option>
											</select>
											<input type="text" placeholder="Neighourhood, City....">
											<button type="submit"><i class="fa fa-search" aria-hidden="true"></i></button>
										</div>
									</div>
									<div class="col-md-6 less_pad_custom">
										<div class="row">
											<div class="col-md-6 less_pad_custom">
												<select id="basic" class="selectpicker">
													<option>Popular Localities</option>
													<option>Bengaluru</option>
													<option>Hassan</option>
													<option>Mysore</option>
												</select>
											</div>
											<div class="col-md-6 less_pad_custom">
												<select id="basic" class="selectpicker">
													<option>Popular Type</option>
													<option>High Level</option>
													<option>Low Level</option>
													<option>Mid Level</option>
												</select>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 col-xs-12 less_pad_custom">
								<div class="row">
									<div class="col-md-5 less_pad_custom">
										<div class="row">
											<div class="col-md-6 less_pad_custom">
												<select id="basic" class="selectpicker">
													<option>Price Range</option>
													<option>24Lakhs</option>
													<option>25Lakhs</option>
													<option>28Crores</option>
												</select>
											</div>
											<div class="col-md-6 less_pad_custom">
												<select id="basic" class="selectpicker">
													<option>Area Unit</option>
													<option>24Lakhs</option>
													<option>25Lakhs</option>
													<option>28Crores</option>
												</select>
											</div>
										</div>
									</div>
									<div class="col-md-7 less_pad_custom">
										<div class="row">
											<div class="col-md-4 less_pad_custom"><a href="javascript:void(0);" class="same_btn_list" id="more_filter">More Filters</a></div>
											<div class="col-md-4 less_pad_custom"><a href="javascript:void(0);" class="same_btn_list" id="apply">Apply Now</a></div>
											<div class="col-md-4 less_pad_custom"><a href="javascript:void(0);" class="same_btn_list" id="reset">Reset</a></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="main_result">
				<div class="row">
					<div class="col-md-6">
						<div class="result_left_section">
							<div class="white_bg_color" id="map_look_good">
								<div id="map_result"></div>
							</div>
						</div>
					</div>
					<div class="col-md-6 less_padding">
						<div class="result_right_section">
							<div class="white_bg_color">
								<div class="title_box_sort">
									<div class="row">
										<div class="col-md-8">
											<h4>Showing 28 results for <span>Residential Projects</span></h4>
										</div>
										<div class="col-md-4 text-right">
											<label for="">Sort By</label>
											<select name="" id="">
												<option value="">Relevent</option>
												<option value="">Abcdef</option>
												<option value="">Bcedddd</option>
											</select>
										</div>
									</div>
								</div>
								<div class="result_listing">
									<ul>
										<li>
											<div class="text-right post_date">
												<h5>Posted on 14<sup>th</sup> Feb 17</h5>
											</div>
											<div class="row">
												<div class="col-md-8">
													<div class="row">
														<div class="col-md-5">
															<div class="left_result">
																<figure>
																	<a href=""><img src="image/result/1.jpg" width="145" height="170" alt=""></a>
																</figure>
															</div>
														</div>
														<div class="col-md-7">
															<div class="center_result">
																<a href="">Brigade Developer</a>
																<div>
																	<p class="location">Location</p>
																	<h4>Doddballapur</h4>
																</div>
																<div>
																	<p class="space">Space</p>
																	<h4>174240 Sq.ft (4 Acres)</h4>
																</div>
																<div>
																	<p class="price_tag">Price Range</p>
																	<h4><i class="fa fa-inr" aria-hidden="true"></i> 995/Sq.ft - 4500/Sq.ft</h4>
																</div>
															</div>
														</div>
													</div>
												</div>
												<div class="col-md-4">
													<div class="right_result">
														<ul class="list-inline">
															<li><button class="contact_logo_btn">CONTACT</button></li>
														</ul>
													</div>
												</div>
											</div>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
@include('partials.inner_footer')

@endsection

	<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCdNQvIbP21kxPzykkrLSvU-Hl3X7_F6zc&callback=initMap"></script>
	<script>
		function initMap() {
			var myLatLng = {lat: 12.954517, lng: 77.715671};
			var map = new google.maps.Map(document.getElementById('map_result'), {
				zoom: 13,
				center: myLatLng,
				scrollwheel: false
			});
				var marker = new google.maps.Marker({
				position: myLatLng,
				map: map,
				title: 'Pavani Royal'
			});
		}
	</script>