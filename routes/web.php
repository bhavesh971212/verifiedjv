<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


/* Start Routes for Front side */
Route::get('/ind', function () {
    return view('header');
});
Route::get('/home', function () {
    return view('welcome');
});

Route::get('/','HomeController@index' );
//Route::post('home/search','HomeController@search'); //admin

Route::get('/site_details', function () {
    return view('user/site_details');
});

Route::get('/search_results', function () {
    return view('user/search_results');
});

Route::get('/requirement_details', function () {
    return view('user/requirement_details');
});

Route::get('/requirement_search', function () {
    return view('user/requirement_search');
});

// Login Routes
Route::post('login','Logincontroller@userlogin')->middleware('web');
Route::get('logout','Logincontroller@userlogout');
// End Login Routes

// Register Routes
Route::post('register','RegisterController@postregister')->middleware('web');
Route::get('register','Registercontroller@register');
Route::post('registerOtp','RegisterController@postOtpCheck')->middleware('web');
Route::get('register/Verify/{confirmation_code}','RegisterController@verifyEmail');
// End Register Routes

// Post Requirement Routes
Route::get('post_requirement','RequirementController@post_requirement');
Route::post('post_requirement','RequirementController@store_post_requirement');
Route::post('getCity','RequirementController@getCityByState');
Route::post('getLocality','RequirementController@getLocalityByCity');
// End Post Requirement Routes

// Post Property Routes
Route::get('post_add','PropertyController@post_adds');
Route::post('post_add','PropertyController@store_post_adds');
// End Post Property Routes

/* End Routes for Front side */

// ------------------------------------------------------------------------------------

Route::get('admin','Logincontroller@login');
Route::post('admin','Logincontroller@postlogin')->middleware('web');
Route::get('adminlogout','Logincontroller@logout');

Route::get('search','SearchController@search');
Route::get('search_result','SearchController@postsearch');
Route::post('search_filter','SearchController@filter')->middleware('web');
Route::post('contact','ContactController@store');
//Route::post('register','Registercontroller@postregister')->middleware('web');
Route::get('property-details/{slug}/{loc}','PropertyController@property_details'); //
Route::post('zipFileDownload','Dashboardcontroller@zipFileDownload'); //
Route::group(['middleware' => ['auth','web']], function ()
{
	Route::get('querylist','ContactController@querylist');  //admin
	Route::get('admin-dashboard','Dashboardcontroller@index');  //admin
	Route::get('property-list','Dashboardcontroller@listproperty');    //admin
	Route::get('property-list/show/{id}','Dashboardcontroller@show');    //admin
	Route::get('property/activate/{id}','Dashboardcontroller@activate'); //admin
	Route::get('property/deactivate/{id}','Dashboardcontroller@deactivate'); //admin
	Route::get('property/deal/{id}','Dashboardcontroller@deal'); //admin
	Route::get('property/recent_deals','PropertyController@recent_deals'); //admin
	Route::get('property/undeal/{id}','Dashboardcontroller@undeal'); //admin
	Route::get('property-list/edit/{id}','PropertyController@edit');    //admin
  Route::post('property-list/edit/{id}','PropertyController@update');    //admin
	Route::get('user-list','UserController@index'); //admin
	Route::get('user/activate/{id}','UserController@activate'); //admin
	Route::get('user/deactivate/{id}','UserController@deactivate'); //admin
	Route::get('partnerlist','PartnerController@index'); //admin
	Route::get('partners/add','PartnerController@create'); //admin
	Route::post('partners/add','PartnerController@store'); //admin
	Route::get('partners/edit/{id}','PartnerController@edit'); //admin
	Route::post('partners/edit/{id}','PartnerController@update'); //admin
    Route::get('partners/activate/{id}','PartnerController@activate'); //admin
	Route::get('partners/deactivate/{id}','PartnerController@deactivate'); //admin
	Route::get('partner/list','PartnerController@partnerlist');//users
	Route::get('requirementslist','RequirementController@index'); //admin
	Route::get('requirement/list','RequirementController@requirementlist'); //user
	Route::get('requirements/activate/{id}','RequirementController@activate'); //admin
	Route::get('requirements/deactivate/{id}','RequirementController@deactivate'); //admin

    Route::get('interestlist','InterestController@interestlist'); //admin
    Route::get('interest/add','InterestController@create');  //admin
    Route::post('interest/add','InterestController@store');  //admin
	Route::get('interest/edit/{id}','InterestController@edit');  //admin
	Route::post('interest/edit/{id}','InterestController@update');  //admin
   	Route::get('interest/delete/{id}','InterestController@delete');  //admin

   	Route::get('property_typelist','PropertytypeController@typelist'); //admin
    Route::get('property_type/add','PropertytypeController@create');  //admin
    Route::post('property_type/add','PropertytypeController@store');  //admin
	Route::get('property_type/edit/{id}','PropertytypeController@edit');  //admin
	Route::post('property_type/edit/{id}','PropertytypeController@update');  //admin
    Route::get('property_type/delete/{id}','PropertytypeController@delete');  //admin

	Route::get('plotlist','PlotController@plotlist'); //admin
	Route::get('plot/add','PlotController@create');  //admin
	Route::post('plot/add','PlotController@store'); //admin
	Route::get('plot/edit/{id}','PlotController@edit');  //admin
	Route::post('plot/edit/{id}','PlotController@update');  //admin
	Route::get('plot/delete/{id}','PlotController@delete');  //admin

	Route::get('locality/list','LocalityController@locality_list');  //admin
	Route::get('locality/add','LocalityController@create');  //admin
	Route::post('locality/add','LocalityController@store');  //admin
	Route::get('locality/edit/{id}','LocalityController@edit');  //admin
	Route::post('locality/edit/{id}','LocalityController@update');  //admin
	Route::get('locality/delete/{id}','LocalityController@delete');  //admin

    Route::get('zonelist','ZoneController@zonelist'); //admin
    Route::get('zone/add','ZoneController@create');  //admin
    Route::post('zone/add','ZoneController@store');  //admin
	Route::get('zone/edit/{id}','ZoneController@edit');  //admin
	Route::post('zone/edit/{id}','ZoneController@update');  //admin
    Route::get('zone/delete/{id}','ZoneController@delete');  //admin

	Route::get('user-dashboard','Dashboardcontroller@index');   //seller
	Route::get('add-property','PropertyController@create'); //seller
	Route::post('add-property','PropertyController@store'); //seller
	Route::get('edit-property/{id}','PropertyController@edit'); //seller
	Route::post('edit-property/{id}','PropertyController@post_edit'); //seller
	Route::get('delete-property/{id}','PropertyController@delete'); //seller
	Route::get('my-property','PropertyController@myproperty'); //seller
	Route::get('notifications-list','NotificationController@index'); //buyer
	Route::get('seen-notifications','NotificationController@seen_notifications'); //buyer
    Route::get('add-requirements','RequirementController@create');//buyer
    Route::post('add-requirements','RequirementController@store');//buyer
    Route::get('edit-requirements/{id}','RequirementController@edit');//buyer
    Route::post('edit-requirements/{id}','RequirementController@update');//buyer


});
